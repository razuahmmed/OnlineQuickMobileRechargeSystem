package com.common;

public class QueryStatement {

    public static String selectSuspendUserInfo(String userId, String password) {

        String selectLoginStatement = " SELECT "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UG.group_id, "
                + " UG.group_name "
                + " FROM "
                + " user_info UI, "
                + " user_group UG "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id ='" + userId + "' "
                + " AND "
                + " UI.user_password ='" + password + "' "
                + " AND "
                + " UI.suspend_activity = 'N' ";

        return selectLoginStatement;
    }

    public static String selectLoginInfo(String userId, String password) {

        String selectLoginStatement = " SELECT "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.user_password, "
                + " UG.group_id, "
                + " UG.group_name "
                + " FROM "
                + " user_info UI, "
                + " user_group UG "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id ='" + userId + "' "
                + " AND "
                + " UI.user_password ='" + password + "' "
                + " AND "
                + " UI.suspend_activity = 'Y' ";

        return selectLoginStatement;
    }

    public static String selectAdminUserLoginInfo(String userName, String password) {

        String selectAULoginStatement = " SELECT "
                + " AG.admin_group_id, "
                + " AG.admin_group_name, "
                + " AU.admin_user_id, "
                + " AU.user_name, "
                + " AU.password, "
                + " AU.admin_group_id, "
                + " AU.active_status "
                + " FROM "
                + " admin_group AG, "
                + " admin_user AU "
                + " WHERE "
                + " AU.admin_group_id = AG.admin_group_id "
                + " AND "
                + " AU.user_name = '" + userName + "' "
                + " AND "
                + " AU.password = '" + password + "' "
                + " AND "
                + " AU.active_status = 'Y' ";

        return selectAULoginStatement;
    }

    public static String selectMenu(String uId, Integer lId) {

        String selectMenu3 = " SELECT C.menu_id, C.level_menu_status, "
                + " CASE WHEN C.service_mrecharge=\"MR\" THEN 'T' ELSE 'F' END service_mrecharge, "
                + " CASE WHEN C.service_mmoney=\"MM\" THEN 'T' ELSE 'F' END service_mmoney "
                + " FROM "
                + " ( SELECT A.menu_id, A.user_level_menu_id, B.service_mrecharge, B.service_mmoney, "
                + " CASE WHEN A.level_menu_status = 'Y' THEN 'T' ELSE 'F' END level_menu_status "
                + " FROM user_level_menu A, user_info B WHERE A.level_id = B.group_id "
                + " AND A.level_id = '" + lId + "' AND  B.user_id = '" + uId + "' )C ";

        return selectMenu3;
    }

    public static String updateLoginStatus(String userId) {

        String updateLoginStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " active_status = 'Y', "
                + " last_login = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + userId + "' ";

        return updateLoginStatement;
    }

    public static String updateLogoutStatus(String userId) {

        String updateLogoutStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " active_status = 'N', "
                + " last_logout = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + userId + "' ";

        return updateLogoutStatement;
    }

    public static String selectSingleMRecharge(String uId, Integer mrhId) {

        String singleMRHSelectStatement = " SELECT "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " UI.user_id, "
                + " MRH.mobile_recharge_id, "
                + " MRH.purchased_on, "
                + " MRH.sender, "
                + " MRH.receiver, "
                + " MRH.user_id, "
                + " MRH.operator, "
                + " MRH.operator_balance, "
                + " MRH.active_status, "
                + " MRH.type, "
                + " MRH.given_balance, "
                + " MRH.originator, "
                + " MRH.trid "
                + " FROM  "
                + " user_info UI, "
                + " mobile_recharge_debit MRD, "
                + " mobile_recharge_history MRH "
                + " WHERE "
                + " MRH.user_id = UI.user_id "
                + " AND "
                + " MRH.mrd_id = MRD.mr_debit_id "
                + " AND "
                + " MRH.user_id = '" + uId + "' "
                + " AND "
                + " MRH.mobile_recharge_id = '" + mrhId + "' ";

        return singleMRHSelectStatement;
    }

    public static String selectLastMRecharge(String uId) {

        String lastMRechargeHSelectStatement = " SELECT * FROM ( "
                + " SELECT * FROM "
                + " mobile_recharge_history "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " ORDER BY "
                + " mobile_recharge_id "
                + " DESC LIMIT 20 ) "
                + " SUB "
                + " ORDER BY "
                + " mobile_recharge_id ASC ";

        return lastMRechargeHSelectStatement;
    }

    public static String selectLatestProcessingOrdersMR(String uId) {

        String latestMRechargeHSelectStatement = " SELECT * FROM ( "
                + " SELECT * FROM "
                + " mobile_recharge_history "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " active_status = 'P' "
                + " ORDER BY "
                + " mobile_recharge_id "
                + " DESC LIMIT 20 ) "
                + " SUB "
                + " ORDER BY "
                + " mobile_recharge_id ASC ";

        return latestMRechargeHSelectStatement;
    }

    public static String selectLatestWaitingOrdersMR(String uId) {

        String latestMRechargeHSelectStatement = " SELECT * FROM ( "
                + " SELECT * FROM "
                + " mobile_recharge_history "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " active_status = 'W' "
                + " ORDER BY "
                + " mobile_recharge_id "
                + " DESC LIMIT 20 ) "
                + " SUB "
                + " ORDER BY "
                + " mobile_recharge_id ASC ";

        return latestMRechargeHSelectStatement;
    }

    public static String selectLatestSuccessOrdersMR(String uId) {

        String latestMRechargeHSelectStatement = " SELECT * FROM ( "
                + " SELECT * FROM "
                + " mobile_recharge_history "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " active_status = 'Y' "
                + " ORDER BY "
                + " mobile_recharge_id "
                + " DESC LIMIT 20 ) "
                + " SUB "
                + " ORDER BY "
                + " mobile_recharge_id ASC ";

        return latestMRechargeHSelectStatement;
    }

    public static String selectAllMRecharge(String uId) {

        String allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                + " type, given_balance, operator_balance, trid, tnxid, originator, active_status FROM mobile_recharge_history "
                + " WHERE user_id = '" + uId + "' ";

//        if (showMRType.equals("All")) {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' ";
//        } else if (showMRType.equals("Success")) {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' AND active_status = 'Y' ";
//        } else if (showMRType.equals("Pending")) {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' AND active_status = 'N' ";
//        } else if (showMRType.equals("Processing")) {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' AND active_status = 'P' ";
//        } else if (showMRType.equals("Waiting")) {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' AND active_status = 'W' ";
//        } else if (showMRType.equals("Failed")) {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' AND active_status = 'F' ";
//        } else {
//
//            allMRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
//                    + " type, given_balance, operator_balance, trid, originator, active_status FROM mobile_recharge_history "
//                    + " WHERE user_id = '" + uId + "' ";
//        }
        return allMRechargeHSelectStatement;
    }

    public static String selectSingleMMoney(String uId, Integer mmhId) {

        String singleMMoneyHSelectStatement = " SELECT "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " MMH.mobile_money_id, "
                + " MMH.purchased_on, "
                + " MMH.sender, "
                + " MMH.receiver, "
                + " MMH.operator, "
                + " MMH.operator_balance, "
                + " MMH.active_status, "
                + " MMH.type, "
                + " MMH.given_balance, "
                + " MMH.total_amount, "
                + " MMH.originator, "
                + " MMH.trid "
                + " FROM  "
                + " user_info UI, "
                + " mobile_money_debit MMD, "
                + " mobile_money_history MMH "
                + " WHERE "
                + " MMH.user_id = UI.user_id "
                + " AND "
                + " MMH.mmd_id = MMD.mm_debit_id "
                + " AND "
                + " MMH.user_id = '" + uId + "' "
                + " AND "
                + " MMH.mobile_money_id = '" + mmhId + "' ";

        return singleMMoneyHSelectStatement;
    }

    public static String selectLastMMoney(String uId) {

        String lastMMoneyHSelectStatement = " SELECT * FROM ( "
                + " SELECT * FROM "
                + " mobile_money_history "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " ORDER BY "
                + " mobile_money_id "
                + " DESC LIMIT 20 ) "
                + " SUB "
                + " ORDER BY "
                + " mobile_money_id ASC ";
        return lastMMoneyHSelectStatement;
    }

    public static String selectAllMMoney(String uId) {

        String allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                + " given_balance, operator_balance, total_amount, trid, originator, active_status "
                + " FROM mobile_money_history WHERE user_id = '" + uId + "' ";

//        if (mmType.equals("All")) {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' ";
//        } else if (mmType.equals("Success")) {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND active_status = 'Y' ";
//        } else if (mmType.equals("Pending")) {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND active_status = 'N' ";
//        } else if (mmType.equals("Processing")) {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND active_status = 'P' ";
//        } else if (mmType.equals("Waiting")) {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND active_status = 'W' ";
//        } else if (mmType.equals("Failed")) {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND active_status = 'F' ";
//        } else {
//
//            allMMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
//                    + " given_balance, operator_balance, total_amount, trid, originator, active_status "
//                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' ";
//        }
        return allMMoneyHSelectStatement;
    }

    public static String selectAllUserGroup() {

        String allUserGroupSelectStatement = " SELECT * FROM "
                + " user_group ";
        return allUserGroupSelectStatement;
    }

    public static String insertManageRateStatement() {

        String manageRateInsertStatement = " INSERT INTO "
                + " manage_rate ( "
                + " rate_name, "
                + " grameen_phone, "
                + " robi, "
                + " banglalink, "
                + " airtel, "
                + " teletalk, "
                + " bkash, "
                + " dbbl, "
                + " user_id, "
                + " insert_by, "
                + " insert_date "
                + " ) VALUES ( "
                + " ?,?,?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP )";

        return manageRateInsertStatement;
    }

    public static String updateManageRateStatement(String uId, Integer rateId) {

        String manageRateUpdateStatement = " UPDATE "
                + " manage_rate "
                + " SET "
                + " rate_name = ?, "
                + " grameen_phone = ?, "
                + " robi = ?, "
                + " banglalink= ?, "
                + " airtel = ?, "
                + " teletalk = ?, "
                + " bkash = ?, "
                + " dbbl = ?, "
                + " update_by = ?, "
                + " update_date = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " manage_rate_id = '" + rateId + "' ";
        return manageRateUpdateStatement;
    }

    public static String deleteManageRateStatement(String uId, Integer rateId) {

        String manageRateDeleteStatement = " DELETE FROM "
                + " manage_rate "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " manage_rate_id = '" + rateId + "' ";
        return manageRateDeleteStatement;
    }

    public static String selectAllManageRate(String uId) {

        String allManageRateSelectStatement = " SELECT * FROM "
                + " manage_rate "
                + " WHERE "
                + " user_id = '" + uId + "' ";

        return allManageRateSelectStatement;
    }

    public static String selectSingleManageRate(String uId, Integer rateId) {

        String oneRatePlanSelectStatement = " SELECT * FROM "
                + " manage_rate "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " manage_rate_id = '" + rateId + "' ";
        return oneRatePlanSelectStatement;
    }

    public static String selectResellerSubAdmin(String pId) {

        String resellerSubAdminSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.parent_id, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.group_id= 1 "
                + " AND "
                + " UI.parent_id= '" + pId + "' ";

        return resellerSubAdminSelectStatement;
    }

    public static String selectResellerFour(String pId) {

        String resellerFourSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.parent_id, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.group_id= 2 "
                + " AND "
                + " UI.parent_id= '" + pId + "' ";

        return resellerFourSelectStatement;
    }

    public static String selectResellerThree(String pId) {

        String resellerThreeSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.parent_id, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.group_id= 3 "
                + " AND "
                + " UI.parent_id= '" + pId + "' ";

        return resellerThreeSelectStatement;
    }

    public static String selectResellerTwo(String pId) {

        String resellerTwoSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.parent_id, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.group_id= 4 "
                + " AND "
                + " UI.parent_id= '" + pId + "' ";

        return resellerTwoSelectStatement;
    }

    public static String selectResellerOne(String pId) {

        String resellerOneSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.parent_id, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.group_id= 5 "
                + " AND "
                + " UI.parent_id= '" + pId + "' ";

        return resellerOneSelectStatement;
    }

    public static String selectAllReseller(String pId) {

        String allResellerSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.parent_id, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.parent_id= '" + pId + "' ";

        return allResellerSelectStatement;
    }

    public static String selectAllResellerByAdmin() {

        String allResellerSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.parent_id, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney, "
                + " UI.otp_status "
                + " FROM  "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id ";

        return allResellerSelectStatement;
    }

    public static String insertUserInfo() {

        String userInfoInsertStatement = " INSERT INTO "
                + " user_info ( "
                + " user_id, "
                + " parent_id, "
                + " user_name, "
                + " user_password, "
                + " contact_number1, "
                + " email_address, "
                + " group_id, "
                + " service_mrecharge, "
                + " service_mmoney, "
                + " manage_rate_id, "
                + " otp_status, "
                + " enable_pin_status, "
                + " api_access_status, "
                + " email_conf_status, "
                + " pin_conf_status, "
                + " insert_by, "
                + " insert_date "
                + ") VALUES ( "
                + " ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP "
                + " )";

        return userInfoInsertStatement;
    }

    public static String updateResellerInfo(String id) {

        String resellerUpdateStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " user_name = ?, "
                + " contact_number1 = ?, "
                + " email_address = ?, "
                + " service_mrecharge = ?, "
                + " service_mmoney = ?, "
                + " manage_rate_id = ?, "
                + " otp_status = ?, "
                + " enable_pin_status = ?, "
                + " api_access_status = ?, "
                + " email_conf_status = ?, "
                + " pin_conf_status = ?, "
                + " update_by = ?, "
                + " update_date = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + id + "' ";
        return resellerUpdateStatement;
    }

    public static String selectAllAdminGroup() {

        String selectAdminGroupStatement = " SELECT * FROM "
                + " admin_group ";
        return selectAdminGroupStatement;
    }

    public static String selectAllAdminUser() {

        String selectAdminUserStatement = " SELECT "
                + " AG.admin_group_id, "
                + " AG.admin_group_name, "
                + " AU.admin_user_id, "
                + " AU.full_name, "
                + " AU.phone, "
                + " AU.email, "
                + " AU.user_name, "
                + " AU.password, "
                + " AU.active_status "
                + " FROM admin_group AG, "
                + " admin_user AU "
                + " WHERE "
                + " AG.admin_group_id = AU.admin_group_id ";
        return selectAdminUserStatement;
    }

    public static String selectSingleAdminUser(Integer id) {

        String singleAdminUserSelectStatement = " SELECT "
                + " AG.admin_group_id, "
                + " AG.admin_group_name, "
                + " AU.admin_user_id, "
                + " AU.full_name, "
                + " AU.phone, "
                + " AU.email, "
                + " AU.user_name, "
                + " AU.password, "
                + " AU.active_status "
                + " FROM admin_group AG, "
                + " admin_user AU "
                + " WHERE AG.admin_group_id = AU.admin_group_id "
                + " AND AU.admin_user_id = '" + id + "' ";
        return singleAdminUserSelectStatement;
    }

    public static String selectMyProfileInfo(String id) {

        String profileSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.pin_conf_status, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.user_id = '" + id + "' ";

        return profileSelectStatement;
    }

    public static String selectAllUserLevel() {

        String userLevelSelectStatement = " SELECT "
                + " level_id, "
                + " level_name, "
                + " status, "
                + " insert_by, "
                + " insert_date, "
                + " update_by, "
                + " update_date, "
                + " com_code, "
                + " level_short_name, "
                + " level_description "
                + " FROM "
                + " user_level ";
        return userLevelSelectStatement;
    }

    public static String selectAllParentMenu() {

        String parentMenuSelectStatement = " SELECT "
                + " menu_id, "
                + " parent_menu_id, "
                + " menu_name, "
                + " menu_title, "
                + " menu_url, "
                + " menu_status "
                + " FROM "
                + " menu "
                + " WHERE "
                + " parent_menu_id = 0 ";

        return parentMenuSelectStatement;
    }

    public static String selectAllChildMenu(Integer pid) {

        String childMenuSelectStatement = " SELECT "
                + " menu_id, "
                + " parent_menu_id, "
                + " menu_name, "
                + " menu_title, "
                + " menu_url, "
                + " menu_status "
                + " FROM "
                + " menu "
                + " WHERE "
                + " parent_menu_id = '" + pid + "' ";

        return childMenuSelectStatement;
    }

    public static String selectUselLevelMenu(Integer lvId) {

        String userLevelMenuSelectStatement = " SELECT "
                + " M.menu_id, "
                + " M.parent_menu_id, "
                + " M.menu_name, "
                + " ULM.user_level_menu_id, "
                + " ULM.menu_id, "
                + " ULM.level_id, "
                + " ULM.level_menu_status "
                + " FROM "
                + " menu M, "
                + " user_level_menu ULM "
                + " WHERE "
                + " ULM.menu_id = M.menu_id "
                + " AND "
                + " ULM.level_id = '" + lvId + "' "
                + " AND "
                + " M.parent_menu_id = 0 ";

        return userLevelMenuSelectStatement;
    }

    public static String selectUselLevelChildMenu(Integer lvId, Integer pmId) {

        String userLevelChildMenuSelectStatement = " SELECT "
                + " M.menu_id, "
                + " M.parent_menu_id, "
                + " M.menu_name, "
                + " ULM.user_level_menu_id, "
                + " ULM.menu_id, "
                + " ULM.level_id, "
                + " ULM.level_menu_status "
                + " FROM "
                + " menu M, "
                + " user_level_menu ULM "
                + " WHERE "
                + " ULM.menu_id = M.menu_id "
                + " AND "
                + " ULM.level_id = '" + lvId + "' "
                + " AND "
                + " M.parent_menu_id = '" + pmId + "' ";

        return userLevelChildMenuSelectStatement;
    }

    public static String selectSingleReseller(String pId, String uId) {

        String singleResellerSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.pin_conf_status, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.user_id = '" + uId + "' ";

//                +" AND "
//                + " UI.parent_id = '" + pId + "'"
//        
        return singleResellerSelectStatement;
    }

    public static String deleteResellerStatement(String uId) {

        String resellerDeleteStatement = " DELETE FROM "
                + " user_info "
                + " WHERE "
                + " user_id = '" + uId + "' ";

        return resellerDeleteStatement;
    }

    public static String updateResellerProgile(String id) {

        String resellerUpdateStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " user_name = ?, "
                + " contact_number1 = ?, "
                + " email_address = ?, "
                + " otp_status = ?, "
                + " enable_pin_status = ?, "
                + " api_access_status = ?, "
                + " pin_conf_status = ?, "
                + " update_by = ?, "
                + " update_date = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + id + "' ";

        return resellerUpdateStatement;
    }

    public static String selectAllMRGroupName(String uId) {

        String allMRGNameSelectStatement = " SELECT "
                + " mr_gname_id, "
                + " mr_group_name, "
                + " user_id, "
                + " insert_by "
                + " FROM "
                + " mr_group_name "
                + " WHERE "
                + " user_id = '" + uId + "' ";

        return allMRGNameSelectStatement;
    }

    public static String updateResellerPassword(String id) {

        String resellerPasswordUpdateStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " user_password = ?, "
                + " update_by = ?, "
                + " update_date = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + id + "' ";
        return resellerPasswordUpdateStatement;
    }

    public static String setResellerTPin(String id) {

        String resellerTPinUpdateStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " enable_pin_status = 'Y', "
                + " pin_conf_status = ?, "
                + " update_by = ?, "
                + " update_date = CURRENT_TIMESTAMP "
                + " WHERE "
                + " user_id = '" + id + "' ";

        return resellerTPinUpdateStatement;
    }

    public static String selectResellerMRBalance(String id) {

        String resellerMRBalanceSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MRD.group_id, "
                + " MRD.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " manage_rate MRC, "
                + " mobile_recharge_debit MRD "
                + " WHERE "
                + " MRD.user_id = UI.user_id "
                + " AND "
                + " MRD.group_id = UG.group_id "
                + " AND "
                + " MRD.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " MRD.user_id = '" + id + "' ";

        return resellerMRBalanceSelectStatement;
    }

    public static String selectResellerMMBalance(String id) {

        String resellerMMBalanceSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " MMD.group_id, "
                + " MMD.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " manage_rate MRC, "
                + " mobile_money_debit MMD "
                + " WHERE "
                + " MMD.user_id = UI.user_id "
                + " AND "
                + " MMD.group_id = UG.group_id "
                + " AND "
                + " MMD.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " MMD.user_id = '" + id + "' ";
        return resellerMMBalanceSelectStatement;
    }

    public static String selectMRNumber(String sId, String mrNumber) {

        String mrNumberSelectStatement = " SELECT "
                + " mobile_recharge_id, "
                + " sender, "
                + " receiver, "
                + " given_balance, "
                + " type, "
                + " purchased_on, "
                + " trid, "
                + " active_status "
                + " FROM "
                + " mobile_recharge_history "
                + " WHERE "
                + " sender = '" + sId + "' "
                + " AND "
                + " receiver = '" + mrNumber + "' ";

        return mrNumberSelectStatement;
    }

    public static String selectMMNumber(String uId, String mmNumber) {

        String mmNumberSelectStatement = " SELECT "
                + " mobile_money_id, "
                + " sender, "
                + " receiver, "
                + " given_balance, "
                + " type, "
                + " purchased_on, "
                + " trid, "
                + " active_status "
                + " FROM "
                + " mobile_money_history "
                + " WHERE "
                + " sender = '" + uId + "' "
                + " AND "
                + " receiver = '" + mmNumber + "' ";

        return mmNumberSelectStatement;
    }

    public static String selectMRHistoryByDate(String uId, String cMrDate) {

        String mrHSelectStatement = " SELECT "
                + " mobile_recharge_id, "
                + " sender, "
                + " receiver, "
                + " given_balance, "
                + " type, "
                + " trid, "
                + " purchased_on, "
                + " active_status "
                + " FROM "
                + " mobile_recharge_history "
                + " WHERE "
                + " sender = '" + uId + "' "
                + " AND "
                + " date(purchased_on) = '" + cMrDate.trim() + "' ";

        return mrHSelectStatement;
    }

    public static String selectMMHistoryByDate(String uId, String cMmDate) {

        String mmHSelectStatement = " SELECT "
                + " mobile_money_id, "
                + " sender, "
                + " receiver, "
                + " given_balance, "
                + " type, "
                + " trid, "
                + " purchased_on, "
                + " active_status "
                + " FROM "
                + " mobile_money_history "
                + " WHERE "
                + " sender = '" + uId + "' "
                + " AND "
                + " date(purchased_on) = '" + cMmDate.trim() + "' ";

        return mmHSelectStatement;
    }

    public static String selectOnlineUser() {

        String onLineUserSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.active_status = 'Y' ";

        return onLineUserSelectStatement;
    }

    public static String updateResellerSuspendStatus(String userId) {

        String updateSuspendStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " suspend_activity = 'N' "
                + " WHERE "
                + " user_id = '" + userId + "' ";

        return updateSuspendStatement;
    }

    public static String updateResellerUnSuspendStatus(String userId) {

        String updateSuspendStatement = " UPDATE "
                + " user_info "
                + " SET "
                + " suspend_activity = 'Y' "
                + " WHERE "
                + " user_id = '" + userId + "' ";

        return updateSuspendStatement;
    }

    public static String selectParentId(String pId, String uId) {

        String parentIdSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_group UG, "
                + " user_info UI "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.parent_id = '" + pId + "' "
                + " AND "
                + " UI.user_id = '" + uId + "' ";

        return parentIdSelectStatement;
    }

    public static String selectAdmin(String uId) {

        String adminSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.user_id = '" + uId + "' "
                + " AND "
                + " UG.group_id = 0 ";

        return adminSelectStatement;
    }

    public static String selectAllContact(String uId) {

        String contactSelectStatement = " SELECT "
                + " contact_id, "
                + " contact_name, "
                + " contact_number, "
                + " insert_by, "
                + " insert_date, "
                + " user_id "
                + " FROM "
                + " save_contact "
                + " WHERE "
                + " user_id = '" + uId + "' ";

        return contactSelectStatement;
    }

    public static String selectContactNumber(Integer cId, String uId) {

        String contactNSelectStatement = " SELECT "
                + " contact_id, "
                + " contact_name, "
                + " contact_number, "
                + " insert_by, "
                + " insert_date, "
                + " user_id "
                + " FROM "
                + " save_contact "
                + " WHERE "
                + " user_id = '" + uId + "' "
                + " AND "
                + " contact_id= '" + cId + "' ";

        return contactNSelectStatement;
    }

    public static String selectCurrentPin(String uId, String uCPin) {

        String pinSelectStatement = " SELECT "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by, "
                + " UI.enable_pin_status, "
                + " UI.pin_conf_status "
                + " FROM  "
                + " user_info UI "
                + " WHERE "
                + " UI.user_id = '" + uId + "' "
                + " AND "
                + " UI.pin_conf_status = '" + uCPin + "' ";

        return pinSelectStatement;
    }

    public static String selectCurrentPassword(String uId, String uCPass) {

        String passSelectStatement = " SELECT "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.user_password, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by "
                + " FROM  "
                + " user_info UI "
                + " WHERE "
                + " UI.user_id = '" + uId + "' "
                + " AND "
                + " UI.user_password = '" + uCPass + "' ";

        return passSelectStatement;
    }

    public static String selectPaymentInfo(String pId) {

        String paymentInfoSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.suspend_activity, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.parent_id = '" + pId + "' ";

        return paymentInfoSelectStatement;
    }

    public static String selectSinglePaymentInfo(String pId, String rId) {

        String singlePaymentInfoSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.user_password, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.suspend_activity, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.parent_id = '" + pId + "' "
                + " AND "
                + " UI.user_id = '" + rId + "' ";

        return singlePaymentInfoSelectStatement;
    }

    // this two
    public static String selectTransactionHistory(String pId) {

        String transactionHSelectStatement = " SELECT "
                + " MRTH.mr_tr_his_id, "
                + " MRTH.mr_cur_bal, "
                + " MRTH.bal_given_by, "
                + " MRTH.bal_return_by, "
                + " MRTH.type, "
                + " MRTH.trid, "
                + " MRTH.bal_add, "
                + " MRTH.bal_return, "
                + " MRTH.old_bal, "
                + " MRTH.memo, "
                + " MRTH.bal_given_date, "
                + " MRTH.bal_return_date, "
                + " MRTH.user_id, "
                + " MRTH.group_id, "
                + " MRTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mr_transaction_history MRTH, "
                + " user_info UI "
                + " WHERE "
                + " MRTH.user_id = UI.user_id ";

        return transactionHSelectStatement;
    }

    public static String selectSingleTransactionHistory(String pId, Integer thId) {

        String singleTransactionHSelectStatement = " SELECT "
                + " MRTH.mr_tr_his_id, "
                + " MRTH.mr_cur_bal, "
                + " MRTH.bal_given_by, "
                + " MRTH.bal_return_by, "
                + " MRTH.type, "
                + " MRTH.trid, "
                + " MRTH.bal_add, "
                + " MRTH.bal_return, "
                + " MRTH.old_bal, "
                + " MRTH.memo, "
                + " MRTH.bal_given_date, "
                + " MRTH.bal_return_date, "
                + " MRTH.user_id, "
                + " MRTH.group_id, "
                + " MRTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mr_transaction_history MRTH, "
                + " user_info UI "
                + " WHERE "
                + " MRTH.user_id = UI.user_id "
                + " AND "
                + " MRTH.mr_tr_his_id = '" + thId + "' ";

        return singleTransactionHSelectStatement;
    }

    //
    public static String selectMRPaymentReceiveHistory(String uId) {

        String mrPReceiveHSelectStatement = " SELECT "
                + " MRTH.mr_tr_his_id, "
                + " MRTH.mr_cur_bal, "
                + " MRTH.bal_given_by, "
                + " MRTH.bal_return_by, "
                + " MRTH.type, "
                + " MRTH.trid, "
                + " MRTH.bal_add, "
                + " MRTH.bal_return, "
                + " MRTH.old_bal, "
                + " MRTH.memo, "
                + " MRTH.bal_given_date, "
                + " MRTH.bal_return_date, "
                + " MRTH.user_id, "
                + " MRTH.group_id, "
                + " MRTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mr_transaction_history MRTH, "
                + " user_info UI "
                + " WHERE "
                + " MRTH.user_id = UI.user_id "
                + " AND "
                + " MRTH.user_id = '" + uId + "' ";

        return mrPReceiveHSelectStatement;
    }

    public static String selectSingleMRPaymentReceiveHistory(String uId, Integer mrthId) {

        String singleMrPReceiveHSelectStatement = " SELECT "
                + " MRTH.mr_tr_his_id, "
                + " MRTH.mr_cur_bal, "
                + " MRTH.bal_given_by, "
                + " MRTH.bal_return_by, "
                + " MRTH.type, "
                + " MRTH.trid, "
                + " MRTH.bal_add, "
                + " MRTH.bal_return, "
                + " MRTH.old_bal, "
                + " MRTH.memo, "
                + " MRTH.bal_given_date, "
                + " MRTH.bal_return_date, "
                + " MRTH.user_id, "
                + " MRTH.group_id, "
                + " MRTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mr_transaction_history MRTH, "
                + " user_info UI "
                + " WHERE "
                + " MRTH.user_id = UI.user_id "
                + " AND "
                + " MRTH.user_id = '" + uId + "' "
                + " AND "
                + " MRTH.mr_tr_his_id = '" + mrthId + "' ";

        return singleMrPReceiveHSelectStatement;
    }

    public static String searchMRPaymentReceiveHistory(String uId, String fName, String fValue) {

        String searchMrPReceiveHSelectStatement = "";

        if (fName.equals("order_id")) {

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND MRTH.mr_tr_his_id = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                    }
                }
            }

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND date(MRTH.bal_given_date) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
        } else if (fName.equals("sender")) {

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND MRTH.bal_given_by = '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND UI.user_id = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND MRTH.mr_cur_bal BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("status")) {

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND MRTH.type = '" + fValue + "' ";
        } else if (fName.equals("trid")) {

            searchMrPReceiveHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.user_id = '" + uId + "' AND MRTH.trid = '" + fValue + "' ";
        }

        return searchMrPReceiveHSelectStatement;
    }

    //
    public static String selectMRPaymentMadeHistory(String uId) {

        String mrPMadeSelectStatement = " SELECT "
                + " MRTH.mr_tr_his_id, "
                + " MRTH.mr_cur_bal, "
                + " MRTH.bal_given_by, "
                + " MRTH.bal_return_by, "
                + " MRTH.type, "
                + " MRTH.trid, "
                + " MRTH.bal_add, "
                + " MRTH.bal_return, "
                + " MRTH.old_bal, "
                + " MRTH.memo, "
                + " MRTH.bal_given_date, "
                + " MRTH.bal_return_date, "
                + " MRTH.user_id, "
                + " MRTH.group_id, "
                + " MRTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.parent_id, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mr_transaction_history MRTH, "
                + " user_info UI "
                + " WHERE "
                + " MRTH.user_id = UI.user_id "
                + " AND "
                + " UI.parent_id = '" + uId + "' ";

        return mrPMadeSelectStatement;
    }

    public static String selectSingleMRPaymentMadeHistory(String uId, Integer mrthId) {

        String singleMrPMadeSelectStatement = " SELECT "
                + " MRTH.mr_tr_his_id, "
                + " MRTH.mr_cur_bal, "
                + " MRTH.bal_given_by, "
                + " MRTH.bal_return_by, "
                + " MRTH.type, "
                + " MRTH.trid, "
                + " MRTH.bal_add, "
                + " MRTH.bal_return, "
                + " MRTH.old_bal, "
                + " MRTH.memo, "
                + " MRTH.bal_given_date, "
                + " MRTH.bal_return_date, "
                + " MRTH.user_id, "
                + " MRTH.group_id, "
                + " MRTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.parent_id, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mr_transaction_history MRTH, "
                + " user_info UI "
                + " WHERE "
                + " MRTH.user_id = UI.user_id "
                + " AND "
                + " MRTH.mr_tr_his_id = '" + mrthId + "' ";

        return singleMrPMadeSelectStatement;
    }

    public static String searchMRPaymentMadeHistory(String uId, String fName, String fValue) {

        String searchMrPMadeHSelectStatement = "";

        if (fName.equals("order_id")) {

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND MRTH.mr_tr_his_id = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                    }
                }
            }

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND date(MRTH.bal_given_date) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
        } else if (fName.equals("sender")) {

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND MRTH.bal_given_by = '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND UI.user_id = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND MRTH.mr_cur_bal BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("status")) {

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND MRTH.type = '" + fValue + "' ";
        } else if (fName.equals("trid")) {

            searchMrPMadeHSelectStatement = " SELECT MRTH.mr_tr_his_id, MRTH.mr_cur_bal, MRTH.bal_given_by, "
                    + " MRTH.bal_return_by, MRTH.type, MRTH.trid, MRTH.bal_add, MRTH.bal_return, MRTH.old_bal, MRTH.memo, "
                    + " MRTH.bal_given_date, MRTH.bal_return_date, MRTH.user_id, MRTH.group_id, MRTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mr_transaction_history MRTH, user_info UI WHERE MRTH.user_id = UI.user_id AND "
                    + " UI.parent_id = '" + uId + "' AND MRTH.trid = '" + fValue + "' ";
        }

        return searchMrPMadeHSelectStatement;
    }

    //
    public static String selectMMPaymentReceiveHistory(String uId) {

        String mmPReceiveHSelectStatement = " SELECT "
                + " MMTH.mm_tr_his_id, "
                + " MMTH.mm_cur_bal, "
                + " MMTH.bal_given_by, "
                + " MMTH.bal_return_by, "
                + " MMTH.type, "
                + " MMTH.trid, "
                + " MMTH.bal_add, "
                + " MMTH.bal_return, "
                + " MMTH.old_bal, "
                + " MMTH.memo, "
                + " MMTH.bal_given_date, "
                + " MMTH.bal_return_date, "
                + " MMTH.user_id, "
                + " MMTH.group_id, "
                + " MMTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mm_transaction_history MMTH, "
                + " user_info UI "
                + " WHERE "
                + " MMTH.user_id = UI.user_id "
                + " AND "
                + " MMTH.user_id = '" + uId + "' ";

        return mmPReceiveHSelectStatement;
    }

    public static String selectSingleMMPaymentReceiveHistory(String uId, Integer mmthId) {

        String singleMmPReceiveHSelectStatement = " SELECT "
                + " MMTH.mm_tr_his_id, "
                + " MMTH.mm_cur_bal, "
                + " MMTH.bal_given_by, "
                + " MMTH.bal_return_by, "
                + " MMTH.type, "
                + " MMTH.trid, "
                + " MMTH.bal_add, "
                + " MMTH.bal_return, "
                + " MMTH.old_bal, "
                + " MMTH.memo, "
                + " MMTH.bal_given_date, "
                + " MMTH.bal_return_date, "
                + " MMTH.user_id, "
                + " MMTH.group_id, "
                + " MMTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mm_transaction_history MMTH, "
                + " user_info UI "
                + " WHERE "
                + " MMTH.user_id = UI.user_id "
                + " AND "
                + " MMTH.user_id = '" + uId + "' "
                + " AND "
                + " MMTH.mm_tr_his_id = '" + mmthId + "' ";

        return singleMmPReceiveHSelectStatement;
    }

    public static String searchMMPaymentReceiveHistory(String uId, String fName, String fValue) {

        String searchMmPReceiveHSelectStatement = "";

        if (fName.equals("order_id")) {

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND MMTH.mm_tr_his_id = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                    }
                }
            }

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND date(MMTH.bal_given_date) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
        } else if (fName.equals("sender")) {

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND MMTH.bal_given_by = '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND UI.user_id = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND MMTH.mm_cur_bal BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("status")) {

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND MMTH.type = '" + fValue + "' ";
        } else if (fName.equals("trid")) {

            searchMmPReceiveHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.user_id = '" + uId + "' AND MMTH.trid = '" + fValue + "' ";
        }

        return searchMmPReceiveHSelectStatement;
    }

    //
    public static String selectMMPaymentMadeHistory(String uId) {

        String mmPMadeHSelectStatement = " SELECT "
                + " MMTH.mm_tr_his_id, "
                + " MMTH.mm_cur_bal, "
                + " MMTH.bal_given_by, "
                + " MMTH.bal_return_by, "
                + " MMTH.type, "
                + " MMTH.trid, "
                + " MMTH.bal_add, "
                + " MMTH.bal_return, "
                + " MMTH.old_bal, "
                + " MMTH.memo, "
                + " MMTH.bal_given_date, "
                + " MMTH.bal_return_date, "
                + " MMTH.user_id, "
                + " MMTH.group_id, "
                + " MMTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mm_transaction_history MMTH, "
                + " user_info UI "
                + " WHERE "
                + " MMTH.user_id = UI.user_id "
                + " AND "
                + " UI.parent_id = '" + uId + "' ";

        return mmPMadeHSelectStatement;
    }

    public static String selectSingleMMPaymentMadeHistory(String uId, Integer mmthId) {

        String singleMmPMadeSelectStatement = " SELECT "
                + " MMTH.mm_tr_his_id, "
                + " MMTH.mm_cur_bal, "
                + " MMTH.bal_given_by, "
                + " MMTH.bal_return_by, "
                + " MMTH.type, "
                + " MMTH.trid, "
                + " MMTH.bal_add, "
                + " MMTH.bal_return, "
                + " MMTH.old_bal, "
                + " MMTH.memo, "
                + " MMTH.bal_given_date, "
                + " MMTH.bal_return_date, "
                + " MMTH.user_id, "
                + " MMTH.group_id, "
                + " MMTH.manage_rate_id, "
                + " UI.user_id, "
                + " UI.user_name, "
                + " UI.enable_pin_status, "
                + " UI.service_mrecharge, "
                + " UI.service_mmoney "
                + " FROM  "
                + " mm_transaction_history MMTH, "
                + " user_info UI "
                + " WHERE "
                + " MMTH.user_id = UI.user_id "
                + " AND "
                + " MMTH.mm_tr_his_id = '" + mmthId + "' ";

        return singleMmPMadeSelectStatement;
    }

    public static String searchMMPaymentMadeHistory(String uId, String fName, String fValue) {

        String searchMmPMadeHSelectStatement = "";

        if (fName.equals("order_id")) {

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, "
                    + " UI.user_id, UI.parent_id, UI.user_name, UI.enable_pin_status, UI.service_mrecharge, "
                    + " UI.service_mmoney FROM mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.parent_id = '" + uId + "' AND MMTH.mm_tr_his_id = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                    }
                }
            }

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, "
                    + " UI.user_id, UI.user_name, UI.parent_id, UI.enable_pin_status, UI.service_mrecharge, "
                    + " UI.service_mmoney FROM mm_transaction_history MMTH, user_info UI "
                    + " WHERE MMTH.user_id = UI.user_id AND UI.parent_id = '" + uId + "' "
                    + " AND date(MMTH.bal_given_date) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
        } else if (fName.equals("sender")) {

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.parent_id, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.parent_id = '" + uId + "' AND MMTH.bal_given_by = '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.parent_id, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.parent_id = '" + uId + "' AND UI.user_id = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.parent_id, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.parent_id = '" + uId + "' AND MMTH.mm_cur_bal BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("status")) {

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.parent_id, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.parent_id = '" + uId + "' AND MMTH.type = '" + fValue + "' ";
        } else if (fName.equals("trid")) {

            searchMmPMadeHSelectStatement = " SELECT MMTH.mm_tr_his_id, MMTH.mm_cur_bal, MMTH.bal_given_by, "
                    + " MMTH.bal_return_by, MMTH.type, MMTH.trid, MMTH.bal_add, MMTH.bal_return, MMTH.old_bal, MMTH.memo, "
                    + " MMTH.bal_given_date, MMTH.bal_return_date, MMTH.user_id, MMTH.group_id, MMTH.manage_rate_id, UI.user_id, "
                    + " UI.user_name, UI.parent_id, UI.enable_pin_status, UI.service_mrecharge, UI.service_mmoney FROM  "
                    + " mm_transaction_history MMTH, user_info UI WHERE MMTH.user_id = UI.user_id "
                    + " AND UI.parent_id = '" + uId + "' AND MMTH.trid = '" + fValue + "' ";
        }

        return searchMmPMadeHSelectStatement;
    }

    //
    public static String searchMRecharge(String uId, String fName, String fValue) {

        String mRechargeHSelectStatement = "";

        if (fName.equals("order_id")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND trid = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                    }
                }
            }

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND date(purchased_on) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
        } else if (fName.equals("sender")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND  sender= '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND receiver = '" + fValue + "' ";
        } else if (fName.equals("op")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND operator = '" + fValue + "' ";
        } else if (fName.equals("type")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND type = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }
            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND given_balance BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("status")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND active_status = '" + fValue + "' ";
        } else if (fName.equals("tnxid")) {

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND tnxid = '" + fValue + "' ";
        } else if (fName.equals("o")) {

            String org = "";
            if (fValue.equals("Modem")) {
                org = "1";
            } else {
                org = "";
            }

            mRechargeHSelectStatement = " SELECT mobile_recharge_id, purchased_on, sender, receiver, operator, "
                    + " type, given_balance, operator_balance, trid, tnxid, originator, user_id, active_status FROM mobile_recharge_history "
                    + " WHERE user_id = '" + uId + "' AND originator = '" + org + "' ";
        }

        return mRechargeHSelectStatement;
    }

    public static String searchMMoney(String uId, String fName, String fValue) {

        String mMoneyHSelectStatement = "";

        if (fName.equals("order_id")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND mobile_money_id = '" + fValue + "' ";
        } else if (fName.equals("sender")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND sender = '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND receiver = '" + fValue + "' ";
        } else if (fName.equals("op")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND operator = '" + fValue + "' ";
        } else if (fName.equals("type")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND type = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }
            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND given_balance BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("trid")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND trid = '" + fValue + "' ";
        } else if (fName.equals("o")) {

            mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                    + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                    + " FROM mobile_money_history WHERE user_id = '" + uId + "' AND originator = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";
            String status = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                        status = dateArray[2];
                    }
                }
            }

            if (status.contains("All")) {

                mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                        + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                        + " FROM mobile_money_history WHERE user_id = '" + uId + "' "
                        + " AND date(purchased_on) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
            } else {

                mMoneyHSelectStatement = " SELECT mobile_money_id, purchased_on, sender, receiver, operator, type, "
                        + " given_balance, operator_balance, total_amount, trid, originator, user_id, active_status "
                        + " FROM mobile_money_history WHERE user_id = '" + uId + "' "
                        + " AND date(purchased_on) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' AND active_status = '" + status.trim() + "' ";
            }
        }

        return mMoneyHSelectStatement;
    }

    public static String selectMRCurBal(String uId) {

        String mrCurBalSelectStatement = " SELECT "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.bal_given_by, "
                + " MRD.type, "
                + " MRD.trid, "
                + " MRD.bal_add, "
                + " MRD.old_bal, "
                + " MRD.mr_bal_description, "
                + " MRD.bal_given_date, "
                + " MRD.user_id, "
                + " MRD.group_id, "
                + " MRD.manage_rate_id "
                + " FROM  "
                + " mobile_recharge_debit MRD "
                + " WHERE "
                + " MRD.user_id = '" + uId + "' ";

        return mrCurBalSelectStatement;
    }

    public static String selectMMCurBal(String uId) {

        String mmCurBalSelectStatement = " SELECT "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.bal_given_by, "
                + " MMD.type, "
                + " MMD.trid, "
                + " MMD.bal_add, "
                + " MMD.old_bal, "
                + " MMD.mm_bal_description, "
                + " MMD.bal_given_date, "
                + " MMD.user_id, "
                + " MMD.group_id, "
                + " MMD.manage_rate_id "
                + " FROM  "
                + " mobile_money_debit MMD "
                + " WHERE "
                + " MMD.user_id = '" + uId + "' ";

        return mmCurBalSelectStatement;
    }

    public static String selectBalanceRequestReceiveHistory(String uId) {

        String balRequestReceiveSelectStatement = " SELECT "
                + " BRH.bal_request_id, "
                + " BRH.amount, "
                + " BRH.bal_type, "
                + " BRH.user_id, "
                + " BRH.request_time, "
                + " BRH.request_success_time, "
                + " BRH.request_status, "
                + " BRH.response_by, "
                + " BRH.request_to, "
                + " BRH.trid "
                + " FROM  "
                + " bal_request_history BRH "
                + " WHERE "
                + " BRH.request_to = '" + uId + "' ";

        return balRequestReceiveSelectStatement;
    }

    public static String selectSingleBalanceRequestReceiveHistory(String uId, Integer brhId) {

        String balRequestReceiveSelectStatement = " SELECT "
                + " BRH.bal_request_id, "
                + " BRH.amount, "
                + " BRH.bal_type, "
                + " BRH.user_id, "
                + " BRH.request_time, "
                + " BRH.request_success_time, "
                + " BRH.request_status, "
                + " BRH.response_by, "
                + " BRH.request_to, "
                + " BRH.trid "
                + " FROM  "
                + " bal_request_history BRH "
                + " WHERE "
                + " BRH.request_to = '" + uId + "' "
                + " AND "
                + " BRH.bal_request_id = '" + brhId + "' ";

        return balRequestReceiveSelectStatement;
    }

    public static String searchBalanceRequestHistory(String uId, String fName, String fValue) {

        String searchBalRequestReceiveSelectStatement = "";

        if (fName.equals("order_id")) {

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND bal_request_id = '" + fValue + "' ";
        } else if (fName.equals("sdate")) {

            String[] dateArray = null;
            String sDate = "";
            String tDate = "";

            if (fValue != null) {
                dateArray = fValue.split(",");
                if (dateArray != null) {
                    for (int i = 0; i < dateArray.length; i++) {
                        sDate = dateArray[0];
                        tDate = dateArray[1];
                    }
                }
            }

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND date(request_time) BETWEEN '" + sDate.trim() + "' AND '" + tDate.trim() + "' ";
        } else if (fName.equals("sender")) {

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND user_id = '" + fValue + "' ";
        } else if (fName.equals("receiver")) {

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND request_to = '" + fValue + "' ";
        } else if (fName.equals("type")) {

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND bal_type = '" + fValue + "' ";
        } else if (fName.equals("famount")) {

            String[] amountArray = null;
            String fAmount = "";
            String tAmount = "";

            if (fValue != null) {
                amountArray = fValue.split(",");
                if (amountArray != null) {
                    for (int i = 0; i < amountArray.length; i++) {
                        fAmount = amountArray[0];
                        tAmount = amountArray[1];
                    }
                }
            }

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND amount BETWEEN '" + fAmount.trim() + "' AND '" + tAmount.trim() + "' ";
        } else if (fName.equals("status")) {

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND request_status = '" + fValue + "' ";
        } else if (fName.equals("trid")) {

            searchBalRequestReceiveSelectStatement = " SELECT bal_request_id, amount, bal_type, user_id, request_time, "
                    + " request_success_time, request_status, response_by, request_to, trid FROM bal_request_history "
                    + " WHERE request_to = '" + uId + "' AND trid = '" + fValue + "' ";
        }

        return searchBalRequestReceiveSelectStatement;
    }

    public static String selectMobileRechargeWatingStatusInfo() {

        String selectMobileRechargeWatingStatus = " SELECT "
                + " MRH.mobile_recharge_id, "
                + " MRH.receiver, "
                + " MRH.given_balance, "
                + " MRH.trid, "
                + " UI.user_id, "
                + " MOCI.country, "
                + " MOCI.operator, "
                + " MOCI.operator_code "
                + " FROM  "
                + " mobile_recharge_history MRH, "
                + " user_info UI, "
                + " mobile_operator_code_info MOCI "
                + " WHERE "
                + " MRH.user_id = UI.user_id "
                + " AND "
                + " MRH.operator = MOCI.operator "
                + " AND "
                + " MRH.active_status = 'W' ";

        return selectMobileRechargeWatingStatus;
    }

    public static String selectMobileRechargeSentStatusInfo() {

        String selectMobileRechargeSentStatus = " SELECT "
                + " MRH.mobile_recharge_id, "
                + " MRH.receiver, "
                + " MRH.given_balance, "
                + " MRH.trid, "
                + " UI.user_id, "
                + " MOCI.country, "
                + " MOCI.operator, "
                + " MOCI.operator_code "
                + " FROM  "
                + " mobile_recharge_history MRH, "
                + " user_info UI, "
                + " mobile_operator_code_info MOCI "
                + " WHERE "
                + " MRH.user_id = UI.user_id "
                + " AND "
                + " MRH.operator = MOCI.operator "
                + " AND "
                + " MRH.active_status = 'S' ";

        return selectMobileRechargeSentStatus;
    }

    public static String selectMobileMoneyWatingStatusInfo() {

        String selectMobileMoneyeWatingStatus = " SELECT "
                + " mobile_money_id, "
                + " receiver, "
                + " given_balance, "
                + " trid "
                + " FROM "
                + " mobile_money_history "
                + " WHERE "
                + " active_status = 'W' ";

        return selectMobileMoneyeWatingStatus;
    }

    public static String selectResellerInfoByParent(String pID) {

        String singleResellerSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.pin_conf_status, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.parent_id = '" + pID + "' ";

        return singleResellerSelectStatement;
    }

    public static String selectResellerByParent(String chldID) {

        String singleResellerSelectStatement = " SELECT "
                + " UG.group_id, "
                + " UG.group_name, "
                + " MRC.manage_rate_id, "
                + " MRC.rate_name, "
                + " MRD.mr_debit_id, "
                + " MRD.mr_cur_bal, "
                + " MRD.user_id, "
                + " MMD.mm_debit_id, "
                + " MMD.mm_cur_bal, "
                + " MMD.user_id, "
                + " UI.user_id, "
                + " UI.parent_id, "
                + " UI.user_name, "
                + " UI.contact_number1, "
                + " UI.email_address, "
                + " UI.active_status, "
                + " UI.suspend_activity, "
                + " UI.insert_by, "
                + " UI.insert_date, "
                + " UI.otp_status, "
                + " UI.enable_pin_status, "
                + " UI.api_access_status, "
                + " UI.email_conf_status, "
                + " UI.service_mrecharge, "
                + " UI.pin_conf_status, "
                + " UI.last_login, "
                + " UI.last_logout, "
                + " UI.service_mmoney "
                + " FROM  "
                + " user_info UI, "
                + " user_group UG, "
                + " mobile_recharge_debit MRD, "
                + " mobile_money_debit MMD, "
                + " manage_rate MRC "
                + " WHERE "
                + " UI.group_id = UG.group_id "
                + " AND "
                + " UI.manage_rate_id = MRC.manage_rate_id "
                + " AND "
                + " UI.user_id = MRD.user_id "
                + " AND "
                + " UI.user_id = MMD.user_id "
                + " AND "
                + " UI.parent_id = '" + chldID + "' ";

        return singleResellerSelectStatement;
    }

}
