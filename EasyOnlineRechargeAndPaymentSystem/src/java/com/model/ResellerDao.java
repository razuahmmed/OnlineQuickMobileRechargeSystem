package com.model;

import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ResellerDao {

    public int addNewResellerInfo(
            Connection connection,
            String userId,
            String parentId,
            String userName,
            String password,
            String phone,
            String email,
            String userType,
            String mobileRecharge,
            String mobileMoney,
            String rates,
            String otpStatus,
            String pinEnabledStatus,
            String apiStatus,
            String sendEmailStatus,
            String pinConfirmStatus,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        PreparedStatement ps3 = null;

        String mrDebitInsertStatement = " INSERT INTO "
                + " mobile_recharge_debit ( "
                + " user_id, "
                + " group_id, "
                + " manage_rate_id "
                + ") VALUES ( "
                + " ?,?,? "
                + " )";

        String mmDebitInsertStatement = " INSERT INTO "
                + " mobile_money_debit ( "
                + " user_id, "
                + " group_id, "
                + " manage_rate_id "
                + ") VALUES ( "
                + " ?,?,? "
                + " )";

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.insertUserInfo());

                ps.setString(1, userId);
                ps.setString(2, parentId);
                ps.setString(3, userName);
                ps.setString(4, password);
                ps.setString(5, phone);
                ps.setString(6, email);
                ps.setString(7, userType);
                ps.setString(8, mobileRecharge);
                ps.setString(9, mobileMoney);
                ps.setString(10, rates);
                ps.setString(11, otpStatus);
                ps.setString(12, pinEnabledStatus);
                ps.setString(13, apiStatus);
                ps.setString(14, sendEmailStatus);
                ps.setString(15, pinConfirmStatus);
                ps.setString(16, insertBy);

                ps2 = connection.prepareStatement(mrDebitInsertStatement);

                ps2.setString(1, userId);
                ps2.setString(2, userType);
                ps2.setString(3, rates);

                ps3 = connection.prepareStatement(mmDebitInsertStatement);

                ps3.setString(1, userId);
                ps3.setString(2, userType);
                ps3.setString(3, rates);

                status = ps2.executeUpdate();
                status = ps3.executeUpdate();
                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }

                if (ps2 != null) {
                    try {
                        ps2.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }

                if (ps3 != null) {
                    try {
                        ps3.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateResellerInfo(
            Connection connection,
            String userFullName,
            String phone,
            String email,
            String mobileRecharge,
            String mobileMoney,
            String rates,
            String otpStatus,
            String pinEnabledStatus,
            String apiStatus,
            String sendEmailStatus,
            String pinConfirmStatus,
            String updateBy,
            String resellerId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateResellerInfo(resellerId));

                ps.setString(1, userFullName);
                ps.setString(2, phone);
                ps.setString(3, email);
                ps.setString(4, mobileRecharge);
                ps.setString(5, mobileMoney);
                ps.setString(6, rates);
                ps.setString(7, otpStatus);
                ps.setString(8, pinEnabledStatus);
                ps.setString(9, apiStatus);
                ps.setString(10, sendEmailStatus);
                ps.setString(11, pinConfirmStatus);
                ps.setString(12, updateBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateSuspendStatus(
            Connection connection,
            String uId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateResellerSuspendStatus(uId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateUnSuspendStatus(
            Connection connection,
            String uId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateResellerUnSuspendStatus(uId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteResellerInfo(
            Connection connection,
            String uId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.deleteResellerStatement(uId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
