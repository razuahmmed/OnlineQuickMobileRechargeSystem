package com.model;

import com.common.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AdminDao {

    public int saveUserAdminInfo(
            Connection connection,
            String pId,
            String fullName,
            String email,
            String phone,
            String userName,
            String password,
            String userRole,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String adminUserInsertStatement = " INSERT INTO "
                    + " admin_user "
                    + " ( "
                    + " admin_parent_id, "
                    + " full_name, "
                    + " email, "
                    + " phone, "
                    + " user_name, "
                    + " password, "
                    + " admin_group_id, "
                    + " insert_by, "
                    + " insert_date "
                    + " ) VALUES ( "
                    + " ?,?,?,?,?,?,?,?, CURRENT_TIMESTAMP "
                    + " ) ";

            try {

                ps = connection.prepareStatement(adminUserInsertStatement);

                ps.setString(1, pId);
                ps.setString(2, fullName);
                ps.setString(3, email);
                ps.setString(4, phone);
                ps.setString(5, userName);
                ps.setString(6, password);
                ps.setString(7, userRole);
                ps.setString(8, insertBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateUserAdminInfo(
            Connection connection,
            String fullName,
            String email,
            String phone,
            String password,
            String userRoleList,
            String updateBy,
            Integer id) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {
                String updateStatement = " UPDATE "
                        + " admin_user "
                        + " SET "
                        + " full_name = ?, "
                        + " email = ?, "
                        + " phone = ?, "
                        + " password = ?, "
                        + " admin_group_id = ?, "
                        + " update_by = ?, "
                        + " update_date = CURRENT_TIMESTAMP "
                        + " WHERE "
                        + " admin_user_id = '" + id + "' ";

                connection = DBConnection.getMySqlConnection();
                ps = connection.prepareStatement(updateStatement);

                ps.setString(1, fullName);
                ps.setString(2, email);
                ps.setString(3, phone);
                ps.setString(4, password);
                ps.setString(5, userRoleList);
                ps.setString(6, updateBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteUserAdminInfo(
            Connection connection,
            Integer id) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String deleteStatement = " DELETE FROM "
                        + " admin_user "
                        + " WHERE "
                        + " admin_user_id = '" + id + "' ";

                ps = connection.prepareStatement(deleteStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
