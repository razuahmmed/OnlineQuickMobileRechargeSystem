package com.model;

import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PaymentsDao {

    public int addMRBalance(
            Connection connection,
            String resellerID,
            Double mrCurBal,
            Double addBal,
            String balGivenBy,
            String balDesc) {

        int status = 0;
        int status2 = 0;

        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        if (connection != null) {

            try {

                String mrAddBalTransHisInsertStatement = " INSERT INTO "
                        + " mr_transaction_history ( "
                        + " user_id, "
                        + " mr_cur_bal, "
                        + " bal_add, "
                        + " bal_given_by,"
                        + " memo, "
                        + " bal_given_date "
                        + " ) VALUES ( "
                        + "?,?,?,?,?, CURRENT_TIMESTAMP )";

                String upquery = " UPDATE "
                        + " mobile_recharge_debit "
                        + " SET "
                        + " mr_cur_bal = (mr_cur_bal + " + mrCurBal + "), "
                        + " bal_add = " + addBal + ", "
                        + " bal_given_by = '" + balGivenBy + "', "
                        + " bal_given_date = CURRENT_TIMESTAMP, "
                        + " mr_bal_description = '" + balDesc + "' "
                        + " WHERE "
                        + " user_id = '" + resellerID + "' ";

                ps = connection.prepareStatement(mrAddBalTransHisInsertStatement);
                ps2 = connection.prepareStatement(upquery);

                ps.setString(1, resellerID);
                ps.setDouble(2, mrCurBal);
                ps.setDouble(3, addBal);
                ps.setString(4, balGivenBy);
                ps.setString(5, balDesc);

                status = ps.executeUpdate();
                status2 = ps2.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
                status2 = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
                if (ps2 != null) {
                    try {
                        ps2.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateMRBalParent(
            Connection connection,
            String parentID,
            Double mrCurBal) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String upqueryparent = " UPDATE "
                        + " mobile_recharge_debit "
                        + " SET "
                        + " mr_cur_bal = (mr_cur_bal - " + mrCurBal + ") "
                        + " WHERE "
                        + " user_id = '" + parentID + "' ";

                ps = connection.prepareStatement(upqueryparent);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int returnMRBalance(
            Connection connection,
            String resellerID,
            Double mrCurBal,
            Double returnBal,
            String balReturnBy,
            String balDesc) {

        int status = 0;
        int status2 = 0;

        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        if (connection != null) {

            try {

                String mrReturnBalTransHisInsertStatement = " INSERT INTO "
                        + " mr_transaction_history ( "
                        + " user_id, "
                        + " mr_cur_bal, "
                        + " bal_return, "
                        + " bal_return_by ,"
                        + " memo, "
                        + " bal_return_date "
                        + " ) VALUES ( "
                        + "?,?,?,?,?, CURRENT_TIMESTAMP )";

                String upquery = " UPDATE "
                        + " mobile_recharge_debit "
                        + " SET "
                        + " mr_cur_bal = (mr_cur_bal - " + mrCurBal + "), "
                        + " bal_return = " + returnBal + ", "
                        + " bal_return_by = '" + balReturnBy + "', "
                        + " bal_return_date = CURRENT_TIMESTAMP, "
                        + " mr_bal_description = '" + balDesc + "' "
                        + " WHERE "
                        + " user_id = '" + resellerID + "' ";

                ps = connection.prepareStatement(mrReturnBalTransHisInsertStatement);
                ps2 = connection.prepareStatement(upquery);

                ps.setString(1, resellerID);
                ps.setDouble(2, mrCurBal);
                ps.setDouble(3, returnBal);
                ps.setString(4, balReturnBy);
                ps.setString(5, balDesc);

                status = ps.executeUpdate();
                status2 = ps2.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
                status2 = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
                if (ps2 != null) {
                    try {
                        ps2.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int returnMRBalParent(
            Connection connection,
            String parentID,
            Double mrCurBal) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String upqueryparent = " UPDATE "
                        + " mobile_recharge_debit "
                        + " SET "
                        + " mr_cur_bal = (mr_cur_bal + " + mrCurBal + ") "
                        + " WHERE "
                        + " user_id = '" + parentID + "' ";

                ps = connection.prepareStatement(upqueryparent);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int addMMBalance(
            Connection connection,
            String resellerID,
            Double mmCurBal,
            Double addBal,
            String balGivenBy,
            String balDesc) {

        int status = 0;
        int status2 = 0;

        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        if (connection != null) {

            try {

                String mmAddBalTransHisInsertStatement = " INSERT INTO "
                        + " mm_transaction_history ( "
                        + " user_id, "
                        + " mm_cur_bal, "
                        + " bal_add, "
                        + " bal_given_by ,"
                        + " memo, "
                        + " bal_given_date "
                        + " ) VALUES ( "
                        + "?,?,?,?,?, CURRENT_TIMESTAMP )";

                String upquery = " UPDATE "
                        + " mobile_money_debit "
                        + " SET "
                        + " mm_cur_bal = (mm_cur_bal + " + mmCurBal + "), "
                        + " bal_add = " + addBal + ", "
                        + " bal_given_by = '" + balGivenBy + "', "
                        + " bal_given_date = CURRENT_TIMESTAMP, "
                        + " mm_bal_description = '" + balDesc + "' "
                        + " WHERE "
                        + " user_id = '" + resellerID + "' ";

                ps = connection.prepareStatement(mmAddBalTransHisInsertStatement);
                ps2 = connection.prepareStatement(upquery);

                ps.setString(1, resellerID);
                ps.setDouble(2, mmCurBal);
                ps.setDouble(3, addBal);
                ps.setString(4, balGivenBy);
                ps.setString(5, balDesc);

                status = ps.executeUpdate();
                status2 = ps2.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
                status2 = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
                if (ps2 != null) {
                    try {
                        ps2.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int addMMBalParent(
            Connection connection,
            String parentID,
            Double mmCurBal) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String upquery = " UPDATE "
                        + " mobile_money_debit "
                        + " SET "
                        + " mm_cur_bal = (mm_cur_bal - " + mmCurBal + ") "
                        + " WHERE "
                        + " user_id = '" + parentID + "' ";

                ps = connection.prepareStatement(upquery);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int returnMMBalance(
            Connection connection,
            String resellerID,
            Double mmCurBal,
            Double returnBal,
            String balReturnBy,
            String balDesc) {

        int status = 0;
        int status2 = 0;

        PreparedStatement ps = null;
        PreparedStatement ps2 = null;

        if (connection != null) {

            try {

                String mmReturnBalTransHisInsertStatement = " INSERT INTO "
                        + " mm_transaction_history ( "
                        + " user_id, "
                        + " mm_cur_bal, "
                        + " bal_return, "
                        + " bal_return_by ,"
                        + " memo, "
                        + " bal_return_date "
                        + " ) VALUES ( "
                        + "?,?,?,?,?, CURRENT_TIMESTAMP )";

                String upquery = " UPDATE "
                        + " mobile_money_debit "
                        + " SET "
                        + " mm_cur_bal = (mm_cur_bal - " + mmCurBal + "), "
                        + " bal_return = " + returnBal + ", "
                        + " bal_return_by = '" + balReturnBy + "', "
                        + " bal_return_date = CURRENT_TIMESTAMP, "
                        + " mm_bal_description = '" + balDesc + "' "
                        + " WHERE "
                        + " user_id = '" + resellerID + "' ";

                ps = connection.prepareStatement(mmReturnBalTransHisInsertStatement);
                ps2 = connection.prepareStatement(upquery);

                ps.setString(1, resellerID);
                ps.setDouble(2, mmCurBal);
                ps.setDouble(3, returnBal);
                ps.setString(4, balReturnBy);
                ps.setString(5, balDesc);

                status = ps.executeUpdate();
                status2 = ps2.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
                status2 = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
                if (ps2 != null) {
                    try {
                        ps2.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int returnMMBalParent(
            Connection connection,
            String parentID,
            Double mmCurBal) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String upquery = " UPDATE "
                        + " mobile_money_debit "
                        + " SET "
                        + " mm_cur_bal = (mm_cur_bal + " + mmCurBal + ") "
                        + " WHERE "
                        + " user_id = '" + parentID + "' ";

                ps = connection.prepareStatement(upquery);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int requestBalanceStatusUpdate(
            Connection connection,
            String trid,
            String responseBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String balRequestHisInsertStatement = " UPDATE "
                        + " bal_request_history "
                        + " SET "
                        + " response_by = '" + responseBy + "', "
                        + " request_status = 'Y', "
                        + " request_success_time = CURRENT_TIMESTAMP "
                        + " WHERE "
                        + " trid= '" + trid + "' ";

                ps = connection.prepareStatement(balRequestHisInsertStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
