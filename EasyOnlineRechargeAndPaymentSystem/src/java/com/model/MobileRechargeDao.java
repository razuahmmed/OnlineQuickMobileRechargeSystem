package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MobileRechargeDao {

    public int saveCsvFileMR(
            Connection connection,
            String sender,
            String receiver,
            String operator,
            Integer type,
            Integer amount,
            Integer modemNo,
            String uID) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String csvSendMRInsertStatement = " INSERT INTO "
                    + " mobile_recharge_history "
                    + " ( "
                    + " purchased_on, "
                    + " sender, "
                    + " receiver, "
                    + " operator, "
                    + " type, "
                    + " given_balance, "
                    + " originator, "
                    + " user_id, "
                    + " mrd_id "
                    + " ) VALUES ( "
                    + " CURRENT_TIMESTAMP ,?,?,?,?,?,?,?, (SELECT mr_debit_id FROM mobile_recharge_debit WHERE user_id = '" + uID + "') "
                    + " ) ";

            try {

                ps = connection.prepareStatement(csvSendMRInsertStatement);

                ps.setString(1, sender);
                ps.setString(2, receiver);
                ps.setString(3, operator);
                ps.setInt(4, type);
                ps.setDouble(5, amount);
                ps.setInt(6, modemNo);
                ps.setString(7, uID);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int sendMRByGroup(
            Connection connection,
            String sender,
            String receiver,
            String operator,
            String type,
            String amount,
            String modemNo,
            String uID) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String sendMRByGroupInsertStatement = " INSERT INTO "
                    + " mobile_recharge_history "
                    + " ( "
                    + " purchased_on, "
                    + " sender, "
                    + " receiver, "
                    + " operator, "
                    + " type, "
                    + " given_balance, "
                    + " originator, "
                    + " user_id, "
                    + " mrd_id "
                    + " ) VALUES ( "
                    + " CURRENT_TIMESTAMP ,?,?,?,?,?,?,?, (SELECT mr_debit_id FROM mobile_recharge_debit WHERE user_id = '" + uID + "') "
                    + " ) ";

            try {

                ps = connection.prepareStatement(sendMRByGroupInsertStatement);

                ps.setString(1, sender);
                ps.setString(2, receiver);
                ps.setString(3, operator);
                ps.setString(4, type);
                ps.setString(5, amount);
                ps.setString(6, modemNo);
                ps.setString(7, uID);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveMRMGroup(
            Connection connection,
            String mobileNo,
            Integer type,
            Double amount,
            Integer modemNo,
            String userId,
            Integer fk) {

        int status = 0;

        PreparedStatement psGInfo = null;

        String mrGroupInfoInsertStatement = " INSERT INTO "
                + " mr_group_info "
                + " ( "
                + " mobile_no, "
                + " type, "
                + " amount, "
                + " modem_no, "
                + " user_id, "
                + " mr_gname_id "
                + " ) VALUES ( "
                + " ?,?,?,?,?,? "
                + " ) ";

        if (connection != null) {

            try {

                psGInfo = connection.prepareStatement(mrGroupInfoInsertStatement);

                psGInfo.setString(1, mobileNo);
                psGInfo.setInt(2, type);
                psGInfo.setDouble(3, amount);
                psGInfo.setInt(4, modemNo);
                psGInfo.setString(5, userId);
                psGInfo.setInt(6, fk);

                status = psGInfo.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (psGInfo != null) {
                    try {
                        psGInfo.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateMRGroupInfo(
            Connection connection,
            String mobileNo,
            String type,
            String amount,
            String modemNo,
            String userId,
            Integer fk) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String mrGroupInfoInsertStatement = " INSERT INTO "
                    + " mr_group_info "
                    + " ( "
                    + " mobile_no, "
                    + " type, "
                    + " amount, "
                    + " modem_no, "
                    + " user_id, "
                    + " mr_gname_id "
                    + " ) VALUES ( "
                    + " ?,?,?,?,?,? "
                    + " ) ";

//            String mrGroupUpdateStatement = " UPDATE "
//                    + " mr_group_info MRGI "
//                    + " SET "
//                    + " MRGI.mobile_no = '" + mobileNo + "', "
//                    + " MRGI.type = '" + type + "', "
//                    + " MRGI.amount = '" + amount + "', "
//                    + " MRGI.modem_no = '" + modemNo + "' "
//                    + " WHERE "
//                    + " MRGI.user_id = '" + userId + "' "
//                    + " AND "
//                    + " MRGI.mr_group_id = '" + gId + "' ";
            try {

                ps = connection.prepareStatement(mrGroupInfoInsertStatement);

                ps.setString(1, mobileNo);
                ps.setString(2, type);
                ps.setString(3, amount);
                ps.setString(4, modemNo);
                ps.setString(5, userId);
                ps.setInt(6, fk);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteMRGroupInfo(
            Connection connection,
            Integer gId,
            String userId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String mrGroupDeleteStatement = " DELETE "
                    + " mr_group_name, "
                    + " mr_group_info "
                    + " FROM "
                    + " mr_group_name "
                    + " INNER JOIN "
                    + " mr_group_info "
                    + " WHERE "
                    + " mr_group_name.mr_gname_id = mr_group_info.mr_gname_id "
                    + " AND "
                    + " mr_group_name.user_id = '" + userId + "' "
                    + " AND "
                    + " mr_group_name.mr_gname_id = '" + gId + "' ";

            try {

                ps = connection.prepareStatement(mrGroupDeleteStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveMobileRecharge(
            Connection connection,
            String sender,
            String mobileNo,
            Integer type,
            Double amount,
            String operator,
            String trid,
            String uID) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String mrInsertStatement = " INSERT INTO "
                    + " mobile_recharge_history "
                    + " ( "
                    + " purchased_on, "
                    + " sender, "
                    + " receiver, "
                    + " type, "
                    + " given_balance, "
                    + " operator, "
                    + " user_id, "
                    + " trid, "
                    + " mrd_id "
                    + " ) VALUES ( "
                    + " CURRENT_TIMESTAMP ,?,?,?,?,?,?,?,(SELECT mr_debit_id FROM mobile_recharge_debit WHERE user_id = '" + uID + "') "
                    + " ) ";

            try {

                ps = connection.prepareStatement(mrInsertStatement);

                ps.setString(1, sender);
                ps.setString(2, mobileNo);
                ps.setInt(3, type);
                ps.setDouble(4, amount);
                ps.setString(5, operator);
                ps.setString(6, uID);
                ps.setString(7, trid);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveContacts(
            Connection connection,
            String contactName,
            String contactNumber,
            String userId,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String scInsertStatement = " INSERT INTO "
                    + " save_contact "
                    + " ( "
                    + " contact_name, "
                    + " contact_number, "
                    + " user_id, "
                    + " insert_by, "
                    + " insert_date "
                    + " ) VALUES ( "
                    + " ?,?,?,?, CURRENT_TIMESTAMP "
                    + " ) ";

            try {

                ps = connection.prepareStatement(scInsertStatement);

                ps.setString(1, contactName);
                ps.setString(2, contactNumber);
                ps.setString(3, userId);
                ps.setString(4, insertBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteContacts(
            Connection connection,
            Integer cId,
            String userId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String scDeleteStatement = " DELETE FROM "
                    + " save_contact "
                    + " WHERE "
                    + " user_id = '" + userId + "' "
                    + " AND "
                    + " contact_id = '" + cId + "' ";

            try {

                ps = connection.prepareStatement(scDeleteStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
