package com.persistance;

public class ChildUserInfo {

    private String chldUserId;
    private String chldParentId;
    private String chldUserName;
    private String chldUserCompanyName;
    private String chldUserPassword;
    private String chldContactNumber1;
    private String chldContactNumber2;
    private String chldEmailAddress;
    private String chldAddress1;
    private String chldAddress2;
    private UserGroup chldUserGroupInfo;
    private ManageRate chldManageRateInfo;
    private MobileRechargeDebit chldMobileRechargeDebitInfo;
    private MobileMoneyDebit chldMobileMoneyDebitInfo;
    private CompanyInfo chldMompanyInfo;
    private String chldGroupDescription;
    private Character chldActiveStatus;
    private String chldInsertBy;
    private String chldInsertDate;
    private String chldUpdateBy;
    private String chldUpdateDate;
    private String chldServiceMRecharge;
    private String chldServiceMMoney;
    private Character chldOtpStatus;
    private String chldEnablePinStatus;
    private Character chldApiAccessStatus;
    private Character chldEmailConfirmationStatus;
    private String chldPinConfirmation;
    private String chldLastLogin;
    private String chldLastLogout;
    private Character chldSuspendActivity;

    /**
     * @return the chldUserId
     */
    public String getChldUserId() {
        return chldUserId;
    }

    /**
     * @param chldUserId the chldUserId to set
     */
    public void setChldUserId(String chldUserId) {
        this.chldUserId = chldUserId;
    }

    /**
     * @return the chldParentId
     */
    public String getChldParentId() {
        return chldParentId;
    }

    /**
     * @param chldParentId the chldParentId to set
     */
    public void setChldParentId(String chldParentId) {
        this.chldParentId = chldParentId;
    }

    /**
     * @return the chldUserName
     */
    public String getChldUserName() {
        return chldUserName;
    }

    /**
     * @param chldUserName the chldUserName to set
     */
    public void setChldUserName(String chldUserName) {
        this.chldUserName = chldUserName;
    }

    /**
     * @return the chldUserCompanyName
     */
    public String getChldUserCompanyName() {
        return chldUserCompanyName;
    }

    /**
     * @param chldUserCompanyName the chldUserCompanyName to set
     */
    public void setChldUserCompanyName(String chldUserCompanyName) {
        this.chldUserCompanyName = chldUserCompanyName;
    }

    /**
     * @return the chldUserPassword
     */
    public String getChldUserPassword() {
        return chldUserPassword;
    }

    /**
     * @param chldUserPassword the chldUserPassword to set
     */
    public void setChldUserPassword(String chldUserPassword) {
        this.chldUserPassword = chldUserPassword;
    }

    /**
     * @return the chldContactNumber1
     */
    public String getChldContactNumber1() {
        return chldContactNumber1;
    }

    /**
     * @param chldContactNumber1 the chldContactNumber1 to set
     */
    public void setChldContactNumber1(String chldContactNumber1) {
        this.chldContactNumber1 = chldContactNumber1;
    }

    /**
     * @return the chldContactNumber2
     */
    public String getChldContactNumber2() {
        return chldContactNumber2;
    }

    /**
     * @param chldContactNumber2 the chldContactNumber2 to set
     */
    public void setChldContactNumber2(String chldContactNumber2) {
        this.chldContactNumber2 = chldContactNumber2;
    }

    /**
     * @return the chldEmailAddress
     */
    public String getChldEmailAddress() {
        return chldEmailAddress;
    }

    /**
     * @param chldEmailAddress the chldEmailAddress to set
     */
    public void setChldEmailAddress(String chldEmailAddress) {
        this.chldEmailAddress = chldEmailAddress;
    }

    /**
     * @return the chldAddress1
     */
    public String getChldAddress1() {
        return chldAddress1;
    }

    /**
     * @param chldAddress1 the chldAddress1 to set
     */
    public void setChldAddress1(String chldAddress1) {
        this.chldAddress1 = chldAddress1;
    }

    /**
     * @return the chldAddress2
     */
    public String getChldAddress2() {
        return chldAddress2;
    }

    /**
     * @param chldAddress2 the chldAddress2 to set
     */
    public void setChldAddress2(String chldAddress2) {
        this.chldAddress2 = chldAddress2;
    }

    /**
     * @return the chldUserGroupInfo
     */
    public UserGroup getChldUserGroupInfo() {
        return chldUserGroupInfo;
    }

    /**
     * @param chldUserGroupInfo the chldUserGroupInfo to set
     */
    public void setChldUserGroupInfo(UserGroup chldUserGroupInfo) {
        this.chldUserGroupInfo = chldUserGroupInfo;
    }

    /**
     * @return the chldManageRateInfo
     */
    public ManageRate getChldManageRateInfo() {
        return chldManageRateInfo;
    }

    /**
     * @param chldManageRateInfo the chldManageRateInfo to set
     */
    public void setChldManageRateInfo(ManageRate chldManageRateInfo) {
        this.chldManageRateInfo = chldManageRateInfo;
    }

    /**
     * @return the chldMobileRechargeDebitInfo
     */
    public MobileRechargeDebit getChldMobileRechargeDebitInfo() {
        return chldMobileRechargeDebitInfo;
    }

    /**
     * @param chldMobileRechargeDebitInfo the chldMobileRechargeDebitInfo to set
     */
    public void setChldMobileRechargeDebitInfo(MobileRechargeDebit chldMobileRechargeDebitInfo) {
        this.chldMobileRechargeDebitInfo = chldMobileRechargeDebitInfo;
    }

    /**
     * @return the chldMobileMoneyDebitInfo
     */
    public MobileMoneyDebit getChldMobileMoneyDebitInfo() {
        return chldMobileMoneyDebitInfo;
    }

    /**
     * @param chldMobileMoneyDebitInfo the chldMobileMoneyDebitInfo to set
     */
    public void setChldMobileMoneyDebitInfo(MobileMoneyDebit chldMobileMoneyDebitInfo) {
        this.chldMobileMoneyDebitInfo = chldMobileMoneyDebitInfo;
    }

    /**
     * @return the chldMompanyInfo
     */
    public CompanyInfo getChldMompanyInfo() {
        return chldMompanyInfo;
    }

    /**
     * @param chldMompanyInfo the chldMompanyInfo to set
     */
    public void setChldMompanyInfo(CompanyInfo chldMompanyInfo) {
        this.chldMompanyInfo = chldMompanyInfo;
    }

    /**
     * @return the chldGroupDescription
     */
    public String getChldGroupDescription() {
        return chldGroupDescription;
    }

    /**
     * @param chldGroupDescription the chldGroupDescription to set
     */
    public void setChldGroupDescription(String chldGroupDescription) {
        this.chldGroupDescription = chldGroupDescription;
    }

    /**
     * @return the chldActiveStatus
     */
    public Character getChldActiveStatus() {
        return chldActiveStatus;
    }

    /**
     * @param chldActiveStatus the chldActiveStatus to set
     */
    public void setChldActiveStatus(Character chldActiveStatus) {
        this.chldActiveStatus = chldActiveStatus;
    }

    /**
     * @return the chldInsertBy
     */
    public String getChldInsertBy() {
        return chldInsertBy;
    }

    /**
     * @param chldInsertBy the chldInsertBy to set
     */
    public void setChldInsertBy(String chldInsertBy) {
        this.chldInsertBy = chldInsertBy;
    }

    /**
     * @return the chldInsertDate
     */
    public String getChldInsertDate() {
        return chldInsertDate;
    }

    /**
     * @param chldInsertDate the chldInsertDate to set
     */
    public void setChldInsertDate(String chldInsertDate) {
        this.chldInsertDate = chldInsertDate;
    }

    /**
     * @return the chldUpdateBy
     */
    public String getChldUpdateBy() {
        return chldUpdateBy;
    }

    /**
     * @param chldUpdateBy the chldUpdateBy to set
     */
    public void setChldUpdateBy(String chldUpdateBy) {
        this.chldUpdateBy = chldUpdateBy;
    }

    /**
     * @return the chldUpdateDate
     */
    public String getChldUpdateDate() {
        return chldUpdateDate;
    }

    /**
     * @param chldUpdateDate the chldUpdateDate to set
     */
    public void setChldUpdateDate(String chldUpdateDate) {
        this.chldUpdateDate = chldUpdateDate;
    }

    /**
     * @return the chldServiceMRecharge
     */
    public String getChldServiceMRecharge() {
        return chldServiceMRecharge;
    }

    /**
     * @param chldServiceMRecharge the chldServiceMRecharge to set
     */
    public void setChldServiceMRecharge(String chldServiceMRecharge) {
        this.chldServiceMRecharge = chldServiceMRecharge;
    }

    /**
     * @return the chldServiceMMoney
     */
    public String getChldServiceMMoney() {
        return chldServiceMMoney;
    }

    /**
     * @param chldServiceMMoney the chldServiceMMoney to set
     */
    public void setChldServiceMMoney(String chldServiceMMoney) {
        this.chldServiceMMoney = chldServiceMMoney;
    }

    /**
     * @return the chldOtpStatus
     */
    public Character getChldOtpStatus() {
        return chldOtpStatus;
    }

    /**
     * @param chldOtpStatus the chldOtpStatus to set
     */
    public void setChldOtpStatus(Character chldOtpStatus) {
        this.chldOtpStatus = chldOtpStatus;
    }

    /**
     * @return the chldEnablePinStatus
     */
    public String getChldEnablePinStatus() {
        return chldEnablePinStatus;
    }

    /**
     * @param chldEnablePinStatus the chldEnablePinStatus to set
     */
    public void setChldEnablePinStatus(String chldEnablePinStatus) {
        this.chldEnablePinStatus = chldEnablePinStatus;
    }

    /**
     * @return the chldApiAccessStatus
     */
    public Character getChldApiAccessStatus() {
        return chldApiAccessStatus;
    }

    /**
     * @param chldApiAccessStatus the chldApiAccessStatus to set
     */
    public void setChldApiAccessStatus(Character chldApiAccessStatus) {
        this.chldApiAccessStatus = chldApiAccessStatus;
    }

    /**
     * @return the chldEmailConfirmationStatus
     */
    public Character getChldEmailConfirmationStatus() {
        return chldEmailConfirmationStatus;
    }

    /**
     * @param chldEmailConfirmationStatus the chldEmailConfirmationStatus to set
     */
    public void setChldEmailConfirmationStatus(Character chldEmailConfirmationStatus) {
        this.chldEmailConfirmationStatus = chldEmailConfirmationStatus;
    }

    /**
     * @return the chldPinConfirmation
     */
    public String getChldPinConfirmation() {
        return chldPinConfirmation;
    }

    /**
     * @param chldPinConfirmation the chldPinConfirmation to set
     */
    public void setChldPinConfirmation(String chldPinConfirmation) {
        this.chldPinConfirmation = chldPinConfirmation;
    }

    /**
     * @return the chldLastLogin
     */
    public String getChldLastLogin() {
        return chldLastLogin;
    }

    /**
     * @param chldLastLogin the chldLastLogin to set
     */
    public void setChldLastLogin(String chldLastLogin) {
        this.chldLastLogin = chldLastLogin;
    }

    /**
     * @return the chldLastLogout
     */
    public String getChldLastLogout() {
        return chldLastLogout;
    }

    /**
     * @param chldLastLogout the chldLastLogout to set
     */
    public void setChldLastLogout(String chldLastLogout) {
        this.chldLastLogout = chldLastLogout;
    }

    /**
     * @return the chldSuspendActivity
     */
    public Character getChldSuspendActivity() {
        return chldSuspendActivity;
    }

    /**
     * @param chldSuspendActivity the chldSuspendActivity to set
     */
    public void setChldSuspendActivity(Character chldSuspendActivity) {
        this.chldSuspendActivity = chldSuspendActivity;
    }
}
