package com.persistance;

public class MobileOperatorCodeInfo {

    private Integer operatorCodeID;
    private String country;
    private String operator;
    private String operatorCode;
    private Character codeStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;

    @Override
    public String toString() {
        return "MobileOperatorCodeInfo{" + "operatorCodeID=" + operatorCodeID + ", country=" + country + ", operator=" + operator + ", operatorCode=" + operatorCode + ", codeStatus=" + codeStatus + ", insertBy=" + insertBy + ", insertDate=" + insertDate + ", updateBy=" + updateBy + ", updateDate=" + updateDate + '}';
    }

    /**
     * @return the operatorCodeID
     */
    public Integer getOperatorCodeID() {
        return operatorCodeID;
    }

    /**
     * @param operatorCodeID the operatorCodeID to set
     */
    public void setOperatorCodeID(Integer operatorCodeID) {
        this.operatorCodeID = operatorCodeID;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the operatorCode
     */
    public String getOperatorCode() {
        return operatorCode;
    }

    /**
     * @param operatorCode the operatorCode to set
     */
    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    /**
     * @return the codeStatus
     */
    public Character getCodeStatus() {
        return codeStatus;
    }

    /**
     * @param codeStatus the codeStatus to set
     */
    public void setCodeStatus(Character codeStatus) {
        this.codeStatus = codeStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
