package com.persistance;

import java.util.Date;

public class UserLevel {

    private Integer levelId;
    private String levelName;
    private char status;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;
    private String comCode;
    private String levelShortName;
    private String levelDescription;

    /**
     * @return the levelId
     */
    public Integer getLevelId() {
        return levelId;
    }

    /**
     * @param levelId the levelId to set
     */
    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    /**
     * @return the levelName
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * @param levelName the levelName to set
     */
    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    /**
     * @return the status
     */
    public char getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(char status) {
        this.status = status;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the comCode
     */
    public String getComCode() {
        return comCode;
    }

    /**
     * @param comCode the comCode to set
     */
    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    /**
     * @return the levelShortName
     */
    public String getLevelShortName() {
        return levelShortName;
    }

    /**
     * @param levelShortName the levelShortName to set
     */
    public void setLevelShortName(String levelShortName) {
        this.levelShortName = levelShortName;
    }

    /**
     * @return the levelDescription
     */
    public String getLevelDescription() {
        return levelDescription;
    }

    /**
     * @param levelDescription the levelDescription to set
     */
    public void setLevelDescription(String levelDescription) {
        this.levelDescription = levelDescription;
    }
}
