package com.persistance;

import java.util.Date;

public class AdminUser {

    private Integer adminUserId;
    private String adminParentId;
    private String fullName;
    private String email;
    private String phone;
    private String userName;
    private String password;
    private AdminGroup adminGroupInfo;
    private Character activeStatus;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;

    /**
     * @return the adminUserId
     */
    public Integer getAdminUserId() {
        return adminUserId;
    }

    /**
     * @param adminUserId the adminUserId to set
     */
    public void setAdminUserId(Integer adminUserId) {
        this.adminUserId = adminUserId;
    }

    /**
     * @return the adminParentId
     */
    public String getAdminParentId() {
        return adminParentId;
    }

    /**
     * @param adminParentId the adminParentId to set
     */
    public void setAdminParentId(String adminParentId) {
        this.adminParentId = adminParentId;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the adminGroupInfo
     */
    public AdminGroup getAdminGroupInfo() {
        return adminGroupInfo;
    }

    /**
     * @param adminGroupInfo the adminGroupInfo to set
     */
    public void setAdminGroupInfo(AdminGroup adminGroupInfo) {
        this.adminGroupInfo = adminGroupInfo;
    }

    /**
     * @return the activeStatus
     */
    public Character getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(Character activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
