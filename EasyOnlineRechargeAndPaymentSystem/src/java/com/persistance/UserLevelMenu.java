package com.persistance;

import java.util.Date;
import java.util.List;

public class UserLevelMenu {

    private Integer userLevelMenuId;
    private String menuId;
    private Integer levelId;
    private ParentMenu parentMenuInfo;
    private ChildMenu childMenu;
    private List<ChildMenu> userLevelChildMenuList;
    private Character levelMenuStatus;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;
    private String comCode;
    private UserInfo userInfo;

    /**
     * @return the userLevelMenuId
     */
    public Integer getUserLevelMenuId() {
        return userLevelMenuId;
    }

    /**
     * @param userLevelMenuId the userLevelMenuId to set
     */
    public void setUserLevelMenuId(Integer userLevelMenuId) {
        this.userLevelMenuId = userLevelMenuId;
    }

    /**
     * @return the menuId
     */
    public String getMenuId() {
        return menuId;
    }

    /**
     * @param menuId the menuId to set
     */
    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    /**
     * @return the levelId
     */
    public Integer getLevelId() {
        return levelId;
    }

    /**
     * @param levelId the levelId to set
     */
    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    /**
     * @return the parentMenuInfo
     */
    public ParentMenu getParentMenuInfo() {
        return parentMenuInfo;
    }

    /**
     * @param parentMenuInfo the parentMenuInfo to set
     */
    public void setParentMenuInfo(ParentMenu parentMenuInfo) {
        this.parentMenuInfo = parentMenuInfo;
    }

    /**
     * @return the userLevelChildMenuList
     */
    public List<ChildMenu> getUserLevelChildMenuList() {
        return userLevelChildMenuList;
    }

    /**
     * @param userLevelChildMenuList the userLevelChildMenuList to set
     */
    public void setUserLevelChildMenuList(List<ChildMenu> userLevelChildMenuList) {
        this.userLevelChildMenuList = userLevelChildMenuList;
    }

    /**
     * @return the levelMenuStatus
     */
    public Character getLevelMenuStatus() {
        return levelMenuStatus;
    }

    /**
     * @param levelMenuStatus the levelMenuStatus to set
     */
    public void setLevelMenuStatus(Character levelMenuStatus) {
        this.levelMenuStatus = levelMenuStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the comCode
     */
    public String getComCode() {
        return comCode;
    }

    /**
     * @param comCode the comCode to set
     */
    public void setComCode(String comCode) {
        this.comCode = comCode;
    }

    /**
     * @return the childMenu
     */
    public ChildMenu getChildMenu() {
        return childMenu;
    }

    /**
     * @param childMenu the childMenu to set
     */
    public void setChildMenu(ChildMenu childMenu) {
        this.childMenu = childMenu;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
