package com.persistance;

import java.util.Date;
import java.util.List;

public class ParentMenu {

    private Integer menuId;
    private String parentMenuName;
    private String parentMenuTitle;
    private Integer parentMenuId;
    private String parentMenuUrl;
    private String parentMenuType;
    private char parentMenuStatus;
    private String parentMenuInsertBy;
    private Date parentMenuInsertDate;
    private String parentMenuUpdateBy;
    private Date parentMenuUpdateDate;

    private List<ChildMenu> childMenuList;

    /**
     * @return the menuId
     */
    public Integer getMenuId() {
        return menuId;
    }

    /**
     * @param menuId the menuId to set
     */
    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    /**
     * @return the parentMenuName
     */
    public String getParentMenuName() {
        return parentMenuName;
    }

    /**
     * @param parentMenuName the parentMenuName to set
     */
    public void setParentMenuName(String parentMenuName) {
        this.parentMenuName = parentMenuName;
    }

    /**
     * @return the parentMenuTitle
     */
    public String getParentMenuTitle() {
        return parentMenuTitle;
    }

    /**
     * @param parentMenuTitle the parentMenuTitle to set
     */
    public void setParentMenuTitle(String parentMenuTitle) {
        this.parentMenuTitle = parentMenuTitle;
    }

    /**
     * @return the parentMenuId
     */
    public Integer getParentMenuId() {
        return parentMenuId;
    }

    /**
     * @param parentMenuId the parentMenuId to set
     */
    public void setParentMenuId(Integer parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    /**
     * @return the parentMenuUrl
     */
    public String getParentMenuUrl() {
        return parentMenuUrl;
    }

    /**
     * @param parentMenuUrl the parentMenuUrl to set
     */
    public void setParentMenuUrl(String parentMenuUrl) {
        this.parentMenuUrl = parentMenuUrl;
    }

    /**
     * @return the parentMenuType
     */
    public String getParentMenuType() {
        return parentMenuType;
    }

    /**
     * @param parentMenuType the parentMenuType to set
     */
    public void setParentMenuType(String parentMenuType) {
        this.parentMenuType = parentMenuType;
    }

    /**
     * @return the parentMenuStatus
     */
    public char getParentMenuStatus() {
        return parentMenuStatus;
    }

    /**
     * @param parentMenuStatus the parentMenuStatus to set
     */
    public void setParentMenuStatus(char parentMenuStatus) {
        this.parentMenuStatus = parentMenuStatus;
    }

    /**
     * @return the parentMenuInsertBy
     */
    public String getParentMenuInsertBy() {
        return parentMenuInsertBy;
    }

    /**
     * @param parentMenuInsertBy the parentMenuInsertBy to set
     */
    public void setParentMenuInsertBy(String parentMenuInsertBy) {
        this.parentMenuInsertBy = parentMenuInsertBy;
    }

    /**
     * @return the parentMenuInsertDate
     */
    public Date getParentMenuInsertDate() {
        return parentMenuInsertDate;
    }

    /**
     * @param parentMenuInsertDate the parentMenuInsertDate to set
     */
    public void setParentMenuInsertDate(Date parentMenuInsertDate) {
        this.parentMenuInsertDate = parentMenuInsertDate;
    }

    /**
     * @return the parentMenuUpdateBy
     */
    public String getParentMenuUpdateBy() {
        return parentMenuUpdateBy;
    }

    /**
     * @param parentMenuUpdateBy the parentMenuUpdateBy to set
     */
    public void setParentMenuUpdateBy(String parentMenuUpdateBy) {
        this.parentMenuUpdateBy = parentMenuUpdateBy;
    }

    /**
     * @return the parentMenuUpdateDate
     */
    public Date getParentMenuUpdateDate() {
        return parentMenuUpdateDate;
    }

    /**
     * @param parentMenuUpdateDate the parentMenuUpdateDate to set
     */
    public void setParentMenuUpdateDate(Date parentMenuUpdateDate) {
        this.parentMenuUpdateDate = parentMenuUpdateDate;
    }

    /**
     * @return the childMenuList
     */
    public List<ChildMenu> getChildMenuList() {
        return childMenuList;
    }

    /**
     * @param childMenuList the childMenuList to set
     */
    public void setChildMenuList(List<ChildMenu> childMenuList) {
        this.childMenuList = childMenuList;
    }
}
