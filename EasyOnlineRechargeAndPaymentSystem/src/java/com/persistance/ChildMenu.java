package com.persistance;

import java.util.Date;

public class ChildMenu {

    private Integer childMenuId;
    private String childMenuName;
    private String childMenuTitle;
    private Integer parentMenuId;
    private String childMenuUrl;
    private String childMenuType;
    private Character childMenuStatus;
    private String childMenuInsertBy;
    private Date childMenuInsertDate;
    private String childMenuUpdateBy;
    private Date childMenuUpdateDate;
    private UserLevelMenu userLevelMenuInfo;

    /**
     * @return the childMenuId
     */
    public Integer getChildMenuId() {
        return childMenuId;
    }

    /**
     * @param childMenuId the childMenuId to set
     */
    public void setChildMenuId(Integer childMenuId) {
        this.childMenuId = childMenuId;
    }

    /**
     * @return the childMenuName
     */
    public String getChildMenuName() {
        return childMenuName;
    }

    /**
     * @param childMenuName the childMenuName to set
     */
    public void setChildMenuName(String childMenuName) {
        this.childMenuName = childMenuName;
    }

    /**
     * @return the childMenuTitle
     */
    public String getChildMenuTitle() {
        return childMenuTitle;
    }

    /**
     * @param childMenuTitle the childMenuTitle to set
     */
    public void setChildMenuTitle(String childMenuTitle) {
        this.childMenuTitle = childMenuTitle;
    }

    /**
     * @return the parentMenuId
     */
    public Integer getParentMenuId() {
        return parentMenuId;
    }

    /**
     * @param parentMenuId the parentMenuId to set
     */
    public void setParentMenuId(Integer parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    /**
     * @return the childMenuUrl
     */
    public String getChildMenuUrl() {
        return childMenuUrl;
    }

    /**
     * @param childMenuUrl the childMenuUrl to set
     */
    public void setChildMenuUrl(String childMenuUrl) {
        this.childMenuUrl = childMenuUrl;
    }

    /**
     * @return the childMenuType
     */
    public String getChildMenuType() {
        return childMenuType;
    }

    /**
     * @param childMenuType the childMenuType to set
     */
    public void setChildMenuType(String childMenuType) {
        this.childMenuType = childMenuType;
    }

    /**
     * @return the childMenuStatus
     */
    public Character getChildMenuStatus() {
        return childMenuStatus;
    }

    /**
     * @param childMenuStatus the childMenuStatus to set
     */
    public void setChildMenuStatus(Character childMenuStatus) {
        this.childMenuStatus = childMenuStatus;
    }

    /**
     * @return the childMenuInsertBy
     */
    public String getChildMenuInsertBy() {
        return childMenuInsertBy;
    }

    /**
     * @param childMenuInsertBy the childMenuInsertBy to set
     */
    public void setChildMenuInsertBy(String childMenuInsertBy) {
        this.childMenuInsertBy = childMenuInsertBy;
    }

    /**
     * @return the childMenuInsertDate
     */
    public Date getChildMenuInsertDate() {
        return childMenuInsertDate;
    }

    /**
     * @param childMenuInsertDate the childMenuInsertDate to set
     */
    public void setChildMenuInsertDate(Date childMenuInsertDate) {
        this.childMenuInsertDate = childMenuInsertDate;
    }

    /**
     * @return the childMenuUpdateBy
     */
    public String getChildMenuUpdateBy() {
        return childMenuUpdateBy;
    }

    /**
     * @param childMenuUpdateBy the childMenuUpdateBy to set
     */
    public void setChildMenuUpdateBy(String childMenuUpdateBy) {
        this.childMenuUpdateBy = childMenuUpdateBy;
    }

    /**
     * @return the childMenuUpdateDate
     */
    public Date getChildMenuUpdateDate() {
        return childMenuUpdateDate;
    }

    /**
     * @param childMenuUpdateDate the childMenuUpdateDate to set
     */
    public void setChildMenuUpdateDate(Date childMenuUpdateDate) {
        this.childMenuUpdateDate = childMenuUpdateDate;
    }

    /**
     * @return the userLevelMenuInfo
     */
    public UserLevelMenu getUserLevelMenuInfo() {
        return userLevelMenuInfo;
    }

    /**
     * @param userLevelMenuInfo the userLevelMenuInfo to set
     */
    public void setUserLevelMenuInfo(UserLevelMenu userLevelMenuInfo) {
        this.userLevelMenuInfo = userLevelMenuInfo;
    }
}
