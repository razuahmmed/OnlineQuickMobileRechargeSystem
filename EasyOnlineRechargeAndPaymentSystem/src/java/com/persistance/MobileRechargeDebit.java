package com.persistance;

public class MobileRechargeDebit {

    private Integer mobileRechargeDebitId;
    private Double mrCurrentBalance;
    private Double addBalance;
    private Double returnBalance;
    private UserInfo userInfo;
    private UserGroup userGroupInfo;
    private ManageRate manageRateInfo;
    private String balanceGivenBy;
    private String balanceReturnBy;
    private String balanceGivenDate;
    private String balanceReturnDate;
    private String mrBalanceDescription;
    private Character type;
    private String trid;
    private Double oldBalance;

    /**
     * @return the mobileRechargeDebitId
     */
    public Integer getMobileRechargeDebitId() {
        return mobileRechargeDebitId;
    }

    /**
     * @param mobileRechargeDebitId the mobileRechargeDebitId to set
     */
    public void setMobileRechargeDebitId(Integer mobileRechargeDebitId) {
        this.mobileRechargeDebitId = mobileRechargeDebitId;
    }

    /**
     * @return the mrCurrentBalance
     */
    public Double getMrCurrentBalance() {
        return mrCurrentBalance;
    }

    /**
     * @param mrCurrentBalance the mrCurrentBalance to set
     */
    public void setMrCurrentBalance(Double mrCurrentBalance) {
        this.mrCurrentBalance = mrCurrentBalance;
    }

    /**
     * @return the addBalance
     */
    public Double getAddBalance() {
        return addBalance;
    }

    /**
     * @param addBalance the addBalance to set
     */
    public void setAddBalance(Double addBalance) {
        this.addBalance = addBalance;
    }

    /**
     * @return the returnBalance
     */
    public Double getReturnBalance() {
        return returnBalance;
    }

    /**
     * @param returnBalance the returnBalance to set
     */
    public void setReturnBalance(Double returnBalance) {
        this.returnBalance = returnBalance;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the userGroupInfo
     */
    public UserGroup getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * @param userGroupInfo the userGroupInfo to set
     */
    public void setUserGroupInfo(UserGroup userGroupInfo) {
        this.userGroupInfo = userGroupInfo;
    }

    /**
     * @return the manageRateInfo
     */
    public ManageRate getManageRateInfo() {
        return manageRateInfo;
    }

    /**
     * @param manageRateInfo the manageRateInfo to set
     */
    public void setManageRateInfo(ManageRate manageRateInfo) {
        this.manageRateInfo = manageRateInfo;
    }

    /**
     * @return the balanceGivenBy
     */
    public String getBalanceGivenBy() {
        return balanceGivenBy;
    }

    /**
     * @param balanceGivenBy the balanceGivenBy to set
     */
    public void setBalanceGivenBy(String balanceGivenBy) {
        this.balanceGivenBy = balanceGivenBy;
    }

    /**
     * @return the balanceReturnBy
     */
    public String getBalanceReturnBy() {
        return balanceReturnBy;
    }

    /**
     * @param balanceReturnBy the balanceReturnBy to set
     */
    public void setBalanceReturnBy(String balanceReturnBy) {
        this.balanceReturnBy = balanceReturnBy;
    }

    /**
     * @return the balanceGivenDate
     */
    public String getBalanceGivenDate() {
        return balanceGivenDate;
    }

    /**
     * @param balanceGivenDate the balanceGivenDate to set
     */
    public void setBalanceGivenDate(String balanceGivenDate) {
        this.balanceGivenDate = balanceGivenDate;
    }

    /**
     * @return the balanceReturnDate
     */
    public String getBalanceReturnDate() {
        return balanceReturnDate;
    }

    /**
     * @param balanceReturnDate the balanceReturnDate to set
     */
    public void setBalanceReturnDate(String balanceReturnDate) {
        this.balanceReturnDate = balanceReturnDate;
    }

    /**
     * @return the mrBalanceDescription
     */
    public String getMrBalanceDescription() {
        return mrBalanceDescription;
    }

    /**
     * @param mrBalanceDescription the mrBalanceDescription to set
     */
    public void setMrBalanceDescription(String mrBalanceDescription) {
        this.mrBalanceDescription = mrBalanceDescription;
    }

    /**
     * @return the type
     */
    public Character getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Character type) {
        this.type = type;
    }

    /**
     * @return the trid
     */
    public String getTrid() {
        return trid;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(String trid) {
        this.trid = trid;
    }

    /**
     * @return the oldBalance
     */
    public Double getOldBalance() {
        return oldBalance;
    }

    /**
     * @param oldBalance the oldBalance to set
     */
    public void setOldBalance(Double oldBalance) {
        this.oldBalance = oldBalance;
    }
}
