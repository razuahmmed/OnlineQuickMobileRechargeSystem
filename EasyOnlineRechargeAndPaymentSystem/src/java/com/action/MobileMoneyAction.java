package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.MobileMoneyDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.MobileMoneyDebit;
import com.persistance.MobileMoneyHistory;
import com.persistance.MobileMoneyTransactionHistory;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MobileMoneyAction extends ActionSupport implements Runnable {

    private String phone;
    private String type;
    private Double amount;
    private String pin;
    private String operator;
    private String mmOperator;
    private Double fee;
    private Double totalAmount;
    private String pinStatus;
    //    
    private Integer mobileMoneyId;
    private Integer mmTransactionHistoryId;
    private String modalOpen;
    private String modalClose;
    private String showMmType;
    //    
    private String messageString;
    private String messageColor;
    private String mess;
    //    
    private String fieldName;
    private String fieldValue;
    //    
    private CommonList commonList = null;
    private Connection connection = null;
    //
    private MobileMoneyDao mobileMoneyDao = null;
    //   
    private List<MobileMoneyHistory> lastMMoneyHInfoList = null;
    private List<MobileMoneyHistory> allMMoneyHInfoList = null;
    private List<MobileMoneyHistory> singleMMoneyHInfoList = null;
    private List<MobileMoneyDebit> currentMmBalInfoList = null;
    private List<MobileMoneyTransactionHistory> mmPReceiveInfoList = null;
    private List<MobileMoneyTransactionHistory> singleMmPaymentReceiveInfoList = null;
    private List<MobileMoneyTransactionHistory> mmPMadeInfoList = null;
    private List<MobileMoneyTransactionHistory> singleMmPaymentMadeInfoList = null;
    private ResellerAction resellerAction = null;
    //
    private static volatile boolean mobileMoneyThreadRunning = false;

    /**
     * *************************************************************************
     *********************** Mobile Money Thread Part
     *
     * @return
     */
    public String startMobileMoneyThread() {

        Thread thread = new Thread(this);

        mobileMoneyThreadRunning = true;

        if (mobileMoneyThreadRunning == true) {

            thread.start();

            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                mobileMoneyThreadRunning = false;
                System.out.println(e);
            }

            messageColor = "success";
            messageString = "Mobile Money Successfully Started";
            return SUCCESS;
        } else {

            messageColor = "danger";
            messageString = "Mobile Money Start Failed";
            return ERROR;
        }
    }

    public String stopMobileMoneyThread() {

        mobileMoneyThreadRunning = false;

        if (mobileMoneyThreadRunning == false) {

            messageColor = "success";
            messageString = "Mobile Money Successfully Stopped";
            return SUCCESS;
        } else {

            messageColor = "danger";
            messageString = "Mobile Money Stop Failed";
            return ERROR;
        }
    }

    @Override
    public void run() {

        while (mobileMoneyThreadRunning == true) {

            if (connection == null) {
                connection = DBConnection.getMySqlConnection();
            }

            if (commonList == null) {
                commonList = new CommonList();
            }

            if (connection != null) {

                List<MobileMoneyHistory> watingInfo = commonList.selectMobileMoneyWatingStatusInfo(connection);

                if (!watingInfo.isEmpty()) {

                    for (MobileMoneyHistory mmh : watingInfo) {

                        System.out.println(mmh.getMobileMoneyId());
                        System.out.println(mmh.getReceiver());
                        System.out.println(mmh.getGivenBalance());
                        System.out.println(mmh.getTrid());

                        try {

//                            String mmStatusQuery = " UPDATE mobile_money_history "
//                                    + " SET active_status =  'Y' "
//                                    + " WHERE mobile_money_id = '" + st + "' ";
//
//                            ps = connection.prepareStatement(mmStatusQuery);
//                            status = ps.executeUpdate();
                        } catch (Exception ex) {
                            System.out.println(ex);
                        }
                    }
                }
            }
            System.out.println("MM_thread_running");
        }
    }

    /**
     * *************************************************************************
     * ********************** MM List Methods
     *
     * @return
     */
    public List<MobileMoneyDebit> currentMMBalance(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCurrentMmBalInfoList(commonList.mmCurBal(connection, uId));
        }

        return currentMmBalInfoList;
    }

    public List<MobileMoneyHistory> lastMMoneyHInfo() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setLastMMoneyHInfoList(commonList.getLastMobileMoneyHistoryInfo(connection, userID));
        }

        return lastMMoneyHInfoList;
    }

    public List<MobileMoneyHistory> allMMoneyHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMMoneyHInfoList(commonList.getAllMobileMoneyHistoryInfo(connection, uId));
        }

        return allMMoneyHInfoList;
    }

    public List<MobileMoneyHistory> getSingleMMoneyHInfo() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMMoneyHInfoList(commonList.getSingleMobileMoneyHistoryInfo(connection, userID, mobileMoneyId));
        }

        return singleMMoneyHInfoList;
    }

    public List<MobileMoneyTransactionHistory> getMMPaymentReceiveHistory(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMmPReceiveInfoList(commonList.getMMPaymentReceiveHistory(connection, uId));
        }

        return mmPReceiveInfoList;
    }

    public List<MobileMoneyTransactionHistory> getSingleMMPReceiveHInfo(String uId, Integer mmthId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMmPaymentReceiveInfoList(commonList.getSingleMMPaymentReceiveHistory(connection, uId, mmthId));
        }

        return singleMmPaymentReceiveInfoList;
    }

    public List<MobileMoneyTransactionHistory> getMMPaymentMadeHistory(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMmPMadeInfoList(commonList.getMMPaymentMadeHistory(connection, uId));
        }

        return mmPMadeInfoList;
    }

    public List<MobileMoneyTransactionHistory> getSingleMMPMadeHInfo(String uId, Integer mmthId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMmPaymentMadeInfoList(commonList.getSingleMMPaymentMadeHistory(connection, uId, mmthId));
        }

        return singleMmPaymentMadeInfoList;
    }

    /**
     * *************************************************************************
     ****************** Action Come from home page
     *
     * @return
     */
    public String showLastMMHInHome() {

        lastMMoneyHInfo();

        return SUCCESS;
    }

    public String mmFromHome() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileMoneyDao == null) {
            mobileMoneyDao = new MobileMoneyDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                int checked = mobileMoneyDao.saveMobileMoney(connection, userID, phone, type, amount, mmOperator, totalAmount, userID);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Tk. " + amount + " has been transferred to mobile number " + phone + ".";
                } else {
                    messageColor = "danger";
                    messageString = "Tk. " + amount + " transfer failed to mobile number " + phone + ".";
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ***************************** Mobile Money Methods
     *
     * @return
     */
    public String showLastMMHistory() {

        lastMMoneyHInfo();

        return SUCCESS;
    }

    public String goMMConfirmPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        phone = phone;
        amount = amount;
        type = type;
        mmOperator = mmOperator;
        operator = operator;

        fee = (amount * 2) / 100;
        totalAmount = amount + fee;

        return SUCCESS;
    }

    public String sendMM() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (mobileMoneyDao == null) {
            mobileMoneyDao = new MobileMoneyDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, pin);

            if (cPin.size() != 0) {

                List<MobileMoneyDebit> mmCurBal = currentMMBalance(userID);

                for (MobileMoneyDebit rechargeDebit : mmCurBal) {

                    if (rechargeDebit.getMmCurrentBalance() > amount) {

                        int checked = mobileMoneyDao.saveMobileMoney(connection, userID, phone, type, amount, mmOperator, totalAmount, userID);

                        if (checked > 0) {
                            showLastMMHistory();
                            mess = "show";
                            messageColor = "success";
                            messageString = "Tk. " + amount + " has been transferred to mobile number " + phone + ".";
                        } else {
                            showLastMMHistory();
                            mess = "show";
                            messageColor = "danger";
                            messageString = "Tk. " + amount + " transfer failed to mobile number " + phone + ".";
                        }
                    } else {
                        showLastMMHistory();
                        mess = "show";
                        messageColor = "danger";
                        messageString = "Oops! There are not enough balance in the mobile money";
                    }
                }
            } else {
                showLastMMHistory();
                mess = "show";
                messageColor = "danger";
                messageString = "Invalid Pin !";
            }
        }

        currentMMBalance(userID);

        return SUCCESS;
    }

    public String viewMMoneyHDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        // type problem
        allMMoneyHInfo(userID);

        getSingleMMoneyHInfo();

        return SUCCESS;
    }

    public String searchMobileMoneyHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMMoneyHInfoList(commonList.searchMobileMoneyHistoryInfo(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String viewMMPReceiveHDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getSingleMMPReceiveHInfo(userID, mmTransactionHistoryId);

        System.out.println(mmTransactionHistoryId);

        return SUCCESS;
    }

    public String searchMMPaymentsReceiveHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMmPReceiveInfoList(commonList.searchMMPaymentReceiveHistory(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String viewMMPMadeHDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getSingleMMPMadeHInfo(userID, mmTransactionHistoryId);

        System.out.println(mmTransactionHistoryId);

        return SUCCESS;
    }

    public String searchMMPaymentsMadeHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMmPMadeInfoList(commonList.searchMMPaymentMadeHistory(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * *********************** MM Page Methods
     *
     * @return
     */
    public String goSendMobileMoneyPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (resellerAction == null) {
            resellerAction = new ResellerAction();
        }

        lastMMoneyHInfo();

        currentMMBalance(userID);

        setPinStatus(resellerAction.checkPinStatus(userID));

        return SUCCESS;
    }

    public String goMobileMoneyHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        allMMoneyHInfo(userID);

        return SUCCESS;
    }

    public String goMMPaymentsReceivedPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMMPaymentReceiveHistory(userID);

        return SUCCESS;
    }

    public String goMMPaymentsMadePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMMPaymentMadeHistory(userID);

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ********************** MM Setter and Getter Methods
     *
     * @return
     */
    /**
     * @return the lastMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getLastMMoneyHInfoList() {
        return lastMMoneyHInfoList;
    }

    /**
     * @param lastMMoneyHInfoList the lastMMoneyHInfoList to set
     */
    public void setLastMMoneyHInfoList(List<MobileMoneyHistory> lastMMoneyHInfoList) {
        this.lastMMoneyHInfoList = lastMMoneyHInfoList;
    }

    /**
     * @return the allMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getAllMMoneyHInfoList() {
        return allMMoneyHInfoList;
    }

    /**
     * @param allMMoneyHInfoList the allMMoneyHInfoList to set
     */
    public void setAllMMoneyHInfoList(List<MobileMoneyHistory> allMMoneyHInfoList) {
        this.allMMoneyHInfoList = allMMoneyHInfoList;
    }

    /**
     * @return the singleMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getSingleMMoneyHInfoList() {
        return singleMMoneyHInfoList;
    }

    /**
     * @param singleMMoneyHInfoList the singleMMoneyHInfoList to set
     */
    public void setSingleMMoneyHInfoList(List<MobileMoneyHistory> singleMMoneyHInfoList) {
        this.singleMMoneyHInfoList = singleMMoneyHInfoList;
    }

    /**
     * @return the mobileMoneyId
     */
    public Integer getMobileMoneyId() {
        return mobileMoneyId;
    }

    /**
     * @param mobileMoneyId the mobileMoneyId to set
     */
    public void setMobileMoneyId(Integer mobileMoneyId) {
        this.mobileMoneyId = mobileMoneyId;
    }

    /**
     * @return the modalOpen
     */
    public String getModalOpen() {
        return modalOpen;
    }

    /**
     * @param modalOpen the modalOpen to set
     */
    public void setModalOpen(String modalOpen) {
        this.modalOpen = modalOpen;
    }

    /**
     * @return the modalClose
     */
    public String getModalClose() {
        return modalClose;
    }

    /**
     * @param modalClose the modalClose to set
     */
    public void setModalClose(String modalClose) {
        this.modalClose = modalClose;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin the pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the mmOperator
     */
    public String getMmOperator() {
        return mmOperator;
    }

    /**
     * @param mmOperator the mmOperator to set
     */
    public void setMmOperator(String mmOperator) {
        this.mmOperator = mmOperator;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the mess
     */
    public String getMess() {
        return mess;
    }

    /**
     * @param mess the mess to set
     */
    public void setMess(String mess) {
        this.mess = mess;
    }

    /**
     * @return the fee
     */
    public Double getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(Double fee) {
        this.fee = fee;
    }

    /**
     * @return the totalAmount
     */
    public Double getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the showMmType
     */
    public String getShowMmType() {
        return showMmType;
    }

    /**
     * @param showMmType the showMmType to set
     */
    public void setShowMmType(String showMmType) {
        this.showMmType = showMmType;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the currentMmBalInfoList
     */
    public List<MobileMoneyDebit> getCurrentMmBalInfoList() {
        return currentMmBalInfoList;
    }

    /**
     * @param currentMmBalInfoList the currentMmBalInfoList to set
     */
    public void setCurrentMmBalInfoList(List<MobileMoneyDebit> currentMmBalInfoList) {
        this.currentMmBalInfoList = currentMmBalInfoList;
    }

    /**
     * @return the mmPReceiveInfoList
     */
    public List<MobileMoneyTransactionHistory> getMmPReceiveInfoList() {
        return mmPReceiveInfoList;
    }

    /**
     * @param mmPReceiveInfoList the mmPReceiveInfoList to set
     */
    public void setMmPReceiveInfoList(List<MobileMoneyTransactionHistory> mmPReceiveInfoList) {
        this.mmPReceiveInfoList = mmPReceiveInfoList;
    }

    /**
     * @return the mmTransactionHistoryId
     */
    public Integer getMmTransactionHistoryId() {
        return mmTransactionHistoryId;
    }

    /**
     * @param mmTransactionHistoryId the mmTransactionHistoryId to set
     */
    public void setMmTransactionHistoryId(Integer mmTransactionHistoryId) {
        this.mmTransactionHistoryId = mmTransactionHistoryId;
    }

    /**
     * @return the singleMmPaymentReceiveInfoList
     */
    public List<MobileMoneyTransactionHistory> getSingleMmPaymentReceiveInfoList() {
        return singleMmPaymentReceiveInfoList;
    }

    /**
     * @param singleMmPaymentReceiveInfoList the singleMmPaymentReceiveInfoList
     * to set
     */
    public void setSingleMmPaymentReceiveInfoList(List<MobileMoneyTransactionHistory> singleMmPaymentReceiveInfoList) {
        this.singleMmPaymentReceiveInfoList = singleMmPaymentReceiveInfoList;
    }

    /**
     * @return the mmPMadeInfoList
     */
    public List<MobileMoneyTransactionHistory> getMmPMadeInfoList() {
        return mmPMadeInfoList;
    }

    /**
     * @param mmPMadeInfoList the mmPMadeInfoList to set
     */
    public void setMmPMadeInfoList(List<MobileMoneyTransactionHistory> mmPMadeInfoList) {
        this.mmPMadeInfoList = mmPMadeInfoList;
    }

    /**
     * @return the singleMmPaymentMadeInfoList
     */
    public List<MobileMoneyTransactionHistory> getSingleMmPaymentMadeInfoList() {
        return singleMmPaymentMadeInfoList;
    }

    /**
     * @param singleMmPaymentMadeInfoList the singleMmPaymentMadeInfoList to set
     */
    public void setSingleMmPaymentMadeInfoList(List<MobileMoneyTransactionHistory> singleMmPaymentMadeInfoList) {
        this.singleMmPaymentMadeInfoList = singleMmPaymentMadeInfoList;
    }

    public List<String> getMobileMoneyWatingStatus() {

        List<String> watingIDList = new ArrayList<String>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmStatusQuery = " SELECT mobile_money_id FROM mobile_money_history WHERE active_status = 'W' ";

                ps = connection.prepareStatement(mmStatusQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    String id = rs.getString("mobile_money_id");
                    watingIDList.add(id);
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return watingIDList;
    }

    /**
     * @return the pinStatus
     */
    public String getPinStatus() {
        return pinStatus;
    }

    /**
     * @param pinStatus the pinStatus to set
     */
    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }
}
