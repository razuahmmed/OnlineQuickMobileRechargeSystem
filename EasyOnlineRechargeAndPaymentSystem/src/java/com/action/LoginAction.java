package com.action;

import static com.common.ActionResult.DBCONNECTIONFAILD;
import static com.common.ActionResult.RESELLERLOGINSUCCESS;
import com.common.CommonList;
import com.common.DBConnection;
import com.model.LoginDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.MobileMoneyDebit;
import com.persistance.MobileMoneyHistory;
import com.persistance.MobileRechargeDebit;
import com.persistance.MobileRechargeHistory;
import com.persistance.UserInfo;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class LoginAction extends ActionSupport {

    private String userId;
    private String userPassword;
    private String userName;
    private String email;
    //    
    private String messageString;
    private String loginMessage;
    private String messageColor;
    // 
    private CommonList commonList = null;
    private Connection connection = null;
    //    
    private List<MobileRechargeHistory> lastMRechargeHInfoList = null;
    private List<MobileMoneyHistory> lastMMoneyHInfoList = null;
    //
    private MobileRechargeAction mobileRechargeActionInfo = null;
    private MobileMoneyAction mobileMoneyActionInfo = null;
    //
    private LoginDao loginDao = null;
    //

    public String login() {

        Map session = ActionContext.getContext().getSession();

        if (userId.isEmpty() || userPassword.isEmpty()) {
            messageColor = "danger";
            messageString = "Enter any User ID and password.";
            return INPUT;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (loginDao == null) {
            loginDao = new LoginDao();
        }

        if (mobileRechargeActionInfo == null) {
            mobileRechargeActionInfo = new MobileRechargeAction();
        }

        if (mobileMoneyActionInfo == null) {
            mobileMoneyActionInfo = new MobileMoneyAction();
        }

        if (connection != null) {

            List<UserInfo> suspendUserList = commonList.suspendTest(connection, userId, userPassword);

            if ((!suspendUserList.isEmpty()) && (suspendUserList.size() > 0)) {
                messageColor = "danger";
                messageString = "You have suspended  from your activity please contact your top reseller";
                return ERROR;
            } else {

                List<UserInfo> userList = commonList.loginInfo(connection, userId, userPassword);

                if ((!userList.isEmpty()) && (userList.size() > 0)) {

                    for (UserInfo user : userList) {

                        if ((user.getUserId().trim().equals(userId)) && (user.getUserPassword().trim().equals(userPassword))) {

                            session.put("userId", user.getUserId());
                            session.put("userPassword", user.getUserPassword());
                            session.put("userName", user.getUserName());
                            session.put("groupId", user.getUserGroupInfo().getGroupId());
                            session.put("groupName", user.getUserGroupInfo().getGroupName());

                            List<UserLevelMenu> menus = commonList.getManus(connection, (String) session.get("userId"), (Integer) session.get("groupId"));

                            for (UserLevelMenu menu : menus) {
                                session.put(menu.getMenuId(), menu.getLevelMenuStatus());
                                session.put(("MR0" + menu.getMenuId()), menu.getUserInfo().getServiceMRecharge());
                                session.put(("MM0" + menu.getMenuId()), menu.getUserInfo().getServiceMMoney());
                            }

                            List<MobileRechargeDebit> mrCBalance = commonList.mrCurBal(connection, (String) session.get("userId"));
                            List<MobileMoneyDebit> mmCBalance = commonList.mmCurBal(connection, (String) session.get("userId"));
                            session.put("mrBalance", mrCBalance.get(0).getMrCurrentBalance());
                            session.put("mmBalance", mmCBalance.get(0).getMmCurrentBalance());

                            loginDao.updateLoginStatus(connection, userId);

                            setLastMRechargeHInfoList(mobileRechargeActionInfo.lastMRechargeHInfo());
                            setLastMMoneyHInfoList(mobileMoneyActionInfo.lastMMoneyHInfo());
                        } else {
                            messageColor = "danger";
                            messageString = "Invalid User ID or Password.";
                            return ERROR;
                        }
                    }
                } else {
                    messageColor = "danger";
                    messageString = "Invalid User ID or Password.";
                    return ERROR;
                }
            }
        } else {
            messageColor = "danger";
            messageString = "DB Connection not found !";
            return DBCONNECTIONFAILD;
        }

        loginMessage = "1";
        return RESELLERLOGINSUCCESS;
    }

    public String forgetPassword() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        String emailExp = "/^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$/";

        if (email.isEmpty() || email == null) {
            messageColor = "danger";
            messageString = "Enter your email.";
            return INPUT;
        }

        if (!email.equals(emailExp)) {
            messageColor = "danger";
            messageString = "Please enter a valid email address.";
            return ERROR;
        }

        loginMessage = "1";

        return SUCCESS;
    }

    public String logout() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (loginDao == null) {
            loginDao = new LoginDao();
        }
        if (connection != null) {
            loginDao.updateLogoutStatus(connection, userID);
        }

        session.remove("userId");
        session.remove("userPassword");
        session.remove("userName");
        session.remove("groupId");
        session.remove("groupName");
        session.remove("MR0");
        session.remove("MM0");
        session.remove("mrBalance");
        session.remove("mmBalance");
        session.clear();

        return SUCCESS;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param aUserId the userId to set
     */
    public void setUserId(String aUserId) {
        userId = aUserId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the lastMRechargeHInfoList
     */
    public List<MobileRechargeHistory> getLastMRechargeHInfoList() {
        return lastMRechargeHInfoList;
    }

    /**
     * @param lastMRechargeHInfoList the lastMRechargeHInfoList to set
     */
    public void setLastMRechargeHInfoList(List<MobileRechargeHistory> lastMRechargeHInfoList) {
        this.lastMRechargeHInfoList = lastMRechargeHInfoList;
    }

    /**
     * @return the lastMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getLastMMoneyHInfoList() {
        return lastMMoneyHInfoList;
    }

    /**
     * @param lastMMoneyHInfoList the lastMMoneyHInfoList to set
     */
    public void setLastMMoneyHInfoList(List<MobileMoneyHistory> lastMMoneyHInfoList) {
        this.lastMMoneyHInfoList = lastMMoneyHInfoList;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the loginMessage
     */
    public String getLoginMessage() {
        return loginMessage;
    }

    /**
     * @param loginMessage the loginMessage to set
     */
    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }
}
