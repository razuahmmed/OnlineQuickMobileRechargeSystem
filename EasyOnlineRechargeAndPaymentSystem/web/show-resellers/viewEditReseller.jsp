<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/profile.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function updateReseller(resellerId) {

                var userGroupId = $("#userLevelList").val();

                var mobileRecharge = document.getElementById("mRecharge");

                var mobileMoney = document.getElementById("mMoney");

                var fullName = document.getElementById("fullName").value;

                var email = document.getElementById("email").value;

                var phone = document.getElementById("phone").value;

                var manageRateId = $("#rateList").val();

                var otpStatus = document.getElementById("otpY");

                var pinEnabledStatus = document.getElementById('pinEnabledY');

                var loadPin = document.getElementById("loadPin").value;

                var apiStatus = document.getElementById("apiY");

                var sendEmailStatus = document.getElementById("sendEmailY");

                var pinConfirmStatus = '';

                var currentUPin = document.getElementById("upin").value;
                //
                if (!mobileRecharge.checked) {
                    mobileRecharge = 'N';
                    flag = true;
                } else {
                    mobileRecharge = document.getElementById("mRecharge").value;
                    flag = true;
                }
                //
                if (!mobileMoney.checked) {
                    mobileMoney = 'N';
                    flag = true;
                } else {
                    mobileMoney = document.getElementById("mMoney").value;
                    flag = true;
                }
                //
                if ((!notEmpty(fullName, "Full Name can't be empty !")) & (!forFullName(fullName, "Invalid fullName Format !")) & (!lengthRestriction(fullName, 3, 20, "your fullName"))) {
                    return false;
                }
                //
                if (email == "") {
                    flag = true;
                } else if (!emailValidator(email, "Invalid Email Format !")) {
                    return false;
                }
                //
                if (phone == "") {
                    flag = true;
                } else if ((!forPhone(phone, "Please use a valid Phone Number !")) & (!lengthRestriction(phone, 11, 11, "your phone"))) {
                    return false;
                }
                //
                if (otpStatus.checked) {
                    otpStatus = document.getElementById("otpY").value;
                    flag = true;
                } else {
                    otpStatus = document.getElementById("otpN").value;
                    flag = true;
                }
                //
                if (pinEnabledStatus.checked) {
                    if ((!notEmpty(loadPin, "Pin can't be empty !")) & (!isAlphanumeric(loadPin, "Invalid Pin Format !")) & (!lengthRestriction(loadPin, 6, 10, "your pin"))) {
                        return false;
                    }
                    pinConfirmStatus = loadPin;
                    pinEnabledStatus = document.getElementById('pinEnabledY').value;
                } else {
                    pinConfirmStatus = '';
                    pinEnabledStatus = document.getElementById('pinEnabledN').value;
                    flag = true;
                }
                //
                if (apiStatus.checked) {
                    apiStatus = document.getElementById("apiY").value;
                    flag = true;
                } else {
                    apiStatus = document.getElementById("apiN").value;
                    flag = true;
                }
                //
                if (sendEmailStatus.checked) {
                    sendEmailStatus = document.getElementById("sendEmailY").value;
                    flag = true;
                } else {
                    sendEmailStatus = document.getElementById("sendEmailN").value;
                    flag = true;
                }
                //
                var message = confirm("Do you want to Update Reseller ?");

                if (message == true) {

                    var flag = true;

                    if (flag) {

                        var dataString = 'userLevelList=' + userGroupId;
                        dataString += '&mobileRecharge=' + mobileRecharge;
                        dataString += '&mobileMoney=' + mobileMoney;
                        dataString += '&resellerId=' + resellerId;
                        dataString += '&fullName=' + fullName;
                        dataString += '&email=' + email;
                        dataString += '&phone=' + phone;
                        dataString += '&rateList=' + manageRateId;
                        dataString += '&otpStatus=' + otpStatus;
                        dataString += '&pinEnabledStatus=' + pinEnabledStatus;
                        dataString += '&apiStatus=' + apiStatus;
                        dataString += '&sendEmailStatus=' + sendEmailStatus;
                        dataString += '&pinConfirmStatus=' + pinConfirmStatus;
                        dataString += '&currentUPin=' + currentUPin;
                        //
                        document.getElementById('createLoadingImage').src = 'images/loading.gif';
                        //
                        $.ajax({
                            type: "POST",
                            url: 'UpdateReseller',
                            data: dataString,
                            success: function (data) {
                                document.getElementById('createLoadingImage').src = '';
                                $("#message").html(data);
                            }
                        });
                    }
                }
            }
            //
            function buttonUpdate(resellerId) {

                var dataString = 'resellerId=' + resellerId;

                $.ajax({
                    type: "POST",
                    url: 'ChangeButton',
                    data: dataString,
                    success: function (data) {
                        $("#buttonSpan").html(data);
                    }
                });
            }
            //            
            function statusUpdate(resellerId) {

                var dataString = 'resellerId=' + resellerId;

                $.ajax({
                    type: "POST",
                    url: 'StatusUpdate',
                    data: dataString,
                    success: function (data) {
                        $("#statusTD").html(data);
                    }
                });
            }
            //            
            function suspendReseller(resellerId) {

                var warning = confirm("Are you sure suspend this reseller ?");

                if (warning == true) {

                    var dataString = 'resellerId=' + resellerId;
                    //
                    document.getElementById('deleteLoadingImage').src = 'images/loading.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'ResellerSuspend',
                        data: dataString,
                        success: function (data) {
                            document.getElementById('deleteLoadingImage').src = '';
                            $("#message").html(data);
                            buttonUpdate(resellerId);
                            statusUpdate(resellerId);
                        }
                    });
                }
            }
            //            
            function unSuspendReseller(resellerId) {

                var warning = confirm("Are you sure activated this reseller ?");

                if (warning == true) {

                    var dataString = 'resellerId=' + resellerId;
                    //
                    document.getElementById('deleteLoadingImage').src = 'images/loading.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'ResellerUnSuspend',
                        data: dataString,
                        success: function (data) {
                            document.getElementById('deleteLoadingImage').src = '';
                            $("#message").html(data);
                            buttonUpdate(resellerId);
                            statusUpdate(resellerId);
                        }
                    });
                }
            }
            //
            function deleteReseller(resellerId) {

                var warning = confirm("You are deleting Reseller - " + resellerId + " This Can't be Undone, It will also delete sub user under this account");

                if (warning == true) {

                    var dataString = 'resellerId=' + resellerId;
                    //
                    document.getElementById('deleteLoadingImage').src = 'images/loading.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'DeleteReseller',
                        data: dataString,
                        success: function (data) {
                            document.getElementById('deleteLoadingImage').src = '';
                            $("#message").html(data);
                        }
                    });
                }
            }
            // 
            function forFullName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_. ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //            
            function isAlphanumeric(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function emailValidator(id, helperMsg) {

                var emailExp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (id.match(emailExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
            //
            function forPhone(id, helperMsg) {
                var numericExpression = /^[0-9]+$/;
                if (id.match(numericExpression)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                View User  
                                <s:if test="singleResallerInfoList !=null">
                                    <s:iterator value="singleResallerInfoList">
                                        <small><s:property value="userName"/></small>
                                    </s:iterator>
                                </s:if>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div id="mesHide<s:property value="messageColor"/>" class="alert alert-success fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-times"></i>
                        <s:property value="messageString"/>
                    </div>

                    <div class="row profile">
                        <div class="col-xs-12">
                            <!--BEGIN TABS-->
                            <div class="tabbable tabbable-custom tabbable-full-width">
                                <ul class="nav nav-tabs">
                                    <li class="<s:property value="messageViewResellerButtonClicked"/>">
                                        <a href="#tab_1_1" data-toggle="tab">
                                            Overview
                                        </a>
                                    </li>
                                    <li class="<s:property value="messageEditResellerButtonClicked"/>">
                                        <a href="#tab_1_3" data-toggle="tab">
                                            Edit&nbsp;Account
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane <s:property value="messageViewResellerButtonClicked"/>" id="tab_1_1">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-8 profile-info">
                                                        <s:if test="singleResallerInfoList !=null">
                                                            <s:if test="singleResallerInfoList.size() !=0">
                                                                <s:iterator value="singleResallerInfoList">
                                                                    <h1><s:property value="userName"/></h1>
                                                                    <div class="clearfix">
                                                                        <h4 class="block">Actions</h4>
                                                                        <% if ((PM03CM01 && PERMISSION_MR)) {%>
                                                                        <a id="<s:property value="fundButton"/>" href="Payments?resellerId=<s:property value="userId"/>" class="btn btn blue">Add&nbsp;Fund</a>
                                                                        <% }%>
                                                                        <% if (!(PM03CM01 && PERMISSION_MR) && (PM04CM01 && PERMISSION_MM)) {%>
                                                                        <a id="<s:property value="fundButton"/>" href="Payments?resellerId=<s:property value="userId"/>&loadType=<s:property value="serviceMMoney"/>" class="btn btn blue">Add&nbsp;Fund</a>
                                                                        <% }%>
                                                                        <span id="<s:property value="suspendButton"/>">
                                                                            <s:if test="suspendActivity=='Y'">
                                                                                <a href="javascript:void(0);" onclick="suspendReseller('<s:property value="userId"/>');" class="btn btn yellow">Suspend</a>
                                                                            </s:if>
                                                                            <s:else>
                                                                                <a href="javascript:void(0);" onclick="unSuspendReseller('<s:property value="userId"/>');" class="btn btn green">Unsuspend</a>
                                                                            </s:else>
                                                                        </span>
                                                                        <a id="<s:property value="deleteButton"/>" href="javascript:void(0);" onclick="deleteReseller('<s:property value="userId"/>');" class="btn btn red">Delete</a>
                                                                        <img id="deleteLoadingImage" src="" alt="" />
                                                                    </div>
                                                                    <br>
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="width: 30%">User ID:</th>
                                                                                <th><s:property value="userId"/></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Full Name:</td>
                                                                                <td><s:property value="userName"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>User Type:</td>
                                                                                <td><s:property value="userGroupInfo.groupName"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>User Email:</td>
                                                                                <td>
                                                                                    <s:if test="emailAddress !=null">
                                                                                        <s:property value="emailAddress"/>
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        No email address
                                                                                    </s:else>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Phone Number:</td>
                                                                                <td>
                                                                                    <s:if test="contactNumber1 !=null">
                                                                                        <s:property value="contactNumber1"/>
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        No phone number
                                                                                    </s:else>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Parent:</td>
                                                                                <td><s:property value="parentId"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Sub Admin</td>
                                                                                <td>  </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Reseller 4</td>
                                                                                <td>   </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Reseller 3</td>
                                                                                <td>  </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Reseller 2</td>
                                                                                <td>    </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Status</td>
                                                                                <td id="statusTD">
                                                                                    <s:if test="suspendActivity=='Y'">Active</s:if>
                                                                                    <s:else>
                                                                                        <b style="color: white;color: red;">Inactive</b>
                                                                                    </s:else>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Creation Date</td>
                                                                                <td><s:property value="insertDate"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Last Login</td>
                                                                                <td><s:property value="lastLogin"/></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Mobile Recharge Balance</td>
                                                                                <td>
                                                                                    <% if (PM03CM01 && PERMISSION_MR) {%>
                                                                                    <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                                                                                    <% }%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Mobile Money Balance</td>
                                                                                <td>
                                                                                    <% if (PM04CM01 && PERMISSION_MM) {%>
                                                                                    <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                                                    <% }%>
                                                                                </td>
                                                                            </tr>
<!--                                                                            <tr>
                                                                                <td>Int Balance</td>
                                                                                <td>0.0</td>
                                                                            </tr>-->
                                                                            <tr>
                                                                                <td>Rate&nbsp;Plan</td>
                                                                                <td>
                                                                                    <s:property value="manageRateInfo.rateName"/>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!--end col-xs-8-->

                                                                <div class="col-xs-4">
                                                                    <div class="portlet sale-summary">

                                                                        <div class="portlet-title">
                                                                            <div class="caption">
                                                                                Summary
                                                                            </div>
                                                                        </div>

                                                                        <div class="portlet-body">
                                                                            <ul class="list-unstyled">

                                                                                <% if (PM03CM01 && PERMISSION_MR) {%>
                                                                                <li>
                                                                                    <span class="sale-info">MOBILE RECHARGE BALANCE</span>
                                                                                    <span class="sale-num"><s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/></span>
                                                                                </li>
<!--                                                                                <li>
                                                                                    <span class="sale-info">
                                                                                        TOTAL REQUEST MADE
                                                                                    </span>
                                                                                    <span class="sale-num">							0 </span>
                                                                                </li>
                                                                                <li>
                                                                                    <span class="sale-info">
                                                                                        TOTAL AMOUNT
                                                                                    </span>
                                                                                    <span class="sale-num">							Taka  </span>
                                                                                </li>-->
                                                                                <% }%>

                                                                                <% if (PM04CM01 && PERMISSION_MM) {%>
                                                                                <li>
                                                                                    <span class="sale-info">MOBILE MONEY BALANCE</span>
                                                                                    <span class="sale-num"><s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/></span>
                                                                                </li>
                                                                                <% }%>

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                    <s:if test="singleResallerInfoList.size() ==0">
                                                        No history found
                                                    </s:if>
                                                    <!--end col-xs-4-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                        </div>
                                    </div>

                                    <!--tab_1_2-->
                                    <div class="tab-pane <s:property value="messageEditResellerButtonClicked"/>" id="tab_1_3">
                                        <div class="row profile-account" id="pwd-container">
                                            <div class="col-xs-12">
                                                <div class="tab-content">

                                                    <div id="tab_1-1" class="tab-pane active">

                                                        <s:if test="singleResallerInfoList !=null">
                                                            <s:if test="singleResallerInfoList.size() !=0">
                                                                <s:iterator value="singleResallerInfoList">
                                                                    <form action="#" method="post" class="form-horizontal">
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Type</label>
                                                                                <div class="col-xs-4">

                                                                                    <p class="form-control-static">
                                                                                        <s:property value="userGroupInfo.groupName"/>
                                                                                    </p>

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Services</label>
                                                                                <div class="col-xs-7">
                                                                                    <div class="checkbox-list">

                                                                                        <% if (PM03CM01 && PERMISSION_MR) {%>
                                                                                        <s:if test='serviceMRecharge=="MR"'>
                                                                                            <label>
                                                                                                <input type="checkbox" id="mRecharge" name="mRecharge" value="MR" checked>
                                                                                                Mobile&nbsp;Recharge
                                                                                            </label>
                                                                                        </s:if>
                                                                                        <s:else>
                                                                                            <label>
                                                                                                <input type="checkbox" id="mRecharge" name="mRecharge" value="MR">
                                                                                                Mobile&nbsp;Recharge
                                                                                            </label>
                                                                                        </s:else>
                                                                                        <% }%>

                                                                                        <% if (PM04CM01 && PERMISSION_MM) {%>
                                                                                        <s:if test='serviceMMoney=="MM"'>
                                                                                            <label>
                                                                                                <input type="checkbox" id="mMoney" name="mMoney" value="MM" checked>
                                                                                                Mobile&nbsp;Money
                                                                                            </label>
                                                                                        </s:if>
                                                                                        <s:else>
                                                                                            <label>
                                                                                                <input type="checkbox" id="mMoney" name="mMoney" value="MM">
                                                                                                Mobile&nbsp;Money
                                                                                            </label>
                                                                                        </s:else>
                                                                                        <% }%>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">User ID*</label>
                                                                                <div class="col-xs-4">

                                                                                    <input type="text" id="userId" name="userId" value="<s:property value="userId"/>" disabled class="form-control">

                                                                                    <span class="help-block">Enter username 5 to 12 character </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">User Fullname*</label>
                                                                                <div class="col-xs-4">

                                                                                    <input type="text" id="fullName" name="fullName" value="<s:property value="userName"/>" class="form-control">

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Email Address</label>
                                                                                <div class="col-xs-4">
                                                                                    <s:if test="emailAddress !=null">
                                                                                        <input type="text" id="email" name="email" value="<s:property value="emailAddress"/>" class="form-control">
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        <input type="text" id="email" name="email" value="" class="form-control">
                                                                                    </s:else>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Phone Number</label>
                                                                                <div class="col-xs-4">
                                                                                    <s:if test="contactNumber1 !=null">
                                                                                        <input type="text" id="phone" name="phone" value="<s:property value="contactNumber1"/>" class="form-control">
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        <input type="text" id="phone" name="phone" value="" class="form-control">
                                                                                    </s:else>
                                                                                    <span class="help-block">Enter phone number with country code </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class=" col-md-3 control-label">Rate&nbsp;Plan</label>
                                                                                <div class=" col-md-4">
                                                                                    <select id="rateList" name="rateList" class="form-control">
                                                                                        <s:if test="allManageRateInfoList !=null">
                                                                                            <s:if test="allManageRateInfoList.size() !=0">
                                                                                                <s:iterator value="allManageRateInfoList">
                                                                                                    <option <s:if test="manageRateId==manageRateInfo.manageRateId">selected</s:if> value="<s:property value="manageRateId"/>"><s:property value="rateName"/></option>
                                                                                                </s:iterator>
                                                                                            </s:if>
                                                                                        </s:if>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Enable OTP ?</label>
                                                                                <div class="col-xs-4">
                                                                                    <div class="radio-list">

                                                                                        <s:if test="otpStatus=='Y'">
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="otpY" name="otp" value="Y" checked>&nbsp;Yes
                                                                                            </label>

                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="otpN" name="otp" value="N">&nbsp;No
                                                                                            </label>
                                                                                        </s:if>
                                                                                        <s:else>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="otpY" name="otp" value="Y">&nbsp;Yes
                                                                                            </label>

                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="otpN" name="otp" value="N" checked>&nbsp;No
                                                                                            </label>
                                                                                        </s:else>

                                                                                    </div>
                                                                                    <span class="help-block"> Enable OTP for better security, You must use a valid email address for OTP </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Enable PIN ?</label>
                                                                                <div class="col-xs-4">
                                                                                    <div class="radio-list">
                                                                                        <s:if test='enablePinStatus=="N"'>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="pinEnabledY" name="pinEnabled" value="Y">&nbsp;Yes
                                                                                            </label>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="pinEnabledN" name="pinEnabled" value="N" checked>&nbsp;No
                                                                                            </label>
                                                                                        </s:if>
                                                                                        <s:else>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="pinEnabledY" name="pinEnabled" value="Y" checked>&nbsp;Yes
                                                                                            </label>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="pinEnabledN" name="pinEnabled" value="N">&nbsp;No
                                                                                            </label>
                                                                                            <input type="hidden" id="ipin" value="Y">
                                                                                        </s:else>
                                                                                    </div>
                                                                                    <span class="help-block"> You can enable transactional PIN. If enabled PIN will be required for transactions </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group" id="pin">
                                                                                <label class="col-xs-3 control-label">PIN</label>
                                                                                <div class="col-xs-4">
                                                                                    <s:if test='enablePinStatus=="N"'>
                                                                                        <input type="password" id="loadPin"  name="loadPin" value="" autocomplete="off" class="form-control">
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        <input type="password" id="loadPin"  name="loadPin" value="" autocomplete="off" class="form-control">
                                                                                    </s:else>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-3 control-label">Enable API Access ?</label>
                                                                                <div class="col-xs-4">
                                                                                    <div class="radio-list">

                                                                                        <s:if test="apiAccessStatus=='Y'">
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="apiY" name="api" value="Y" checked>&nbsp;Yes
                                                                                            </label>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="apiN" name="api" value="N">&nbsp;No
                                                                                            </label>
                                                                                        </s:if>
                                                                                        <s:else>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="apiY" name="api" value="Y">&nbsp;Yes
                                                                                            </label>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="apiN" name="api" value="N" checked>&nbsp;No
                                                                                            </label>
                                                                                        </s:else>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group last">
                                                                                <label class="col-xs-3 control-label">Send Email Confirmation ?</label>
                                                                                <div class="col-xs-4">
                                                                                    <div class="radio-list">

                                                                                        <s:if test="emailConfirmationStatus=='Y'">
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="sendEmailY" name="sendEmail" value="Y" checked>&nbsp;Yes
                                                                                            </label>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="sendEmailN" name="sendEmail" value="N"> &nbsp;No
                                                                                            </label>
                                                                                        </s:if>
                                                                                        <s:else>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="sendEmailY" name="sendEmail" value="Y">&nbsp;Yes
                                                                                            </label>
                                                                                            <label class="radio-inline">
                                                                                                <input type="radio" id="sendEmailN" name="sendEmail" value="N" checked> &nbsp;No
                                                                                            </label>
                                                                                        </s:else>

                                                                                    </div>
                                                                                    <span class="help-block"> Send an Email with Login Details for this users </span>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                            <div class="form-group last">
                                                                                <label for="upin" class="col-xs-3 control-label">Your PIN to Confirm: </label>
                                                                                <div class="col-xs-4">

                                                                                    <input type="password" id="upin" name="upin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr>
                                                                        <div class="form-actions fluid">
                                                                            <div class="col-xs-offset-3 col-xs-9">

                                                                                <s:if test="pinStatus !=null">
                                                                                    <s:if test='pinStatus=="N"'>
                                                                                        <button type="button" onclick="updateReseller('<s:property value="userId"/>');" disabled class="btn blue">
                                                                                            <i class="icon-check"></i>
                                                                                            Save&nbsp;Changes
                                                                                        </button>
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        <button type="button" onclick="updateReseller('<s:property value="userId"/>');" class="btn blue">
                                                                                            <i class="icon-check"></i>
                                                                                            Save&nbsp;Changes
                                                                                        </button>
                                                                                    </s:else>
                                                                                </s:if>
                                                                                <button type="reset" class="btn default">Cancel</button>
                                                                                <img id="createLoadingImage" src="" alt="" />
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end col-xs-9-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script type="text/javascript">

                                                                                            jQuery(document).ready(function () {
                                                                                                $("#mesHide").hide();
                                                                                                $("#delHide").hide();
                                                                                                $("#fundHide").hide();
                                                                                                $("#buttonSpanHide").hide();

                                                                                                // initiate layout and plugins         Metronic.init(); // init metronic core components
                                                                                                Layout.init(); // init current layout
                                                                                                QuickSidebar.init() // init quick sidebar
                                                                                                //        UIIdleTimeout.init();

                                                                                                var i_pin = $("#ipin").val();
                                                                                                if (i_pin == "Y") {
                                                                                                    $("#pin").show();
                                                                                                }
                                                                                                else {
                                                                                                    $("#pin").hide();
                                                                                                }
                                                                                                $("input[name$='pinEnabled']").click(function () {
                                                                                                    var pin_enabled = $(this).val();
                                                                                                    if (pin_enabled == "Y") {
                                                                                                        $("#pin").show();
                                                                                                    }
                                                                                                    else {
                                                                                                        $("#pin").hide();
                                                                                                    }
                                                                                                });

                                                                                                $("#mesShow").show();
                                                                                                $("#del").show();
                                                                                                $("#fund").show();
                                                                                                $("#buttonSpan").show();

                                                                                            });

        </script>

    </body>
</html>