<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%
    String userName = (request.getSession().getAttribute("userName") != null) ? request.getSession().getAttribute("userName").toString() : "";

    String userID = (request.getSession().getAttribute("userId") != null) ? request.getSession().getAttribute("userId").toString() : "";

    int groupId = Integer.parseInt((request.getSession().getAttribute("groupId") != null) ? request.getSession().getAttribute("groupId").toString() : "");

    String mrBalance = (request.getSession().getAttribute("mrBalance") != null) ? request.getSession().getAttribute("mrBalance").toString() : "";

    String mmBalance = (request.getSession().getAttribute("mmBalance") != null) ? request.getSession().getAttribute("mmBalance").toString() : "";

    boolean PM10CMBALREQ = (request.getSession().getAttribute("61") != null && request.getSession().getAttribute("61").toString().equalsIgnoreCase("T")) ? true : false;
%>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">

        <!-- LOGO -->
        <div class="page-logo">
            <a class="navbar-brand" href="Home">
                <i class="icon-paper-plane"></i>
                Easy Online Recharge and Payment System
            </a>
            <div class="menu-toggler sidebar-toggler hide"></div>
        </div>

        <!-- BEGIN MENU TOGGLE -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
        <!-- END MENU TOGGLE -->

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

                <!-- BEGIN USER LOGIN DROP DOWN -->
                <li class="dropdown dropdown-user">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username">
                            Welcome&nbsp;<% out.println(userName);%>&nbsp;!&nbsp;Your&nbsp;Balance&nbsp;(&nbsp;MR&nbsp;Tk.&nbsp;<% out.println(mrBalance);%>&boxv;&nbsp;MM&nbsp;Tk.&nbsp;<% out.println(mmBalance);%>).
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="MyProfile">
                                <i class="icon-user"></i>
                                My Profile
                            </a>
                        </li>

                        <%  if (PM10CMBALREQ) {%>
                        <li>
                            <a href="BalanceRequestHistory">
                                <i class="icon-wallet"></i>
                                Bal Request Receive
                            </a>
                        </li>
                        <% }%>

                        <li>
                            <a href="EditProfile">
                                <i class="icon-note"></i>
                                Edit Profile
                            </a>
                        </li>
                        <li>
                            <a href="ChangePassword">
                                <i class="icon-key"></i>
                                Change Password
                            </a>
                        </li>
                        <!--                        <li>
                                                    <a href="ApiInfo">
                                                        <i class="icon-notebook"></i>
                                                        API Info
                                                    </a>
                                                </li>-->
                        <!--                        <li>
                                                    <a href="ContactSupport">
                                                        <i class="icon-call-out"></i>
                                                        Contact Support
                                                    </a>
                                                </li>-->

                        <li class="divider"></li>

                        <li>
                            <a href="Logout">
                                <i class="icon-logout"></i>
                                Log Out
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROP DOWN -->
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
