<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Search <small>search by phone number</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row search-form-default">
                        <div class="col-xs-12">
                            <form class="form-inline" method="post" action="#">
                                <div class="input-group">
                                    <div class="input-cont">
                                        <input type="text" id="phone" name="phone" placeholder="Number..." maxlength="11" class="form-control"/>
                                    </div>
                                    <span class="input-group-btn">
                                        <button type="button" id="btnSearch" class="btn green">
                                            Search
                                            <i class="m-icon-swapright m-icon-white"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>

                    <img id="createLoadingImage" src="" alt="" />

                    <div id="showSearchNumberDiv">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $("#btnSearch").click(function () {

                    var searchNumber = $('#phone').val();

                    if (searchNumber != '') {

                        document.getElementById('createLoadingImage').src = 'images/loading.gif';

                        $.post('SearchMobileNubmer', {
                            searchPhone: $('#phone').val()
                        }).done(function (data) {

                            document.getElementById('createLoadingImage').src = '';
                            $("#showSearchNumberDiv").html(data);

                            var totalAmount = 0;
                            $('#example td.amount').each(function () {
                                totalAmount += parseFloat($(this).text().replace(",", ""), 10)
                            });
                            $("#total").text(totalAmount);

                        });
                    } else {
                        alert("Please enter the mobile number");
                    }
                });

                $(document).on("input", "#phone", function () {
                    this.value = this.value.replace(/[^0-9\.]/g, '');
                });
            });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

    </body>
</html>