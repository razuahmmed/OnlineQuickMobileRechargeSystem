<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/comp-sep.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/select2.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function viewResellerButtonClicked(resellerId) {

                var dataString = resellerId;

                window.location = "ViewResellerButtonClicked?resellerId=" + dataString;
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Dashboard <small>Admin Dashboard</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-4 col-xs-4 col-sm-6 col-xs-12 margin-bottom-10">
                            <div class="dashboard-stat blue-madison">
                                <div class="visual">
                                    <i class="icon-paper-plane"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        Taka&nbsp;<s:property value="totalMRSale"/>
                                    </div>
                                    <div class="desc">
                                        Lifetime Mobile Recharge Sales
                                    </div>
                                </div>
                                <a href="MobileRechargeHistory" class="more">
                                    View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xs-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red-intense">
                                <div class="visual">
                                    <i class="icon-handbag"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <s:property value="allMRNumber.size()"/>
                                    </div>
                                    <div class="desc">
                                        Total Mobile Recharge Requests
                                    </div>
                                </div>
                                <a href="MobileRechargeHistory" class="more">
                                    View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xs-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green-haze">
                                <div class="visual">
                                    <i class="icon-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <s:property value="allResallersInfoList.size()"/>
                                    </div>
                                    <div class="desc">
                                        Total Users
                                    </div>
                                </div>
                                <a href="ShowAllResellers" class="more">
                                    View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 col-xs-4 col-sm-6 col-xs-12 margin-bottom-10">
                            <div class="dashboard-stat purple-seance">
                                <div class="visual">
                                    <i class="icon-wallet"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        Taka&nbsp;<s:property value="totalMMSale"/>
                                    </div>
                                    <div class="desc">
                                        Lifetime Mobile Money Sales
                                    </div>
                                </div>
                                <a href="MobileMoneyHistory" class="more">
                                    View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xs-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red-intense">
                                <div class="visual">
                                    <i class="icon-calendar"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <s:property value="allMMNumber.size()"/>
                                    </div>
                                    <div class="desc">
                                        Total Mobile Money Requests
                                    </div>
                                </div>
                                <a href="MobileMoneyHistory" class="more">
                                    View more
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-speech"></i>
                                        <span class="caption-subject bold uppercase"> Customers</span>
                                        <span class="caption-helper">Balance Summary...</span>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <h4>Total&nbsp;Resellers&nbsp;Mobile&nbsp;Recharge&nbsp;Balance&nbsp;(&nbsp;All&nbsp;Resellers&nbsp;Created&nbsp;by&nbsp;<% out.println(userID);%>)&nbsp;:&nbsp;&nbsp;&nbsp;<strong>BDT&nbsp;&nbsp;<s:property value="totalResellerMrBal"/></strong></h4>
                                    <h4>Total&nbsp;Resellers&nbsp;Mobile&nbsp;Money&nbsp;Balance&nbsp;(&nbsp;All&nbsp;Resellers&nbsp;Created&nbsp;by&nbsp;<% out.println(userID);%>)&nbsp;:&nbsp;&nbsp;&nbsp;<strong>BDT&nbsp;&nbsp;<s:property value="totalResellerMmBal"/></strong></h4>
                                    <div style="margin-top: 30px; border: 1px solid #9eacb4;">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr><th colspan="3" style="text-align: center;"><h4>Total Balance By Reseller Type</h4></th></tr>
                                            <tr>
                                                <th>Reseller Type</th>
                                                <th>MR Balance</th>
                                                <th>MM Balance</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Reseller&nbsp;SubAdmin</td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalResellerSubAdminMrBal"/></strong></td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalResellerSubAdminMmBal"/></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Reseller&nbsp;4</td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller4MrBal"/></strong></td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller4MmBal"/></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Reseller&nbsp;3</td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller3MrBal"/></strong></td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller3MmBal"/></strong></td>
                                                </tr>
                                                <tr> 
                                                    <td>Reseller&nbsp;2</td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller2MrBal"/></strong></td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller2MmBal"/></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Reseller&nbsp;1</td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller1MrBal"/></strong></td>
                                                    <td><strong>Tk.&nbsp;<s:property value="totalReseller1MmBal"/></strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <!-- Begin: life time stats -->
                            <div class="portlet box blue-steel">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-thumb-tack"></i>
                                        Latest Mobile Recharge Orders
                                    </div>
                                </div>
                                <div class="portlet-body">

                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#overview_1" data-toggle="tab">
                                                Processing
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#overview_2" data-toggle="tab">
                                                Waiting
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#overview_3" data-toggle="tab">
                                                Success
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="overview_1">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Sender</th>
                                                            <th>Phone</th>
                                                            <th>Amount</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <s:if test="latestProcessingOrdersList !=null">
                                                            <s:if test="latestProcessingOrdersList.size() !=0">
                                                                <s:iterator value="latestProcessingOrdersList">
                                                                    <tr>
                                                                        <td><a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="sender"/>')"><s:property value="sender"/></a></td>
                                                                        <td><s:property value="receiver"/></td>
                                                                        <td><s:property value="givenBalance"/></td>
                                                                        <td>
                                                                            <a href="javascript:void(0);" id="<s:property value="mobileRechargeId"/>" class="btn default btn-xs red-stripe fview">
                                                                                View
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="overview_2">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Sender</th>
                                                            <th>Phone</th>
                                                            <th>Amount</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <s:if test="latestWaitingOrdersList !=null">
                                                            <s:if test="latestWaitingOrdersList.size() !=0">
                                                                <s:iterator value="latestWaitingOrdersList">
                                                                    <tr>
                                                                        <td><a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="sender"/>')"><s:property value="sender"/></a></td>
                                                                        <td><s:property value="receiver"/></td>
                                                                        <td><s:property value="givenBalance"/></td>
                                                                        <td>
                                                                            <a href="javascript:void(0);" id="<s:property value="mobileRechargeId"/>" class="btn default btn-xs red-stripe fview">
                                                                                View
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="overview_3">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Sender</th>
                                                            <th>Phone</th>
                                                            <th>Amount</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <s:if test="latestSuccessOrdersList !=null">
                                                            <s:if test="latestSuccessOrdersList.size() !=0">
                                                                <s:iterator value="latestSuccessOrdersList">
                                                                    <tr>
                                                                        <td><a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="sender"/>')"><s:property value="sender"/></a></td>
                                                                        <td><s:property value="receiver"/></td>
                                                                        <td><s:property value="givenBalance"/></td>
                                                                        <td>
                                                                            <a href="javascript:void(0);" id="<s:property value="mobileRechargeId"/>" class="btn default btn-xs red-stripe fview">
                                                                                View
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>

                        <div class="col-xs-6">
                            <!-- Begin: life time stats -->
                            <div class="portlet box red-sunglo tabbable">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-bar-chart-o"></i>
                                        Revenue
                                    </div>
                                    <div class="tools">
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="portlet-tabs">

                                        <ul class="nav nav-tabs" style="margin-right: 50px">
                                            <li>
                                                <a href="#portlet_tab2" data-toggle="tab" id="statistics_amounts_tab">
                                                    Amounts
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a href="#portlet_tab1" data-toggle="tab">
                                                    Orders
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="portlet_tab1">
                                                <div id="statistics_1" class="chart">

                                                    <!--here is chart 1-->

                                                </div>
                                            </div>
                                            <div class="tab-pane" id="portlet_tab2">
                                                <div id="statistics_2" class="chart">

                                                    <!--here is chart 2-->

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="well no-margin no-border">
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-3 col-xs-6 text-stat">
                                                <span class="label label-success">&nbsp;Total&nbsp;Amount&nbsp;MR&nbsp;:&nbsp;</span>
                                                <h3><s:property value="totalMRSale"/></h3>
                                            </div>
                                            <div class="col-xs-3 col-sm-3 col-xs-6 text-stat">
                                                <span class="label label-info">&nbsp;Total&nbsp;MR&nbsp;Requests&nbsp;:&nbsp;</span>
                                                <h3><s:property value="allMRNumber.size()"/></h3>
                                            </div>
                                            <div class="col-xs-3 col-sm-3 col-xs-6 text-stat">
                                                <span class="label label-danger">&nbsp;Total&nbsp;User&nbsp;Signup&nbsp;:&nbsp;</span>
                                                <h3><s:property value="allResallersInfoList.size()"/></h3>
                                            </div>
                                            <div class="col-xs-3 col-sm-3 col-xs-6 text-stat">
                                                <span class="label label-warning">&nbsp;MR&nbsp;Orders&nbsp;:&nbsp;</span>
                                                <h3><s:property value="allMRNumber.size()"/></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <!--FOR CHART-->
        <script src="<%= request.getContextPath()%>/my-js/flot.js" type="text/javascript"></script>
        <!--<script src="<%= request.getContextPath()%>/my-js/resizes.js" type="text/javascript"></script>-->
        <script src="<%= request.getContextPath()%>/my-js/categories.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/ecommerce-index.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <!--FOR TABLE LOADING-->
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/select2.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                                            jQuery(document).ready(function () {
                                                                                // initiate layout and plugins
                                                                                Metronic.init(); // init metronic core components
                                                                                Layout.init(); // init current layout
                                                                                QuickSidebar.init() // init quick sidebar
                                                                                //        UIIdleTimeout.init();
                                                                                EcommerceIndex.init();
                                                                            });
        </script>

        <script type="text/javascript">

            jQuery(document).ready(function () {

                $(".fview").click(function (e) {

                    e.preventDefault();

                    var id = this.id;

                    $('body').modalmanager('loading');

                    setTimeout(function () {
                        $('#ajax-modal').load('ViewMRechargeHDetails', {mobileRechargeId: id}, function () {
                            $('#ajax-modal').modal();
                        });
                    }, 2000, id);
                });
            });

        </script>

    </body>
</html>