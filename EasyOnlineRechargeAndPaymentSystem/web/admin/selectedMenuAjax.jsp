<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="userLevelMenuInfoList !=null">
    <s:if test="userLevelMenuInfoList.size() !=0">
        <ul class="admin-menu-control-menu" data-auto-scroll="true" data-slide-speed="200">
            <% int pid = 0;%>
            <li class="">
                <s:iterator value="userLevelMenuInfoList" >
                    <% pid++;%>
                    <% int cid = 0;%>
                    <a>
                        <s:if test="levelMenuStatus=='Y'">
                            <input type="checkbox" value="<s:property value="menuId"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="parentMenu" checked>
                        </s:if>
                        <s:else>
                            <input type="checkbox" value="<s:property value="menuId"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="parentMenu">
                        </s:else>
                        <i class="icon-home"></i>
                        <span class="title">
                            <s:property value="parentMenuInfo.parentMenuName"/>
                        </span>
                        <span id=""  onclick="menuShowHide('collapseMenu<%=pid%>')" class="arrow"></span>
                    </a>
                    <!-- userLevelChildMenuList come from UserLevelMenu persistance class -->
                    <s:iterator value="userLevelChildMenuList">
                        <% cid++;%>
                        <ul id="" class="sub-menu collapseMenu<%=pid%>">
                            <li>
                                <a>
                                    <s:if test="userLevelMenuInfo.levelMenuStatus=='Y'">
                                        <input type="checkbox" value="<s:property value="childMenuId"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenu" checked>
                                    </s:if>
                                    <s:else>
                                        <input type="checkbox" value="<s:property value="childMenuId"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenu">
                                    </s:else>
                                    <s:property value="childMenuName"/>
                                </a>
                            </li>
                        </ul>
                    </s:iterator>
                </s:iterator>
            </li>
        </ul>
        <div class="form-actions fluid">
            <div class=" col-md-offset-3 col-md-9">
                <button type="button" onclick="menuPermissionSave();" class="btn blue">
                    <i class="icon-check"></i>
                    Save&nbsp;Changes
                </button>
                <button type="reset" class="btn default">Cancel</button>
                <img id="createLoadingImage" src="" alt="" />
            </div>
        </div> 
    </s:if>
</s:if>
<s:if test="parentMenuInfoList !=null">
    <s:if test="parentMenuInfoList.size() !=0">
        <ul class="admin-menu-control-menu" data-auto-scroll="true" data-slide-speed="200">
            <% int pid = 0;%>
            <li class="">
                <s:iterator value="parentMenuInfoList" >
                    <% pid++;%>
                    <% int cid = 0;%>
                    <a>
                        <s:if test="parentMenuStatus=='Y'">
                            <input type="checkbox" value="<s:property value="menuId"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="parentMenu" checked>
                        </s:if>
                        <s:else>
                            <input type="checkbox" value="<s:property value="menuId"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="parentMenu">
                        </s:else>
                        <i class="icon-home"></i>
                        <span class="title">
                            <s:property value="parentMenuName"/>
                        </span>
                        <span id=""  onclick="menuShowHide('collapseMenu<%=pid%>')" class="arrow"></span>
                    </a>
                    <!-- childMenuList come from ParentMenu persistance class -->
                    <s:iterator value="childMenuList"  >
                        <% cid++;%>
                        <ul id="" class="sub-menu collapseMenu<%=pid%>">
                            <li>
                                <a>
                                    <s:if test="childMenuStatus=='Y'">
                                        <input type="checkbox" value="<s:property value="childMenuId"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenu" checked>
                                    </s:if>
                                    <s:else>
                                        <input type="checkbox" value="<s:property value="childMenuId"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenu">
                                    </s:else>
                                    <s:property value="childMenuName"/>
                                </a>
                            </li>
                        </ul>
                    </s:iterator>
                </s:iterator>
            </li>
        </ul>
        <div class="form-actions fluid">
            <div class=" col-md-offset-3 col-md-9">

                <button type="button" onclick="menuPermissionSave()" class="btn blue">
                    <i class="icon-check"></i>
                    Save
                </button>
                <button type="reset" class="btn default">Cancel</button>
                <img id="createLoadingImage" src="" alt="" />
            </div>
        </div> 
    </s:if>
</s:if>