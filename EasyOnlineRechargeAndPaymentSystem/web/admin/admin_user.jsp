<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function showAdminUser() {

                $.ajax({
                    type: "POST",
                    url: 'ShowAdminUser',
                    success: function (data) {
                        $("#userTable").html(data);
                    }
                });
            }
            //
            function createNewAdminUser() {

                var userRoleId = $("#userRole").val();

                var fullName = document.getElementById("fullName").value;

                var email = document.getElementById("email").value;

                var phone = document.getElementById("phone").value;

                var userName = document.getElementById("username").value;

                var password = document.getElementById("password").value;
                //
                if ((!notEmpty(fullName, "Full Name can't be empty !")) & (!forFullName(fullName, "Invalid fullName Format !")) & (!lengthRestriction(fullName, 3, 20, "your fullName"))) {
                    flag = false;
                }
                //
                if (email == "") {
                    flag = true;
                } else if (!emailValidator(email, "Invalid Email Format !")) {
                    flag = false;
                }
                //
                if (phone == "") {
                    flag = true;
                } else if ((!forPhone(phone, "Please use a valid Phone Number !")) & (!lengthRestriction(phone, 11, 11, "your phone"))) {
                    flag = false;
                }
                //
                if ((!notEmpty(userName, "User Name can't be empty and Unchangeable!")) & (!forUserName(userName, "Invalid userName Format !")) & (!lengthRestriction(userName, 3, 18, "your userName"))) {
                    flag = false;
                }
                //
                if ((!notEmpty(password, "Password can't be empty !")) & (!isAlphanumeric(password, "Invalid password Format !")) & (!lengthRestriction(password, 6, 18, "your password"))) {
                    flag = false;
                }
                //
                var message = confirm("Do you want to create New Admin User ?");

                if (message == true) {

                    var flag = true;

                    if (flag) {

                        var dataString = 'userRoleList=' + userRoleId;
                        dataString += '&fullName=' + fullName;
                        dataString += '&email=' + email;
                        dataString += '&phone=' + phone;
                        dataString += '&userName=' + userName;
                        dataString += '&password=' + password;
                        //
                        document.getElementById('createLoadingImage').src = 'images/loading.gif';
                        //
                        $.ajax({
                            type: "POST",
                            url: 'AddNewAdminUser',
                            data: dataString,
                            success: function (data) {
                                document.getElementById('createLoadingImage').src = '';
                                $("#message").html(data);
                                showAdminUser();
                            }
                        });
                    }
                }
            }
            //
            function adminUserDetailsView(adminUserId) {

                var dataString = adminUserId;

                window.location = "AdminUserDetailsView?adminUserId=" + dataString;
            }
            //
            function deleteAdminUser(adminUserId) {

                var warning = confirm("Do you want to delete this Admin User ?");

                if (warning == true) {

                    var dataString = 'adminUserId=' + adminUserId;
                    //
                    document.getElementById('createLoadingImage').src = 'images/loading.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'DeleteAdminUser',
                        data: dataString,
                        success: function (data) {
                            document.getElementById('createLoadingImage').src = '';
                            $("#message").html(data);
                            showAdminUser();
                        }
                    });
                }
            }
            //
            function forFullName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_. ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function forUserName(id, helperMsg) {
                var alphaExp = /^[0-9a-z_.]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function isAlphanumeric(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function emailValidator(id, helperMsg) {

                var emailExp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (id.match(emailExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
            //
            function forPhone(id, helperMsg) {
                var numericExpression = /^[0-9]+$/;
                if (id.match(numericExpression)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }

        </script>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Admin <small>Manage Users Role</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Add New Role Based User
                                    </div>
                                </div>

                                <div class="portlet-body form">

                                    <form class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label" for="role">User Role</label>
                                                <div class=" col-md-9">
                                                    <select id="userRole" name="userRole" class="form-control input-medium">
                                                        <s:if test="allAdminGroupInfoList !=null">
                                                            <s:if test="allAdminGroupInfoList.size() !=0">
                                                                <s:iterator value="allAdminGroupInfoList">
                                                                    <option <s:if test="adminGroupId==1">selected</s:if> value="<s:property value="adminGroupId"/>"><s:property value="adminGroupName"/></option>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </select>
                                                    <span class="help-block">
                                                        <strong>Support:</strong> Can View All Recharge &amp; Mobile Money Requests, Operator SMS Replies. Manually Confirm Requests. <br>
                                                        <strong>Billing:</strong> Can View All Resellers, Balance, Transactions &amp; Can Add Fund to Resellers. <br>
                                                        <strong>NOC:</strong> View / Monitoring All Requests <br>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class=" col-md-3 control-label" for="fullname">Full Name</label>
                                                <div class=" col-md-4">
                                                    <input type="text" id="fullName" name="fullName" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="email" class=" col-md-3 control-label">Email</label>
                                                <div class=" col-md-4">
                                                    <input type="text" id="email" name="email" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="phone" class=" col-md-3 control-label">Phone</label>
                                                <div class=" col-md-4">
                                                    <input type="text" id="phone" name="phone" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="username" class=" col-md-3 control-label">Username</label>
                                                <div class=" col-md-4">
                                                    <input type="text" id="username" name="username" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="password" class=" col-md-3 control-label">Password</label>
                                                <div class=" col-md-4">
                                                    <input type="password" id="password" name="password" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-actions fluid">
                                            <div class=" col-md-offset-3 col-md-9">
                                                <button type="button" onclick="createNewAdminUser();" class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Add
                                                </button>
                                                <button type="reset" class="btn default">Cancel</button>
                                                <img id="createLoadingImage" src="" alt="" />
                                            </div>
                                        </div>
                                    </form>

                                    <hr>

                                    <p><strong>Login URL : </strong><a href="AdminUserLoginPage" target="_blank">http://brandbanglait.com/adminuser/</a></p>

                                    <hr>

                                    <div style="padding-top: 16px;" class="table-scrollable">
                                        <table id="datatable_orders" class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Full&nbsp;Name</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>UserName</th>
                                                    <th>Role</th>
                                                    <th>Manage</th>
                                                </tr>
                                            </thead>
                                            <tbody id="userTable">
                                                <s:if test="allAdminUserInfoList !=null">
                                                    <s:if test="allAdminUserInfoList.size() !=0">
                                                        <s:iterator value="allAdminUserInfoList" var="adminUserInfo">
                                                            <tr>
                                                                <td class="highlight">
                                                                    <div class="success"></div>
                                                                    <a href="javascript:void(0);" onclick="adminUserDetailsView('<s:property value="adminUserId"/>');">
                                                                        ${adminUserInfo.fullName}
                                                                    </a>
                                                                </td>
                                                                <td>${adminUserInfo.phone}</td>
                                                                <td>${adminUserInfo.email}</td>
                                                                <td>${adminUserInfo.userName}</td>
                                                                <td>${adminGroupInfo.adminGroupName}</td>
                                                                <td>
                                                                    <a href="javascript:void(0);" onclick="adminUserDetailsView('<s:property value="adminUserId"/>');" class="btn btn-primary btn-xs purple">
                                                                        <i class="icon-pencil"></i>
                                                                        View&nbsp;/&nbsp;Edit
                                                                    </a>
                                                                    <a href="javascript:void(0);" onclick="deleteAdminUser('<s:property value="adminUserId"/>');" class="btn btn-danger btn-xs purple">
                                                                        <i class="icon-trash"></i>
                                                                        Delete
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- END FORM TABLE-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/dataTables.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                                        $(document).ready(function () {
                                                                            $('#datatable_orders').DataTable({
                                                                                "orderCellsTop": true,
                                                                                "pagingType": "full_numbers"
                                                                            });
                                                                        });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

    </body>
</html>