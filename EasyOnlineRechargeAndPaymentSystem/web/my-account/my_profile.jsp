<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/comp-sep.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Reports <small>Summary Reports</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <s:if test="profileInfoList !=null">
                                            <s:if test="profileInfoList.size() !=0">
                                                <s:iterator value="profileInfoList">
                                                    <span class="caption-subject font-green-sharp bold uppercase"> Summary for - <s:property value="userName"/> - <s:property value="userGroupInfo.groupName"/> - </span>
                                                    <span class="caption-helper">
                                                        <%
                                                            DateFormat formatter = new SimpleDateFormat("EEEE, MMMM dd YYYY " + "|" + " HH:mm:ss");
                                                            Date date = new Date();
                                                            String today = formatter.format(date);
                                                            out.print(today);
                                                        %>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="portlet-body">
                                                <div class="tabbable">

                                                    <ul class="nav nav-tabs nav-tabs-lg">
                                                        <li class="active">
                                                            <a href="#tab_1" data-toggle="tab"> Details </a>
                                                        </li>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_1">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12">
                                                                    <div class="portlet yellow-crusta box">

                                                                        <div class="portlet-title">
                                                                            <div class="caption">
                                                                                My Details
                                                                            </div>

                                                                            <div class="actions">
                                                                                <a href="EditProfile" class="btn btn-default btn-sm">
                                                                                    <i class="fa fa-pencil"></i>
                                                                                    Edit
                                                                                </a>
                                                                            </div>
                                                                        </div>

                                                                        <div class="portlet-body">
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    User ID :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="userId"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Full Name :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="userName"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Parent ID :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="parentId"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Status :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:if test="activeStatus=='Y'">
                                                                                        <span class="label label-success">Active</span>
                                                                                    </s:if>
                                                                                    <s:else>
                                                                                        <p style="color: red;">Inactive</p>
                                                                                    </s:else>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    User Type :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="userGroupInfo.groupName"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Mobile Recharge Balance :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Mobile Money Balance :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Password :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <a href="ChangePassword">
                                                                                        Change&nbsp;Password
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    Last Login :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:property value="lastLogin"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    OTP :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:if test="otpStatus=='Y'">Yes</s:if>
                                                                                    <s:else>
                                                                                        No
                                                                                    </s:else>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    PIN :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:if test='enablePinStatus=="N"'>No</s:if>
                                                                                    <s:else>
                                                                                        Yes
                                                                                    </s:else>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row static-info">
                                                                                <div class="col-xs-5 name">
                                                                                    API Access :
                                                                                </div>
                                                                                <div class="col-xs-7 value">
                                                                                    <s:if test="apiAccessStatus=='Y'">Yes</s:if>
                                                                                    <s:else>
                                                                                        No
                                                                                    </s:else>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12">
                                                                    <div style="border: 1px solid #b1bdbd;" class="portlet yellow-crusta box">
                                                                        <div style="background-color: #35aa47;" class="portlet-title">
                                                                            <div class="caption">
                                                                                Your Parent Reseller to request for balance :  <s:property value="parentId"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="portlet-body">
                                                                            <div style="padding-top: 12px;" class="row static-info">
                                                                                <div style="width: 70%" class="col-xs-5 name">
                                                                                    <a class="btn blue" data-toggle="modal" href="#modal_flexiload">
                                                                                        <i class="icon-paper-plane"></i>
                                                                                        Request For Balance
                                                                                    </a>
                                                                                </div>
                                                                                <!--                                                                                <div style="width: 30%" class="col-xs-7 value">
                                                                                                                                                                    <a class="btn red" data-toggle="modal" href="#modal_bkash">
                                                                                                                                                                        <i class="icon-wallet"></i>
                                                                                                                                                                        Request For Mobile Money Balance
                                                                                                                                                                    </a>
                                                                                                                                                                </div>-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <div class="well">

<!--                                                                        <div class="row static-info align-reverse">
                                                                            <div class="col-xs-8 name">
                                                                                Total Payments Received:
                                                                            </div>

                                                                            <div class="col-xs-3 value">
                                                                                Default
                                                                            </div>
                                                                        </div>-->

<!--                                                                        <div class="row static-info align-reverse">
                                                                            <div class="col-xs-8 name">
                                                                                Total Recharge Made (Me + My Users):
                                                                            </div>

                                                                            <div class="col-xs-3 value">
                                                                                Default
                                                                            </div>
                                                                        </div>-->

                                                                        <div class="row static-info align-reverse">
                                                                            <div class="col-xs-8 name">
                                                                                Available Balance:
                                                                            </div>

                                                                            <div class="col-xs-3 value">
                                                                                <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/> &AMP;
                                                                                <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-xs-12 col-sm-12">
                                                                    <div class="portlet grey-cascade box">

                                                                        <div class="portlet-title">
                                                                            <div class="caption">
                                                                                Balance in My Resellers
                                                                            </div>
                                                                        </div>

                                                                        <div class="portlet-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-bordered table-striped">
                                                                                    <tbody>
<!--                                                                                        <tr>
                                                                                            <td>Total Available Balance in All My Resellers</td>
                                                                                            <td>Default</td>
                                                                                        </tr>-->
                                                                                        <tr>
                                                                                            <td>My Mobile Recharge Balance</td>
                                                                                            <td><s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>My Mobile Money Balance</td>
                                                                                            <td><s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </s:iterator>
                                                                        </s:if>
                                                                    </s:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                    <!-- Mobile Recharge Request Modal -->
                    <div class="modal fade" id="modal_flexiload" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog">
                            <form id="mr_form" role="form">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Current Balance (
                                            <s:if test="profileInfoList !=null">
                                                <s:if test="profileInfoList.size() !=0">
                                                    <s:iterator value="profileInfoList">
                                                        M Recharge
                                                        <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                                                        and M Money
                                                        <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>)
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-body">
                                            <div class="form-group has-success">
                                                <label><strong>Your Parent ID</strong></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon-arrow-right"></i>
                                                    </span>
                                                    <s:if test="profileInfoList !=null">
                                                        <s:if test="profileInfoList.size() !=0">
                                                            <s:iterator value="profileInfoList">
                                                                <input type="text" id="parentId" name="parentId" value="<s:property value="parentId"/>" readonly="true" class="form-control input-lg" style="font-size:18px;">
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label><strong>Your User ID</strong></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon-arrow-right"></i>
                                                    </span>
                                                    <s:if test="profileInfoList !=null">
                                                        <s:if test="profileInfoList.size() !=0">
                                                            <s:iterator value="profileInfoList">
                                                                <input type="text" id="resellerId" name="resellerId" value="<s:property value="userId"/>" readonly="true" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label><strong>Balance Type</strong></label>
                                                <div class="radio-list">
                                                    <% if ((PM03CM01 && PERMISSION_MR) && (PM04CM01 && PERMISSION_MM)) {%>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="balType" name="balType" value="MR" checked/>
                                                        Mobile Recharge
                                                    </label>

                                                    <label class="radio-inline">
                                                        <input type="radio" id="balType" name="balType" value="MM"/>
                                                        Mobile Money
                                                    </label>
                                                    <% } else if (PM03CM01 && PERMISSION_MR) { %>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="balType" name="balType" value="MR" checked/>
                                                        Mobile Recharge
                                                    </label>
                                                    <% } else if (PM04CM01 && PERMISSION_MM) {%>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="balType" name="balType" value="MM" checked/>
                                                        Mobile Money
                                                    </label>
                                                    <% }%>
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label>
                                                    <strong>Request Amount</strong>
                                                </label>
                                                <div class="input-group input-small">
                                                    <span class="input-group-addon">
                                                        <i class="icon-credit-card"></i>
                                                    </span>
                                                    <input type="text" id="amount" name="amount" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label>
                                                    <strong>PIN</strong>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="icon-key"></i>
                                                    </span>
                                                    <input type="password" id="currentPin" name="currentPin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="createLoadingImage" src="" alt="" />
                                        <button type="button" class="btn default" data-dismiss="modal">
                                            <i class="icon-close"></i>
                                            Close
                                        </button>
                                        <s:if test="pinStatus !=null">
                                            <s:if test='pinStatus=="N"'>
                                                <button type="button" id="btnRequestSubmit" disabled class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Send
                                                </button>
                                            </s:if>
                                            <s:else>
                                                <button type="button" id="btnRequestSubmit" class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Send
                                                </button>
                                            </s:else>
                                        </s:if>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Mobile Money Request Modal -->
                    <!--                    <div class="modal fade" id="modal_bkash" tabindex="-1" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <form role="form">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Current&nbsp;Mobile&nbsp;Money&nbsp;Balance&nbsp;Tk.
                    <s:if test="currentMmBalInfoList !=null">
                        <s:if test="currentMmBalInfoList.size() !=0">
                            <s:iterator value="currentMmBalInfoList">
                                <s:property value="mmCurrentBalance"/>
                            </s:iterator>
                        </s:if>
                    </s:if>
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group has-success">
                    <label><strong>Your Parent ID</strong></label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="icon-arrow-right"></i>
                        </span>
                    <s:if test="profileInfoList !=null">
                        <s:if test="profileInfoList.size() !=0">
                            <s:iterator value="profileInfoList">
                                <input type="text" id="parentId" name="parentId" value="<s:property value="parentId"/>" readonly="true" class="form-control input-lg" style="font-size:18px;">
                            </s:iterator>
                        </s:if>
                    </s:if>
                </div>
            </div>
            <div class="form-group has-success">
                <label><strong>Your User ID</strong></label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="icon-arrow-right"></i>
                    </span>
                    <s:if test="profileInfoList !=null">
                        <s:if test="profileInfoList.size() !=0">
                            <s:iterator value="profileInfoList">
                                <input type="text" id="resellerId" name="resellerId" value="<s:property value="userId"/>" readonly="true" class="form-control input-lg" style="font-size:20px; color:#C00">
                            </s:iterator>
                        </s:if>
                    </s:if>
                </div>
            </div>
            <div class="form-group has-success">
                <label>
                    <strong>Mobile&nbsp;Money&nbsp;Operator</strong>
                </label>
                <select id="mmOperatorList" name="mmOperatorList" class="form-control input-small">
                    <option value="bKash">bKash</option>
                    <option value="DBBL">DBBL</option>
                    <option value="mCash">mCash</option>
                </select>
            </div>
            <div class="form-group has-success">
                <label>
                    <strong>Amount</strong>
                </label>
                <div class="input-group input-medium">
                    <span class="input-group-addon">
                        <i class="icon-credit-card"></i>
                    </span>
                    <input type="text" id="mmAmount" name="mmAmount" class="form-control input-lg" style="font-size:20px; color:#C00">
                </div>
            </div>
            <div class="form-group has-success">
                <label>
                    <strong>PIN</strong>
                </label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="icon-key"></i>
                    </span>
                    <input type="password" id="currentPin" name="currentPin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn default" data-dismiss="modal">
                <i class="icon-close"></i>
                Close
            </button>
            <button type="button" class="btn blue">
                <i class="icon-check"></i>
                Send
            </button>
        </div>
    </div>
</form>
</div>
</div>-->
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/sms-replies.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $("#btnRequestSubmit").click(function (e) {

                    e.preventDefault();

                    var pId = ($("#parentId").val());
                    var rId = ($("#resellerId").val());
                    var bType = $("input[name=balType]:checked").val();
                    var amount = ($("#amount").val());

                    if ((pId != "") && (rId != "") && (bType != "") && (amount != "")) {

                        var message = confirm("Do you want to request for " + bType + " balance ?");

                        if (message == true) {

                            document.getElementById('createLoadingImage').src = 'images/loading.gif';

                            $.post('RequestForBalance', $('#mr_form').serialize(), function (data) {
                                setTimeout(function () {
                                    document.getElementById('createLoadingImage').src = '';
                                    $('#modal_flexiload').modal('hide');
                                    $("#message").html(data);
                                }, 2000);
                            });
                        }
                    } else {
                        alert('Empty field not allow');
                    }
                });
            });

            $(document).on("input", "#amount", function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
                EcommerceOrders.init();
            });
        </script>

    </body>
</html>