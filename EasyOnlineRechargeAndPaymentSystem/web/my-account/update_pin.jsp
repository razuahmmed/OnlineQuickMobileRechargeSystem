<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function setPin() {

                var curPassword = document.getElementById("curPassword").value;

                var transactionPin = document.getElementById("transactionPin").value;

                var confirmTransactionPin = document.getElementById("confirmTransactionPin").value;

                if ((!notEmpty(transactionPin, "Transaction pin can't be empty !")) & (!isAlphanumeric(transactionPin, "Invalid transaction pin Format !")) & (!lengthRestriction(transactionPin, 6, 10, "your transaction pin"))) {
                    return false;
                }

                if (transactionPin != confirmTransactionPin) {
                    alert("Pin do not match !");
                    return false;
                } else {

                    var r = confirm("Do you want to set transaction pin ?");

                    if (r == true) {

                        var dataString = 'transactionPin=' + transactionPin;
                        dataString += '&curPassword=' + curPassword;

                        if ((transactionPin.length != 0) && (confirmTransactionPin.length != 0)) {

                            document.getElementById('createLoadingImage').src = 'images/loading.gif';

                            $.ajax({
                                type: "POST",
                                url: 'SetTransactionPin',
                                data: dataString,
                                success: function (data) {
                                    document.getElementById('createLoadingImage').src = '';
                                    $("#message").html(data);
                                }
                            });
                        }
                    }
                }
            }
            //
            function isAlphanumeric(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //            
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Profile&nbsp<small>Set Transaction Pin</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row profile">
                        <div class=" col-md-12  ">
                            <!--BEGIN TABS-->
                            <div class="portlet light">

                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-green-sharp bold uppercase">Set Transaction Pin</span>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable tabbable-custom tabbable-full-width">

                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_3" data-toggle="tab">Account</a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <!--tab_1_2-->
                                            <div class="tab-pane active" id="tab_1_3" >
                                                <div class=" col-md-4  ">
                                                    <div class="tab-content" id="pwd-container">

                                                        <div id="tab_3-3" class="tab-pane active">
                                                            <form>
                                                                <div class="form-group">
                                                                    <label class="control-label">Current&nbsp;Password</label>
                                                                    <input type="password" id="curPassword" name="curPassword" class="form-control"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Transaction Pin</label>
                                                                    <input type="password" id="transactionPin" name="transactionPin" class="form-control pws"/>
                                                                    <span class="help-block">Please Use a Strong Password</span>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="pwstrength_viewport_progress"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Re-type Transaction Pin</label>
                                                                    <input type="password" id="confirmTransactionPin" name="confirmTransactionPin" class="form-control"/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <button type="button" onclick="setPin();" class="btn green">
                                                                        <i class="icon-check"></i>
                                                                        Set Transaction Pin
                                                                    </button>
                                                                    <img id="createLoadingImage" src="" alt="" />
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end tab-pane-->

                                            <!--end tab-pane-->

                                            <!--end tab-pane-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>


        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/passwordStrength.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/checkpw.js" type="text/javascript"></script>

        <script>
                                                                        jQuery(document).ready(function () {
                                                                            // initiate layout and plugins
                                                                            Metronic.init(); // init metronic core components
                                                                            Layout.init(); // init current layout
                                                                            QuickSidebar.init() // init quick sidebar
                                                                            //        UIIdleTimeout.init();

                                                                        });

        </script>

    </body>
</html>