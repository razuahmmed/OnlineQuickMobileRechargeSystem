<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleBalanceRequestReceiveInfoList !=null">
    <s:if test="singleBalanceRequestReceiveInfoList.size() !=0">
        <s:iterator value="singleBalanceRequestReceiveInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Request ID # <s:property value="balanceRequestId"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN ALERTS PORTLET-->
                        <div class="portlet green box">
                            <div class="portlet-title">
                                <div class="caption">
                                    Request Details
                                </div>
                            </div>
                            <div class="portlet-body">
<!--                                <div class="alert alert-success">
                                    <strong>Success! </strong>Request Transaction ID is - 
                                </div>                    
                                <form method="post" class="form-inline" role="form" action="#">
                                    <div class="form-group">
                                        <label class="sr-only" for="trid">TrID</label>
                                        <input class="form-control" autocomplete="off" id="trid" name="trid" value="Success" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-success">Manual Confirm</button>
                                </form>
                                <div class="clearfix">
                                    <br>
                                </div>
                                <form method="post" class="form-inline" role="form" action="#">
                                    <div class="form-group">
                                        <label class="sr-only" for="trid">TrID</label>
                                        <input autocomplete="off" placeholder="Failed Reason..." class="form-control" id="trid" name="trid" value="Success" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-danger">Refund</button>
                                </form>
                                <div class="clearfix">
                                    <br>
                                                                    <a href="" class="btn red">Refund</a>
                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>
                                </div>-->
                                <h4>Request Details</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 30%">Request ID</th>
                                                <th><s:property value="balanceRequestId"/></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Request Sender</td>
                                                <td><s:property value="requestSender"/></td>
                                            </tr>
                                            <tr>
                                                <td>Request Amount</td>
                                                <td><s:property value="amount"/></td>
                                            </tr>
                                            <tr>
                                                <td>Request Receiver</td>
                                                <td><s:property value="requestReceiver"/></td>
                                            </tr>
                                            <tr>
                                                <td>Balance Type</td>
                                                <td>
                                                    <s:if test='balanceType=="MR"'>
                                                        Mobile Recharge
                                                    </s:if>
                                                    <s:else>
                                                        <span style="color: #009966;">
                                                            Mobile Money
                                                        </span>
                                                    </s:else>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Request Status</td>
                                                <td>
                                                    <s:if test="requestStatus=='Y'">
                                                        <span>
                                                            Success
                                                        </span>
                                                    </s:if>
                                                    <s:elseif test="requestStatus=='W'">
                                                        <span>
                                                            Waiting
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="requestStatus=='P'">
                                                        <span>
                                                            Processing
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="requestStatus=='W'">
                                                        <span>
                                                            Waiting
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="requestStatus=='F'">
                                                        <span>
                                                            Failed
                                                        </span>
                                                    </s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Request Time</td>
                                                <td><s:property value="requestTime"/></td>
                                            </tr>
                                            <tr>
                                                <td>Success Time</td>
                                                <td><s:property value="requestSuccessTime"/></td>
                                            </tr>
                                            <tr>
                                                <td>Transaction ID</td>
                                                <td><s:property value="trid"/></td>
                                            </tr>
<!--                                            <tr>
                                                <td>IP</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>A</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>R</td>
                                                <td></td>
                                            </tr>-->
                                            <tr>
                                                <td>Response By</td>
                                                <td><s:property value="requestResponseBy"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Request For this number</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Sender</th>
                                                <th>Receiver</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><s:property value="balanceRequestId"/></td>
                                                <td><s:property value="requestSender"/></td>
                                                <td><s:property value="requestReceiver"/></td>
                                                <td><s:property value="requestTime"/></td>
                                                <td>
                                                    <s:if test="requestStatus=='Y'">
                                                        <span>
                                                            Success
                                                        </span>
                                                    </s:if>
                                                    <s:elseif test="requestStatus=='W'">
                                                        <span>
                                                            Waiting
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="requestStatus=='P'">
                                                        <span>
                                                            Processing
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="requestStatus=='W'">
                                                        <span>
                                                            Waiting
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="requestStatus=='F'">
                                                        <span>
                                                            Failed
                                                        </span>
                                                    </s:elseif>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
<!--                                <h4>Related SMS</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                                <th>SMS</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>-->
                            </div>
                        </div>
                        <!-- END ALERTS PORTLET-->
                    </div>
                </div>
            </div>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>