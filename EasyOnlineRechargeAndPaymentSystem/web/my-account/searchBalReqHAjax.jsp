<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>

<%
    boolean PM03CM01_BRH = (request.getSession().getAttribute("23") != null && request.getSession().getAttribute("23").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM04CM01_BRH = (request.getSession().getAttribute("31") != null && request.getSession().getAttribute("31").toString().equalsIgnoreCase("T")) ? true : false;

    boolean PERMISSION_MR_BRH = (request.getSession().getAttribute("MR022") != null && request.getSession().getAttribute("MR022").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PERMISSION_MM_BRH = (request.getSession().getAttribute("MM030") != null && request.getSession().getAttribute("MM030").toString().equalsIgnoreCase("T")) ? true : false;
%>

<s:if test="balanceRequestReceiveInfoList !=null">
    <s:if test="balanceRequestReceiveInfoList.size() !=0">
        <s:iterator value="balanceRequestReceiveInfoList">
            <tr>
                <td><input type="checkbox" id="chkb" name="chkb" value="<s:property value="balanceRequestId"/>" class="form-filter"></td>
                <td align="center" class="sorting_1">
                    <s:property value="balanceRequestId"/>
                    <br/>
                    <a href="javascript:void(0);" id="<s:property value="balanceRequestId"/>" class="btn fview btn-circle btn-xs blue">
                        <i class="icon-list"></i>
                        View
                    </a>
                </td>
                <td align="center"><s:property value="requestTime"/></td>
                <td><s:property value="requestSender"/></td>
                <td><s:property value="requestReceiver"/></td>
                <td>
                    <s:if test='balanceType=="MR"'>
                        Mobile Recharge
                    </s:if>
                    <s:else>
                        <span style="color: #009966;">
                            Mobile Money
                        </span>
                    </s:else>
                </td>
                <td class="amount"><s:property value="amount"/></td>
                <td align="center">
                    <s:if test="requestStatus=='Y'">
                        <span>
                            Success
                        </span>
                    </s:if>
                    <s:elseif test="requestStatus=='W'">
                        <span style="color: #1590f2;">
                            Waiting
                        </span>
                    </s:elseif>
                    <s:elseif test="requestStatus=='P'">
                        <span>
                            Processing
                        </span>
                    </s:elseif>
                    <s:elseif test="requestStatus=='F'">
                        <span>
                            Failed
                        </span>
                    </s:elseif>
                </td>
                <td><s:property value="trid"/></td>
                <td><s:property value="requestSuccessTime"/></td>
                <td><s:property value="requestResponseBy"/></td>
                <td>
                    <s:if test='balanceType=="MR"'>
                        <% if ((PM03CM01_BRH && PERMISSION_MR_BRH)) {%>
                        <s:if test="requestStatus!='Y'">
                            <a href="RequestForPayment?resellerId=<s:property value="requestSender"/>&balReqID=<s:property value="trid"/>&amount=<s:property value="amount"/>" class="send_balance btn btn-circle blue btn-sm">
                                <i class="icon-plus"></i>
                                Send Balance
                            </a>
                        </s:if>
                        <% }%>
                    </s:if>

                    <s:if test='balanceType=="MM"'>
                        <% if (PM04CM01_BRH && PERMISSION_MM_BRH) {%>
                        <s:if test="requestStatus!='Y'">
                            <a href="RequestForPayment?resellerId=<s:property value="requestSender"/>&loadType=<s:property value="balanceType"/>&balReqID=<s:property value="trid"/>&amount=<s:property value="amount"/>" id="<s:property value="balanceRequestId"/>" class="send_balance btn btn-circle purple-medium btn-sm">
                                <i class="icon-plus"></i>
                                Send Balance
                            </a>
                        </s:if>
                        <% }%>
                    </s:if>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>