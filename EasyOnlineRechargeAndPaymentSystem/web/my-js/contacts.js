$(document).ready(function () {

    var ajax_modal = $('#ajax-modal');

    var contact_save = $(".contact_save");

    function load_contacts() {
        $.get('ShowContactsList', function (data) {
            $("#tb").html(data);
        });
    }

    $("#my_contacts").on('click', function (e) {

        e.preventDefault();

        $('body').modalmanager('loading');

        ajax_modal.load('.my_contacts', '', function () {
            load_contacts();
            ajax_modal.modal();
        });
    });

    ajax_modal.on('click', '.contact_save', function (e) {

        e.preventDefault();

        contact_save.prop('disabled', true);

        contact_save.html("Saving...");

        var cName = $('#contact_name').val();

        var cNumber = $('#contact_number').val();

        if ((notEmpty(cName, "Contact name empty")) && (forContactName(cName, "Invalid format")) && (lengthRestriction(cName, 3, 16, "contact name"))) {

        }

        if ((notEmpty(cNumber, "Mobile number empty")) && (!forPhone(cNumber))) {
            contact_save.prop('disabled', false);
            contact_save.html("Save");
            return false;
        }

        var message = confirm("Do you want to add contact ?");

        if (message == true) {

            var dataString = 'contactName=' + cName;
            dataString += '&contactNumber=' + cNumber;

            $.post('ContactsAdd', dataString).done(function (data) {

                contact_save.prop('disabled', false);

                contact_save.html("Save");

                $("#contact_resp").html(data);

                load_contacts();
            });
        } else {
            contact_save.prop('disabled', false);
            contact_save.html("Save");
        }
    });

    ajax_modal.on('click', '.contact_recharge', function (e) {

        e.preventDefault();

        var cid = this.id;

        $.get('ContactsGet', {contactId: cid}, function (data) {
            $('#ajax-modal').modal('hide');
            $("#spanPhone").html(data);
        });
    });

    ajax_modal.on('click', '.contact_delete', function (e) {

        e.preventDefault();

        var cid = this.id;

        var message = confirm("Do you want to delete this contact ?");

        if (message == true) {

            $.get('ContactsDelete', {contactId: cid}, function (data) {

                $("#contact_resp").html(data);

                load_contacts();
            });
        }
    });

    function notEmpty(id, helperMsg) {
        if (id.length == 0) {
            contact_save.prop('disabled', false);
            contact_save.html("Save");
            alert(helperMsg);
            id.focus();
            return false;
        }
        return true;
    }

    function forContactName(id, helperMsg) {
        var alphaExp = /^[0-9a-zA-Z_.]+$/;
        if (id.match(alphaExp)) {
            return true;
        } else {
            contact_save.prop('disabled', false);
            contact_save.html("Save");
            alert(helperMsg);
            id.focus();
            return false;
        }
    }

    function lengthRestriction(id, min, max, msg) {
        var uInput = id;
        if (uInput.length >= min && uInput.length <= max) {
            return true;
        } else {
            contact_save.prop('disabled', false);
            contact_save.html("Save");
            alert("Please enter " + msg + " between " + min + " to " + max + " characters");
            id.focus();
            return false;
        }
    }

    function phoneTest(a) {
        var b = /^(\+88|88|)(\d{11})$/;
        return b.test(a)
    }

    function forPhone(a) {

        var d = a;

        var h = phoneTest(d);

        if (h == true) {

            if (d.length > 11) {
                d = d.substring(d.length - 11);
            }

            var i = d.substring(0, 3);

            if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {
                return true;
            } else {
                alert("Invalid Mobile No");
            }
        } else {
            alert("Invalid Mobile No");
        }
    }
});