$(document).ready(function () {

    $('#summernote_1').summernote({height: 300});

    $("#submit").click(function (e) {

        e.preventDefault();

        $(this).html("Working ...");

        $(this).addClass("red");

        $.post('', {
            contents: $('#summernote_1').code()
        })
                .done(function (data) {

                    setTimeout(function () {
                        var sbutton = $("#submit");
                        location.reload();
                    }, 2000);
                });
    });
});