

$(document).ready(function () {

    function showUserSummary() {

        var cid = $('#cid').val();

        if (cid != '') {

            var result = $("#result");

            result.html('<h3>Loading...</h3>');

            $.post('UserSummaryAjax', {
                resellerId: $('#cid').val(),
                loadType: $("input[name=loadType]:checked").val()
            }).done(function (data) {

                var result = $("#result");
                result.html(data);
            });
        }
    }

    showUserSummary();

    $('input[type=radio][name=loadType]').change(function () {
        showUserSummary();
    });

    $('#cid').select2().on("change", function () {
        showUserSummary();
    });
});