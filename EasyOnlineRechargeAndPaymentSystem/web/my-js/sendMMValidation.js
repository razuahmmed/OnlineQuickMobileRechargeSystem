function phoneTest(a) {
    var b = /^(\+88|88|)(\d{11})$/;
    return b.test(a)
}

function phoneTestDBBL(a) {
    var b = /^(\+88|88|)(\d{12})$/;
    return b.test(a)
}

function amountTest(a) {
    var b = /^[1-9][0-9]+$/;
    return b.test(a)
}

function showError(a, b) {
    alert(b);
    a.focus();
    return false;
}

function sendMMValidate(a, b, c) {

    var operator = document.getElementById("operatorList").value;

    var d = document.getElementById("phone").value;

    var reTypePhone = document.getElementById("reTypePhone").value;

    if (d == "") {
        return showError(a, "Mobile No cannot be empty");
    }

    if (operator == "DBBL") {
        var h = phoneTestDBBL(d);
    } else {
        var h = phoneTest(d);
    }

    if (h == true) {

        var phone = d;

        if (operator == "DBBL") {
            if (d.length > 12) {
                d = d.substring(d.length - 12);
            }
        } else {
            if (d.length > 11) {
                d = d.substring(d.length - 11);
            }
        }

        var i = d.substring(0, 3);

        if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {

            if (phone != reTypePhone) {
                alert("Mobile number do not match !");
                return false;
            }

            var g = document.getElementById("amount").value;

            if (g == "") {
                return showError(b, "Mobile Money amount cannot be empty");
            }

            if (g < 500) {
                return showError(b, "Amount cannot be less than 500.");
            }

            if (g > 50000) {
                return showError(b, "Amount cannot be greater than 50000.");
            }

            var am = amountTest(g);

            if (am == true) {
                mmConfirmPage();
            } else {
                return showError(b, "Invalid amount");
            }
        } else {
            return showError(a, "Invalid Mobile No");
        }
    } else {
        return showError(a, "Invalid Mobile No")
    }
}

function mmConfirmPage() {

    var phoneNo = document.getElementById("phone").value;

    var operatorTest = document.getElementById("operatorList").value;

    if (operatorTest == "DBBL") {
        if (phoneNo.length > 12) {
            phoneNo = phoneNo.substring(phoneNo.length - 12);
        }
    } else {
        if (phoneNo.length > 11) {
            phoneNo = phoneNo.substring(phoneNo.length - 11);
        }
    }

    var i = phoneNo.substring(0, 3);

    if (i == "019") {
        i = "Banglalink";
    } else if (i == "018") {
        i = "Robi";
    } else if (i == "017") {
        i = "Grameen Phone";
    } else if (i == "016") {
        i = "Airtel";
    } else if (i == "015") {
        i = "Teletalk";
    } else {
        return showError(a, "Invalid mobile operator");
    }

    document.getElementById('createLoadingImage').src = 'images/loading.gif';

    var operator = document.getElementById("operatorList").value;

    var type = document.getElementById('type').value;

    var phone = document.getElementById("phone").value;

    var amount = document.getElementById("amount").value;

    var dataString = 'phone=' + phone;
    dataString += '&amount=' + amount;
    dataString += '&type=' + type;
    dataString += '&mmOperator=' + operator;
    dataString += '&operator=' + i;

    document.getElementById('createLoadingImage').src = '';

    window.location = 'MMConfirmPage?' + dataString;
}