<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>

        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/datepicker.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            .modal-scrollable {
                overflow: auto !important;
            }
        </style>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Money<small>&nbsp;bKash,&nbsp;DBBL,&nbsp;mCash,&nbsp;uCash,&nbsp;MobiCash&nbsp;etc.</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-green-sharp">
                                        <span class="caption-subject bold uppercase">Mobile&nbsp;Money</span>
                                        <span class="caption-helper">History...</span>
                                    </div>
                                    <div class="actions">
                                        <div class="tools">
                                            <a href="SendMobileMoney" class="btn btn-circle purple-medium btn-sm">
                                                <i class="icon-plus"></i>&nbsp;Send&nbsp;Money</a>
                                            <a href="javascript:void(0);" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span></span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="refund">Refund</option>
                                                <option value="resend">Resend</option>
                                                <option value="confirm">Manual Confirm</option>
                                            </select>
                                            <button class="btn btn-sm yellow table-group-action-submit"><i class="icon-check"></i> Submit</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                            <thead>
                                                <tr role="row" class="filter">
                                                    <td colspan="14">
                                                        <h4>Filter</h4>
                                                        <form class="form-inline" role="form">
                                                            <div class="form-group">
                                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                                    <input type="text" id="sdate" name="sdate" placeholder="From Date" value="" readonly class="form-control form-filter input-md">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-md default" type="button"><i class="icon-calendar"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                                    <input type="text" id="tdate" name="tdate" placeholder="To Date" value="" readonly class="form-control form-filter input-md">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-md default" type="button"><i class="icon-calendar"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <select id="status" name="status" class="form-control form-filter input-md">
                                                                <option value="All">All</option>
                                                                <option value="Y">Success</option>
                                                                <option value="N">Pending</option>
                                                                <option value="P">Processing</option>
                                                                <option value="W">Waiting</option>
                                                                <option value="F">Failed</option>
                                                            </select>
                                                            <button type="button" class="btn yellow  filter-submit1">Search</button>
                                                        </form>
                                                        <hr>
                                                    </td>
                                                </tr>

                                                <tr role="row" class="heading">
                                                    <th width="2%"><input type="checkbox" class="group-checkable"></th>
                                                    <th width="5%">Order</th>
                                                    <th width="8%">Purchased&nbsp;On</th>
                                                    <th width="10%">Sender</th>
                                                    <th width="10%">Receiver</th>
                                                    <th width="8%">Operator</th>
                                                    <th width="8%">Type</th>
                                                    <th width="8%">Amount</th>
                                                    <th width="6%">Amount+Fee</th>
                                                    <th width="8%">Status</th>
                                                    <th width="10%">TrID</th>
                                                    <th width="6%">Originator</th>
                                                    <th width="6%">OP Bal</th>
                                                    <th width="7%"> Actions</th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td></td>
                                                    <td><input type="text" id="order_id" name="order_id" class="form-control form-filter input-sm"></td>
                                                    <td></td>
                                                    <td><input type="text" id="sender" name="sender" class="form-control form-filter input-sm"></td>
                                                    <td><input type="text" id="receiver" name="receiver" class="form-control form-filter input-sm"></td>
                                                    <td>
                                                        <select id="op" name="op" style="width: 90px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="bKash">bKash</option>
                                                            <option value="DBBL">DBBL</option>
                                                            <option value="mCash">mCash</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="type" name="type" style="width: 100px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="Personal">Personal</option>
                                                            <option value="Agent">Agent</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <input type="text" id="famount" name="famount" class="form-control form-filter input-sm margin-bottom-5 clearfix" placeholder="From"/>
                                                        </div>
                                                        <input type="text" id="tamount" name="tamount" class="form-control form-filter input-sm" placeholder="To"/>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><input type="text" id="trid" name="trid" style="width: 70px;" class="form-control form-filter input-sm"></td>
                                                    <td><input type="text" id="o" name="o" class="form-control form-filter input-sm"></td>
                                                    <td></td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit1 margin-bottom"><i class="icon-check"></i> Search</button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="icon-close"></i> Reset</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_mm_history">
                                                <s:if test="allMMoneyHInfoList != null">
                                                    <s:if test="allMMoneyHInfoList.size()!=0">
                                                        <s:iterator value="allMMoneyHInfoList">
                                                            <tr>
                                                                <td><input type="checkbox" id="chkb" name="chkb" value="<s:property value="mobileMoneyId"/>"></td>
                                                                <td align="center" class="sorting_1">
                                                                    <s:property value="mobileMoneyId"/>
                                                                    <br/>
                                                                    <a href="javascript:void(0);" id="<s:property value="mobileMoneyId"/>" class="btn fview btn-circle btn-xs purple-medium tooltips" data-container="body" data-placement="top" data-original-title="Show Details">
                                                                        <i class="icon-list"></i>
                                                                    </a>
                                                                </td>
                                                                <td align="center">
                                                                    <s:property value="purchasedOn"/>
                                                                </td>
                                                                <td align="center"><s:property value="sender"/></td>
                                                                <td align="center"><s:property value="receiver"/></td>
                                                                <td align="center"><s:property value="operator"/></td>
                                                                <td align="center"><s:property value="type"/></td>
                                                                <td align="center"><s:property value="givenBalance"/></td>
                                                                <td align="center"><s:property value="totalAmount"/></td>
                                                                <td align="center">
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td align="center"><s:property value="trid"/></td>
                                                                <td align="center"><s:property value="originator"/></td>
                                                                <td align="center"><s:property value="operatorBalance"/></td>
                                                                <td>
                                                                    <a href="javascript:void(0);"  id="<s:property value="mobileMoneyId"/>" class="fview btn btn-xs purple-medium btn-editable">
                                                                        <i class="icon-list"></i>
                                                                        View
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="7" style="text-align:right">Total:</th>
                                                    <th colspan="7" style="text-align: left"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery-migrate-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery-ui-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-hover-dropdown.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_005.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_003.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_004.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-switch.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/select2.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/dataTables.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-datepicker.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/datatable.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/mmHModalView.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(7)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(7, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(7).footer()).html(
                                'Page Total ' + pageTotal + ' [ All Total ' + total + ' ]'
                                );
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                //initiate layout and plugins
                Metronic.init();
                Layout.init();
                QuickSidebar.init();
                EcommerceOrders.init();
            });
        </script>

    </body>
</html>