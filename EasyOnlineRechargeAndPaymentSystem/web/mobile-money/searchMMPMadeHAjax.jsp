<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="mmPMadeInfoList !=null">
    <s:if test="mmPMadeInfoList.size() !=0">
        <s:iterator value="mmPMadeInfoList">
            <tr>
                <td><input type="checkbox" id="chkb" name="chkb" value="<s:property value="mmTransactionHistoryId"/>"></td>
                <td><s:property value="mmTransactionHistoryId"/></td>
                <td><s:property value="balanceGivenDate"/></td>
                <td><s:property value="balanceGivenBy"/></td>
                <td><s:property value="userInfo.userId"/></td>
                <td>
                    <s:if test="type=='Y'">
                        Transfer
                    </s:if>
                    <s:else>
                        <span style="color: #ff0033;">
                            Waiting
                        </span>
                    </s:else>
                </td>
                <td><s:property value="addBalance"/></td>
                <td><s:property value="0"/></td>
                <td><s:property value="0"/></td>
                <td><s:property value="trid"/></td>
                <td><s:property value="mmBalanceDescription"/></td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="mmTransactionHistoryId"/>" class="fview btn btn-xs default btn-editable">
                        <i class="icon-list"></i>
                        View
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>